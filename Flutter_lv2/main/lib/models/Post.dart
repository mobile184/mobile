import 'Comment.dart';

class Post {
  String avatar;
  String name;
  String title;
  String time_ago;
  String image;
  List<String> user_liked;
  int likes;
  List<Comment> comments;
  Post(this.avatar, this.name, this.title, this.time_ago, this.image, this.user_liked,
      this.likes, this.comments);
}
