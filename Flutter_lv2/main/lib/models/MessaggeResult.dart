class MessageResult {
  String name;
  String img;
  String lastMessage;
  bool followed;
  MessageResult(this.name, this.img, this.lastMessage, this.followed);
}
