class SuggestedUser {
  String avatar;
  String user_name;
  String name;
  bool followed;
  SuggestedUser(this.avatar, this.user_name, this.name, this.followed);
}
