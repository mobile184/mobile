import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:main/activities/register.dart';
import 'package:main/main.dart';

class MyLoginPage extends StatefulWidget {
  @override
  State<MyLoginPage> createState() => _MyLoginState();
}

class _MyLoginState extends State<MyLoginPage> {
  var _passVisible;
  @override
  void initState() {
    super.initState();
    _passVisible = false;
  }

  final _email_controller = TextEditingController();
  final _password_controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Welcom Back!',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
              Container(
                decoration:
                    BoxDecoration(color: Color.fromARGB(255, 248, 246, 246)),
                child: TextField(
                  controller: _email_controller,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: Icon(Icons.email_outlined),
                    hintText: 'Email adress',
                  ),
                ),
              ),
              Container(
                decoration:
                    BoxDecoration(color: Color.fromARGB(255, 248, 246, 246)),
                child: TextFormField(
                  controller: _password_controller,
                  obscureText: !_passVisible,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: Icon(Icons.lock_outline),
                    hintText: 'Password',
                    suffixIcon: IconButton(
                      icon: _passVisible == true
                          ? Icon(Icons.visibility_outlined)
                          : Icon(Icons.visibility_off_outlined),
                      onPressed: () {
                        setState(() {
                          _passVisible = !_passVisible;
                        });
                      },
                    ),
                  ),
                ),
              ),
              Align(
                child: Text('Forgot password?'),
                alignment: Alignment.centerRight,
              ),
              SizedBox(
                width: 300,
                child: TextButton(
                  onPressed: () {
                    if (_email_controller.text == 'admin' &&
                        _password_controller.text == '123456') {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MainPage()),
                      );
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(
                            'Wrong email or password!',
                          ),
                        ),
                      );
                    }
                  },
                  child: Text('Sign in'),
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blue,
                    primary: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                child: Text(
                  "I haven't an account",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyRegisterPage(),
                    ),
                  );
                },
              ),
            ]
                .map(
                  (e) => Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: e,
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }
}
