import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:main/activities/login.dart';

class MyRegisterPage extends StatefulWidget {
  @override
  State<MyRegisterPage> createState() => _MyRegisterState();
}

class _MyRegisterState extends State<MyRegisterPage> {
  var _passVisible;
  @override
  void initState() {
    super.initState();
    _passVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Create an account!',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
              Container(
                decoration:
                    BoxDecoration(color: Color.fromARGB(255, 248, 246, 246)),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: Icon(Icons.person),
                    hintText: 'User name',
                  ),
                ),
              ),
              Container(
                decoration:
                    BoxDecoration(color: Color.fromARGB(255, 248, 246, 246)),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: Icon(Icons.email),
                    hintText: 'Email adress',
                  ),
                ),
              ),
              Container(
                decoration:
                    BoxDecoration(color: Color.fromARGB(255, 248, 246, 246)),
                child: TextFormField(
                  obscureText: !_passVisible,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: Icon(Icons.lock),
                    hintText: 'Password',
                    suffixIcon: IconButton(
                      icon: _passVisible == true
                          ? Icon(Icons.visibility)
                          : Icon(Icons.visibility_off),
                      onPressed: () {
                        setState(() {
                          _passVisible = !_passVisible;
                        });
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 300,
                child: TextButton(
                  onPressed: () {},
                  child: Text('Register'),
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blue,
                    primary: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyLoginPage(),
                    ),
                  );
                },
                child: Text(
                  "I've already an account",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              ),
            ]
                .map(
                  (e) => Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: e,
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }
}
