import 'package:flutter/material.dart';
import 'package:main/activities/home.dart';
import 'package:main/models/SuggestedUser.dart';

class MyFlowPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyFlowPageState();
  }
}

class MessageResult {
  String name;
  String img;
  String lastMessage;
  bool followed;
  MessageResult(this.name, this.img, this.lastMessage, this.followed);
}

List<String> recent = [
  'Donald Trump posted on Social. See his post.',
  'Khanh An is premiering. Tap to see.'
];
List<SuggestedUser> suggestedUsers = [
  new SuggestedUser(
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHI2VETqCsHvG9Kyg8nbrAbJgSsV6t0CzXsZ457BtHBQ&s',
      'florentino',
      'Florentino',
      false),
  new SuggestedUser(
      'https://thumbs.dreamstime.com/b/portrait-cute-funny-raccoon-isolated-white-background-portrait-cute-funny-raccoon-141168093.jpg',
      'nanami desu',
      'Nanami',
      false),
];

class _MyFlowPageState extends State<MyFlowPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          //Follow request
          FollowRequest(),
          ListTile(
            leading: Text(
              'Today',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
          //Today new followers
          TodayFollow(),
          ListTile(
            leading: Text(
              'Recent',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          //Recent events

          RecentEvent(),
          ListTile(
            leading: Text(
              'Suggestions',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
          //List suggestions
          ListSuggestion(),
        ],
      ),
    );
  }
}

class FollowRequest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: 40),
        child: ListTile(
          title: Text('Follow Request'),
          subtitle: Text('Approve or ignore request'),
          leading: Container(
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: Colors.black, width: 1)),
            child: IconButton(
              icon: Icon(Icons.person_add_outlined),
              onPressed: () {},
            ),
          ),
        ),
      ),
    );
  }
}

class TodayFollow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Stack(
        children: List.generate(2, (index) {
          return Container(
            margin: EdgeInsets.only(left: 10.0 * index, top: 8.0 * index),
            child: CircleAvatar(
              backgroundImage:
                  NetworkImage('https://source.unsplash.com/random?sig=$index'),
            ),
          );
        }),
      ),
      title: Wrap(children: [
        Text('Zuckkerberg, Obama and 3 others'),
        Text(
          'started following you',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
        ),
      ]),
    );
  }
}

class RecentEvent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(recent.length, (index) {
        return ListTile(
          title: Text(recent[index],
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal)),
          leading: CircleAvatar(
              radius: 25,
              backgroundImage: NetworkImage(
                  'https://source.unsplash.com/random?sig=${index + 100}')),
        );
      }),
    );
  }
}

class ListSuggestion extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ListSuggestionState();
  }
}

class _ListSuggestionState extends State<ListSuggestion> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
        suggestedUsers.length,
        (index) {
          return Container(
            height: 60,
            child: ListTile(
              trailing: SizedBox(
                width: 120,
                child: Row(mainAxisSize: MainAxisSize.min, children: [
                  SizedBox(
                    width: 90,
                    child: ElevatedButton(
                      child: Text(
                        suggestedUsers[index].followed == true
                            ? 'Unfollow'
                            : 'Follow',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        setState(() {
                          suggestedUsers[index].followed =
                              !suggestedUsers[index].followed;
                        });
                      },
                      style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        backgroundColor: suggestedUsers[index].followed == true
                            ? Color.fromARGB(255, 171, 248, 127)
                            : Color.fromARGB(255, 61, 168, 255),
                        textStyle: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 30,
                    child: RawMaterialButton(
                        child: Icon(Icons.close),
                        onPressed: () {
                          suggestedUsers.removeAt(index);
                        }),
                  ),
                ]),
              ),
              title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(suggestedUsers[index].user_name),
                    Text(
                      suggestedUsers[index].name,
                      style: TextStyle(
                          fontSize: 12, fontWeight: FontWeight.normal),
                    ),
                    Text(
                      'Suggested for you',
                      style: TextStyle(
                          fontSize: 12, fontWeight: FontWeight.normal),
                    ),
                  ]),
              leading: CircleAvatar(
                  radius: 25,
                  backgroundImage: NetworkImage(suggestedUsers[index].avatar)),
            ),
          );
        },
      ),
    );
  }
}
