import 'package:flutter/material.dart';
import 'package:main/activities/home.dart';

class MyProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyProfilePageState();
  }
}

class _MyProfilePageState extends State<MyProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 40, left: 20, right: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //Profile details
          ProfileDetail(),
          //Posts, followers, following
          Overview(),
          //Complete profile progress
          ListTile(
            title: Text('Complete your profile'),
            subtitle: Row(
              children: [
                Text(
                  '2 of 4',
                  style: TextStyle(color: Color.fromARGB(255, 151, 240, 154)),
                ),
                Text(' COMPLETE'),
              ],
            ),
          ),
          //Add photo
          AddProfile(),
        ],
      ),
    );
  }
}

class ProfileDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(
        alignment: Alignment.topLeft,
        height: 100,
        width: 100,
        child: CircleAvatar(
            radius: 48,
            backgroundImage:
                NetworkImage('https://source.unsplash.com/random')),
      ),
      Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Martin Rankin',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text('Fashion Model'),
                  Text('I love to be kind'),
                  Text('From heaven'),
                  Text(
                    'Set more',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.bottomRight,
              child: SizedBox(
                width: 100,
                child: TextButton(
                  child: Text(
                    'Edit',
                    style: TextStyle(color: Colors.white),
                  ),
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    backgroundColor: Colors.blue,
                  ),
                  onPressed: () {},
                ),
              ),
            ),
          ],
        ),
      )
    ]);
  }
}

class Overview extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)),
        color: Color.fromARGB(255, 255, 255, 255),
        child: Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Text(
                    '15',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Posts',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    '486',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Followers',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    '58',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Following',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AddProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(children: [
        Padding(
          padding: EdgeInsets.all(10),
          child: Container(
            width: 200,
            height: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Color.fromARGB(255, 251, 250, 250),
              border: Border.all(
                width: 0.1,
              ),
            ),
            child: Column(children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      border: Border.all(width: 1), shape: BoxShape.circle),
                  child: Icon(Icons.person_outline),
                ),
              ),
              Text(
                'Add Profile Photo',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                '15th available, always a first text looks for.',
                textAlign: TextAlign.center,
              ),
              TextButton(
                style: TextButton.styleFrom(
                  backgroundColor: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                child: Text(
                  'Add Photo',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {},
              )
            ]),
          ),
        ),
        Padding(
          padding: EdgeInsets.all(10),
          child: Container(
            width: 200,
            height: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Color.fromARGB(255, 251, 250, 250),
              border: Border.all(
                width: 0.1,
              ),
            ),
            child: Column(children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      border: Border.all(width: 1), shape: BoxShape.circle),
                  child: Icon(Icons.chat_bubble_outline_rounded),
                ),
              ),
              Text(
                'Add Bubble',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                '15th available, always a first text looks for.',
                textAlign: TextAlign.center,
              ),
              TextButton(
                style: TextButton.styleFrom(
                  backgroundColor: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                child: Text(
                  'Add Buble',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {},
              )
            ]),
          ),
        ),
      ]),
    );
  }
}
