import 'package:flutter/material.dart';
import 'package:main/activities/home.dart';

import '../models/MessaggeResult.dart';
import '../models/User.dart';

class MySearchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MySearchPageState();
  }
}

List<User> recent = [
  new User(
      'Mikako',
      'https://thumbs.dreamstime.com/b/portrait-handsome-smiling-young-man-folded-arms-smiling-joyful-cheerful-men-crossed-hands-isolated-studio-shot-172869765.jpg',
      true),
  new User(
      "I'm cat",
      'https://thumbs.dreamstime.com/b/happy-cat-closeup-portrait-funny-smile-cardboard-young-blue-background-102078702.jpg',
      false),
  new User(
      'Mimamimo',
      'https://thumbs.dreamstime.com/b/portrait-cute-funny-raccoon-isolated-white-background-portrait-cute-funny-raccoon-141168093.jpg',
      false),
  new User(
      'Pony girl',
      'https://thumbs.dreamstime.com/b/happy-young-woman-smiling-girl-white-t-shirt-portrait-happy-young-woman-smiling-girl-white-t-shirt-portrait-136240232.jpg',
      true)
];
List<MessageResult> messageResults = [
  new MessageResult(
      'Florentino',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHI2VETqCsHvG9Kyg8nbrAbJgSsV6t0CzXsZ457BtHBQ&s',
      'Qua ghe gom',
      true),
  new MessageResult(
      'Tokuda',
      'https://thumbs.dreamstime.com/b/portrait-cute-funny-raccoon-isolated-white-background-portrait-cute-funny-raccoon-141168093.jpg',
      "I'm comming",
      false),
  new MessageResult(
      'Furry cat',
      'https://thumbs.dreamstime.com/b/happy-cat-closeup-portrait-funny-smile-cardboard-young-blue-background-102078702.jpg',
      'meow',
      false),
  new MessageResult(
      'Furry cat',
      'https://thumbs.dreamstime.com/b/happy-cat-closeup-portrait-funny-smile-cardboard-young-blue-background-102078702.jpg',
      'meow',
      false),
  new MessageResult(
      'Furry cat',
      'https://thumbs.dreamstime.com/b/happy-cat-closeup-portrait-funny-smile-cardboard-young-blue-background-102078702.jpg',
      'meow',
      false),
  new MessageResult(
      'Furry cat',
      'https://thumbs.dreamstime.com/b/happy-cat-closeup-portrait-funny-smile-cardboard-young-blue-background-102078702.jpg',
      'meow',
      false),
  new MessageResult(
      'Furry cat',
      'https://thumbs.dreamstime.com/b/happy-cat-closeup-portrait-funny-smile-cardboard-young-blue-background-102078702.jpg',
      'meow',
      false),
  new MessageResult(
      'Furry cat',
      'https://thumbs.dreamstime.com/b/happy-cat-closeup-portrait-funny-smile-cardboard-young-blue-background-102078702.jpg',
      'meow',
      false),
];

class _MySearchPageState extends State<MySearchPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          //Search bar
          SearchBar(),
          ListTile(
            leading: Text(
              'Recent',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            trailing: Text(
              'Clear',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.blue),
            ),
          ),
          //Recent search

          RecentSearch(),
          Padding(
            padding: EdgeInsets.only(left: 20),
            child: Container(
              alignment: Alignment.topLeft,
              child: Text(
                'Result',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
          ),
          //List result
          ListResult(),
        ],
      ),
    );
  }
}

class SearchBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 40, left: 20, right: 20),
      child: Container(
        color: Color.fromARGB(255, 244, 242, 242),
        child: TextField(
          decoration: InputDecoration(
            hintText: 'Search messages',
            prefixIcon: Icon(Icons.search),
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }
}

class RecentSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: Container(
        alignment: Alignment.topLeft,
        height: 96,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: List.generate(
              users.length,
              (index) {
                return Padding(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          CircleAvatar(
                            radius: 25,
                            backgroundImage: NetworkImage(recent[index].avatar),
                          ),
                          if (users[index].isOn == true)
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: Container(
                                height: 15,
                                width: 15,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.green),
                              ),
                            )
                        ],
                      ),
                      Text(recent[index].name),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

class ListResult extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ListResultState();
  }
}

class _ListResultState extends State<ListResult> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
        messageResults.length,
        (index) {
          return Container(
            height: 60,
            child: ListTile(
              trailing: SizedBox(
                width: 90,
                child: ElevatedButton(
                  child: Text(
                    messageResults[index].followed == true
                        ? 'Unfollow'
                        : 'Follow',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    setState(() {
                      messageResults[index].followed =
                          !messageResults[index].followed;
                    });
                  },
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    backgroundColor: messageResults[index].followed == true
                        ? Color.fromARGB(255, 171, 248, 127)
                        : Color.fromARGB(255, 61, 168, 255),
                    textStyle: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              title: Text(messageResults[index].name),
              subtitle: Text(messageResults[index].lastMessage),
              leading: CircleAvatar(
                  radius: 25,
                  backgroundImage: NetworkImage(messageResults[index].img)),
            ),
          );
        },
      ),
    );
  }
}
