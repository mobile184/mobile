import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../models/Comment.dart';
import '../models/Post.dart';
import '../models/User.dart';

List<User> users = [
  new User(
      'Ngô Diệu Huyền',
      'https://thumbs.dreamstime.com/b/happy-cat-closeup-portrait-funny-smile-cardboard-young-blue-background-102078702.jpg',
      true),
  new User(
      'Trần Quốc Huy',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTYQAIaz24z1xrKBUtBsX7Yyts6ZC3of6ymQ&usqp=CAU',
      false),
  new User(
      'Phạm Nhật Vượng',
      'https://thumbs.dreamstime.com/b/default-avatar-profile-image-vector-social-media-user-icon-potrait-182347582.jpg',
      false),
];

class MyHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          //user list section
          UserListSection(),
          Divider(),
          Expanded(child: ContentHomeSection()),
        ],
      ),
    );
  }
}

class UserListSection extends StatelessWidget {
  List<User> users = [
    new User(
        'Manamo',
        'https://thumbs.dreamstime.com/b/portrait-handsome-smiling-young-man-folded-arms-smiling-joyful-cheerful-men-crossed-hands-isolated-studio-shot-172869765.jpg',
        true),
    new User(
        "I'm cat",
        'https://thumbs.dreamstime.com/b/happy-cat-closeup-portrait-funny-smile-cardboard-young-blue-background-102078702.jpg',
        false),
    new User(
        'Mimamimo',
        'https://thumbs.dreamstime.com/b/portrait-cute-funny-raccoon-isolated-white-background-portrait-cute-funny-raccoon-141168093.jpg',
        false),
    new User(
        'Pony girl',
        'https://thumbs.dreamstime.com/b/happy-young-woman-smiling-girl-white-t-shirt-portrait-happy-young-woman-smiling-girl-white-t-shirt-portrait-136240232.jpg',
        true),
    new User(
        'Hello kitty',
        'https://thumbs.dreamstime.com/b/portrait-happy-family-garden-smiling-to-camera-34170692.jpg',
        true),
    new User(
        'Halida',
        'https://thumbs.dreamstime.com/b/portrait-white-rhinoceros-monochrome-square-lipped-ceratotherium-simum-largest-species-exists-has-95673125.jpg',
        true)
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(top: 30),
      height: 96,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: List.generate(
            users.length,
            (index) {
              return Padding(
                padding: EdgeInsets.all(5),
                child: Stack(children: [
                  CircleAvatar(
                    radius: 27,
                    backgroundImage: NetworkImage(users[index].avatar),
                  ),
                  if (users[index].isOn == true)
                    Positioned(
                      right: 0,
                      bottom: 0,
                      child: Container(
                        height: 15,
                        width: 15,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.green),
                      ),
                    )
                ]),
              );
            },
          ),
        ),
      ),
    );
  }
}

class ContentHomeSection extends StatelessWidget {
  List<Post> posts = [
    new Post(
        'https://thumbs.dreamstime.com/b/portrait-handsome-smiling-young-man-folded-arms-smiling-joyful-cheerful-men-crossed-hands-isolated-studio-shot-172869765.jpg',
        'Arnold Wilcox',
        'Surat, Gujarat',
        '12 min',
        'https://thumbs.dreamstime.com/b/fashion-model-lilac-flowers-young-woman-beautiful-long-dress-waving-wind-outdoor-beauty-portrait-blooming-garden-purple-191608268.jpg',
        ['https://picsum.photos/200', 'https://picsum.photos/203'],
        21,
        [
          new Comment('Nola Padilla', 'Humour, it use used middle.'),
          new Comment('Kristie', 'Senctence free churks is in.'),
        ]),
    new Post(
        'https://thumbs.dreamstime.com/b/google-logo-17835342.jpg',
        'Google',
        'Sponsored',
        'Yesterday',
        'https://thumbs.dreamstime.com/b/image-google-chrome-app-icon-loaded-laptop-screen-web-browser-google-chrome-app-icon-selective-focus-180826738.jpg',
        ['https://picsum.photos/20', 'https://picsum.photos/95'],
        100,
        [
          new Comment('Nola Padilla', 'Humour, it use used middle.'),
          new Comment('Kristie', 'Senctence free churks is in.'),
        ])
  ];
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: posts.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.only(left: 5, right: 5),
          child: Column(
            children: [
              ListTile(
                  leading: CircleAvatar(
                      radius: 25,
                      backgroundImage: NetworkImage(posts[index].avatar)),
                  title: Text(posts[index].name),
                  subtitle: Text(posts[index].title),
                  trailing: Text(posts[index].time_ago)),
              Container(
                height: 250,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(posts[index].image),
                  ),
                ),
              ),
              ListTile(
                leading: Stack(
                    children:
                        List.generate(posts[index].user_liked.length, (id) {
                  return Container(
                    margin: EdgeInsets.only(left: 15.0 * id),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(
                          'https://source.unsplash.com/random?sig=${id + index}'),
                    ),
                  );
                })),
                title: Text(
                  "${posts[index].likes} Like this",
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14),
                ),
                trailing: Icon(Icons.more_vert),
              ),
              Column(
                children: List.generate(posts[index].comments.length, (i) {
                  return Padding(
                    padding: EdgeInsets.only(left: 15),
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(right: 5),
                          child: Text(
                            posts[index].comments[i].name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                        ),
                        Text(posts[index].comments[i].comment),
                      ],
                    ),
                  );
                }),
              ),
              Divider(),
            ],
          ),
        );
      },
    );
  }
}
