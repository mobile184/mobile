import 'package:flutter/material.dart';
import 'package:main/activities/home.dart';
import 'package:main/activities/login.dart';
import 'package:main/activities/search.dart';

import 'activities/follow.dart';
import 'activities/profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          colorSchemeSeed: const Color(0xff6750a4), useMaterial3: true),
      home: MyLoginPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MainPageState();  
  }
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        bottomNavigationBar: Container(
          decoration:
              BoxDecoration(border: Border(top: BorderSide(width: 0.5))),
          child: Expanded(
            child: TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.home_outlined),
                ),
                Tab(
                  icon: Icon(Icons.search),
                ),
                Tab(
                  icon: Icon(Icons.timeline_outlined),
                ),
                Tab(
                  icon: Icon(Icons.person_outline),
                ),
              ],
              unselectedLabelColor: Colors.black,
              labelColor: Colors.blue,
            ),
          ),
        ),
        body: TabBarView(children: [
          MyHomePage(),
          MySearchPage(),
          MyFlowPage(),
          MyProfilePage(),
        ]),
      ),
    );
  }
}
