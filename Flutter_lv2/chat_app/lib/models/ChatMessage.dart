import 'package:flutter/material.dart';

enum MessageType { text, audio, image, video }

enum MessageStatus { not_sent, not_view, viewed }

class ChatMessage {
  final String text;
  final MessageType messageType;
  final MessageStatus messageStatus;
  final bool isSender;
  ChatMessage({
    required this.text,
    required this.messageType,
    required this.messageStatus,
    required this.isSender,
  });
}

List demoChatMessage = [
  ChatMessage(
      text: 'Hi Sajo',
      messageType: MessageType.text,
      messageStatus: MessageStatus.viewed,
      isSender: false),
  ChatMessage(
      text: 'Hello, how are u',
      messageType: MessageType.text,
      messageStatus: MessageStatus.viewed,
      isSender: true),
  ChatMessage(
      text: '',
      messageType: MessageType.audio,
      messageStatus: MessageStatus.viewed,
      isSender: false),
  ChatMessage(
      text: '',
      messageType: MessageType.video,
      messageStatus: MessageStatus.viewed,
      isSender: true),
  ChatMessage(
      text: 'Error happened',
      messageType: MessageType.text,
      messageStatus: MessageStatus.not_sent,
      isSender: true),
  ChatMessage(
      text: 'This looks great',
      messageType: MessageType.text,
      messageStatus: MessageStatus.viewed,
      isSender: false),
  ChatMessage(
      text: 'Glad you like it',
      messageType: MessageType.text,
      messageStatus: MessageStatus.not_view,
      isSender: true),
];
