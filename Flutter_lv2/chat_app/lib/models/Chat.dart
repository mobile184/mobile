class Chat {
  final String name, last_message, image, time;
  final bool isActive;

  Chat({
    required this.name,
    required this.last_message,
    required this.image,
    required this.time,
    required this.isActive,
  });
}

List chatsData = [
  Chat(
      name: 'Jenny Wilson',
      last_message: 'Hope you are doing well...',
      image: 'assets/images/user.png',
      time: '3m ago',
      isActive: false),
  Chat(
      name: 'Esther Howard',
      last_message: 'Hello Abdullar',
      image: 'assets/images/user_2.png',
      time: '8m ago',
      isActive: true),
  Chat(
      name: 'Esther Howard',
      last_message: 'Hello Abdullar',
      image: 'assets/images/user_3.png',
      time: '8m ago',
      isActive: true),
  Chat(
      name: 'Esther Howard',
      last_message: 'Hello Abdullar',
      image: 'assets/images/user_4.png',
      time: '8m ago',
      isActive: true),
  Chat(
      name: 'Esther Howard',
      last_message: 'Hello Abdullar',
      image: 'assets/images/user_5.png',
      time: '8m ago',
      isActive: true),
  Chat(
      name: 'Jenny Wilson',
      last_message: 'Hope you are doing well...',
      image: 'assets/images/user.png',
      time: '3m ago',
      isActive: false),
  Chat(
      name: 'Esther Howard',
      last_message: 'Hello Abdullar',
      image: 'assets/images/user_2.png',
      time: '8m ago',
      isActive: true),
  Chat(
      name: 'Esther Howard',
      last_message: 'Hello Abdullar',
      image: 'assets/images/user_3.png',
      time: '8m ago',
      isActive: true),
  Chat(
      name: 'Esther Howard',
      last_message: 'Hello Abdullar',
      image: 'assets/images/user_4.png',
      time: '8m ago',
      isActive: true),
  Chat(
      name: 'Esther Howard',
      last_message: 'Hello Abdullar',
      image: 'assets/images/user_5.png',
      time: '8m ago',
      isActive: true),
];
