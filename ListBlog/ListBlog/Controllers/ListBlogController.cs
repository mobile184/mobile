﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ListBlog.Data;
using ListBlog.Models;
using ListBlog.Repositores;

namespace ListBlog.Controllers
{
    public class ListBlogController : Controller
    {
        ///blog repository to do logic task
        BlogRepository blogRepository = new BlogRepository();
        //a blog sample to get full location and type list
        Blog modifyBlog = new Blog();

        /**home page with list of blogs contains [searchString]
        *@param searchString
        *return View
        */
        public ActionResult Index(string searchString)
        {
            return View(blogRepository.Get(searchString));
        }
       
        /**create page where you create a new blog
         * return View
         */
        public ActionResult Create()
        {
            ViewBag.locationList = modifyBlog.getFullLocationList();
            ViewBag.typeList = modifyBlog.getFullTypeList();
            return View();        
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        /**create a new blog with given blog data
         * @param blog
         * return Index View if data is valid or reload page if invalid
         */
        public ActionResult Create(Blog blog)
        {
            ViewBag.locationList = modifyBlog.getFullLocationList();
            ViewBag.typeList = modifyBlog.getFullTypeList();
            if (blogRepository.Create(blog))
            {
                RedirectToAction("Index");
            }
            return View(blog);
        }

        /**edit page where you can modify data of a blog from database
         * @param id
         * return View 
         */
        public ActionResult Edit(int? id)
        {
            ViewBag.locationList = modifyBlog.getFullLocationList();
            ViewBag.typeList = modifyBlog.getFullTypeList();
            if (blogRepository.GetBlog(id) == null) {
                return HttpNotFound();
            }
            return View(blogRepository.GetBlog(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        /**modify data of existed blog
         * @param blog
         * return Index View if data is valid or reload if invalid
         */
        public ActionResult Edit(Blog blog)
        {
            ViewBag.locationList = modifyBlog.getFullLocationList();
            ViewBag.typeList = modifyBlog.getFullTypeList();

            if (ModelState.IsValid)
            {
                if (blogRepository.Update(blog))
                {
                    return RedirectToAction("Index");
                }

            }
            return View(blog);
        }

        /**delete page where you confirm to delete blog
         * @param id
         * return View
         */
        public ActionResult Delete(int? id)
        {
            if (blogRepository.GetBlog(id) == null)
            {
                return HttpNotFound();
            }
            return View(blogRepository.GetBlog(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        /**delete a blog with given id
         * @param id
         * return Index View if success
         */
        public ActionResult Delete(int id)
        {
            blogRepository.Delete(id);
            Console.WriteLine("Success");
            return RedirectToAction("Index");
        }
    }
}
