﻿namespace ListBlog.Models
{
    using DocumentFormat.OpenXml.Drawing.ChartDrawing;
    using Microsoft.OData.Edm;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Drawing;
    [Table("Table")]
    public partial class Blog
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Mô tả ngắn")]
        public string description { get; set; }
        [Display(Name = "Chi tiết")]
        public string details { get; set; }
        [Display(Name = "Hình ảnh")]
        public string file { get; set; }
        [Required]
        [StringLength(20)]
        [Display(Name = "Tin")]
        public string news { get; set; }
        [Required]
        [Display(Name = "Loại")]
        public string type { get; set; }
        [Required]
        [Display(Name = "Trạng thái")]
        public bool state { get; set; }
        [Display(Name = "Địa điểm")]
        public string locations { get; set; }
        public List<string> locationList { get; set; }
        [Required(ErrorMessage ="Please choose time")]
        [Column(TypeName = "date")]
        [Display(Name = "Public")]
        public DateTime publicDate { get; set; }
        readonly List<string> fullLocationList = new List<string> { "Việt Nam", "Châu Á", "Châu Âu", "Châu Mỹ" };
        readonly List<string> fullTypeList = new List<string> { "Kinh Doanh", "Giải Trí", "Thế Giới", "Thời Sự" };
        //return full default location list
        public List<string> getFullLocationList()
        {
            return fullLocationList;
        }
        //return full default type list
        public List<string> getFullTypeList()
        {
            return fullTypeList;
        }
    }
}
