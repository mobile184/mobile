﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ListBlog.Data;
using ListBlog.Models;
namespace ListBlog.Repositores
{
    public class BlogRepository
    {
        //blog database 
        ListBlogContext db = new ListBlogContext(); 
        /**get list blog from database that contains [searchString] in news
         * @param searchString
         * return List<Blog>
         */
        public List<Blog> Get(string searchString)
        {
            var blogs = db.Blogs.ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                blogs = db.Blogs.Where(s => s.news.Contains(searchString)).ToList();
            }
            return blogs;
        }

        /**get data of a blog from database
         * @param id
         * return Blog
         */
        public Blog GetBlog(int? id)
        {
            return db.Blogs.Find(id);
        }

        /**create a blog 
         * @param blog
         * return bool
         */
        public bool Create(Blog blog)
        {
            blog.locations = String.Join(", ", blog.locationList);
            db.Blogs.Add(blog);
            db.SaveChanges();
            return true;
        }

        /**edit a blog from database with given data
         * @param blog
         * return bool
         */
        public bool Update(Blog blog)
        {

            blog.locations = String.Join(",", blog.locationList);
            db.Blogs.AddOrUpdate(blog);
            db.SaveChanges();

            return true;
        }

        /**delete a blog from database by id
         * @param id
         * return void
         */
        public void Delete(int id)
        {
            Blog blog = db.Blogs.Find(id);
            db.Blogs.Remove(blog);
            db.SaveChanges();
        }
    }
}
