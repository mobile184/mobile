import 'package:flutter/material.dart';

class MyClipper extends CustomClipper<Path> {
  final double distance;
  MyClipper({required this.distance});
  
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.addRect(Rect.fromLTWH(0, 0, size.width, size.height));
    path.moveTo(size.width, 0);
    path.lineTo(size.width - distance, size.height / 2);
    path.lineTo(size.width, size.height);
    return path;
  }
  
  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}