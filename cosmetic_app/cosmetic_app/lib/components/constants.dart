import 'package:flutter/material.dart';

const Color kPrimaryColor = Color.fromRGBO(0, 1, 2, 1);
const Color kSubTitleColor = Colors.black26;
const Color kBottomIcon = Color.fromRGBO(206, 27, 92, 1);