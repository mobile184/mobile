import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'constants.dart';

class MyAppBar extends StatelessWidget {
  final String title;
  final List<String> listAction;
  const MyAppBar({Key? key, required this.title, required this.listAction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: kPrimaryColor,
      title: Text(title, style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),),
    actions: [
      for (int i = 0; i < listAction.length; i++)
      Row(
        children: [
          SvgPicture.asset("assets/icons/${listAction[i]}.svg", width: 30, color: Colors.white,),
      SizedBox(width: 15,),
        ],
      ),

    ],
    );
  }
}