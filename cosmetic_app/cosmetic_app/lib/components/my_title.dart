import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class MyTitle extends StatelessWidget {
  final String title;
  final String subTitle;
  final double fontSize;
  const MyTitle({Key? key, required this.title, this.subTitle = "", this.fontSize = 18}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
          mainAxisAlignment: subTitle != "" ? MainAxisAlignment.spaceBetween : MainAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(top: 10, left: 10, bottom: 10),
              child: Text(title, style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.bold),)),
              if (subTitle != "")
            Container(
              padding: EdgeInsets.only(top: 10, right: 10, bottom: 10),
              child: Text(subTitle, style: TextStyle(fontSize: 12, color: Colors.pink),)),
          ],
        );
  }
}