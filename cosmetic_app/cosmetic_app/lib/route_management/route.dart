const String kHomePage = "/home";
const String kCategoriesPage = "/categories";
const String kCategoryPage = "/categories/category";
const String kCartPage = "/cart";
const String kProfilePage = "/profile";
const String kWelcomPage = "/";
const String kDetailPage = "/detail";