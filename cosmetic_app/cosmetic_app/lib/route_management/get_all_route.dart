import 'package:cosmetic_app/route_management/route.dart';
import 'package:get/get.dart';

import '../pages/categories_page/categories_page.dart';
import '../pages/category_page/category_page.dart';
import '../pages/detail_page/detail_page.dart';
import '../pages/home_page/home_page.dart';
import '../pages/welcome_page/welcome_page.dart';

class GetAllPage {
  /** get list of page for routing
   * @param null
   * return List<GetPage>
   */
  static List<GetPage> getAllPages() {
    return [
      GetPage(name: kWelcomPage, page: () => WelcomePage()),
      GetPage(name: kHomePage, page: () => HomePage()),
      GetPage(name: kDetailPage, page: () => DetailPage()),
      GetPage(name: kCategoriesPage, page: () => CategoriesPage())
    ];
  }
}
