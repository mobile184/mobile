import 'package:cosmetic_app/components/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../detail_page/detail_page.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var data = Get.arguments;

  late List listCategory;

  void sortListCategory() {
    setState(() {
      listCategory.sort((a, b) => a.price.compareTo(b.price));
    });
  }
  int calTotalReviews(List rate) {
    int total = 0;
    for (int i = 0; i < rate.length; i++) {
      total += rate[i] as int;
    }
    return total;
  }

  double calTotalRate(List rate) {
    int totalRate = 0;
    for (int i = 0; i < rate.length; i++) {
      totalRate += rate[i] * (i + 1) as int;
    }
    return totalRate / calTotalReviews(rate);
  }

  @override
  void initState() {
    listCategory = data["listCategory"];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 0,
          bottom: 50,
          child: SingleChildScrollView(
            child: Container(
              width: Get.width,
              padding: const EdgeInsets.only(top: 10, left: 10, bottom: 10),
              child: Wrap(
                spacing: 10,
                runSpacing: 10,
                children: [
                  for (int i = 0; i < listCategory.length; i++) CategoryItem(i)
                ],
              ),
            ),
          ),
        ),
        Positioned(bottom: 0, left: 0, child: BottomSortBar())
      ],
    );
  }

  Widget BottomSortBar() {
    return Container(
      width: Get.width,
      height: 50,
      decoration: const BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(color: Colors.black12, spreadRadius: 1, blurRadius: 10)
      ]),
      child: Row(
        children: [
          GestureDetector(
            onTap: () {
              sortListCategory();
            },
            child: Container(
              width: Get.width * 0.45,
              child: Row(mainAxisAlignment: MainAxisAlignment.center, children: const [
                Icon(
                  Icons.swap_vert,
                  size: 20,
                  color: Colors.black,
                ),
                Text(
                  "Sort",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                )
              ]),
            ),
          ),
          Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: const VerticalDivider(
                thickness: 1,
              )),
          Container(
            width: Get.width * 0.45,
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: const [
              Icon(
                Icons.filter_alt_outlined,
                size: 20,
                color: Colors.black,
              ),
              Text(
                "Filter",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
              )
            ]),
          ),
        ],
      ),
    );
  }

  Widget CategoryItem(int i) {
    return GestureDetector(
      onTap: () {
        Get.to(() => DetailPage(), arguments: {
          "image": listCategory[i].image,
          "name": listCategory[i].name,
          "subTitle": listCategory[i].subTitle,
          "price": listCategory[i].price,
          "shades": listCategory[i].shades,
          "description": listCategory[i].description,
          "ingredient": listCategory[i].ingredient,
          "sale": listCategory[i].sale,
          "rate": listCategory[i].rate,
          "reviews": listCategory[i].reviews,
        });
      },
      child: Container(
        width: Get.width / 2 - 15,
        height: 400,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: Colors.white),
        child: Stack(
          children: [
            Container(
              height: 220,
              width: Get.width / 2 - 15,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network(
                  listCategory[i].image[0],
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              width: Get.width / 2 - 15,
              top: 220,
              height: 170,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    listCategory[i].name,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                    ),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: listCategory[i].shades.length != 1
                                ? Colors.pink
                                : Colors.white),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      if (listCategory[i].shades.length != 1)
                        Text(
                          "${listCategory[i].shades.length} shades",
                          style: TextStyle(color: Colors.black26, fontSize: 12),
                        ),
                    ],
                  ),
                  RichText(
                      text: TextSpan(children: [
                    if (listCategory[i].sale != 0)
                      TextSpan(
                          text: "\$${listCategory[i].price.toStringAsFixed(2)}",
                          style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                              color: Colors.black26,
                              fontSize: 12)),
                    TextSpan(
                        text:
                            " \$${listCategory[i].sale != 0 ? (listCategory[i].price * (100 - listCategory[i].sale) / 100).toStringAsFixed(2) : (listCategory[i].price.toStringAsFixed(2))} ",
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Colors.black)),
                    if (listCategory[i].sale != 0)
                      TextSpan(
                          text: "(${listCategory[i].sale}% Off)",
                          style: TextStyle(color: Colors.pink, fontSize: 12))
                  ])),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.star,
                        color: kPrimaryColor,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "${calTotalRate(listCategory[i].rate).toStringAsFixed(1)} (${calTotalReviews(listCategory[i].rate)})",
                        style: TextStyle(color: Colors.black26, fontSize: 12),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(width: 1)),
                        padding: EdgeInsets.all(5),
                        child: SvgPicture.asset(
                          "assets/icons/favourite.svg",
                          width: 20,
                        ),
                      ),
                      Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: kPrimaryColor),
                          padding:
                              EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                          child: Text(
                            "CHOOSE PRODUCTS",
                            style: TextStyle(color: Colors.white, fontSize: 12),
                          ))
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
