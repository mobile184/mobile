import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../../main_page.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  void initState() {
    Timer(Duration(seconds: 3), () {
      Get.to(() => MainPage());
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircleAvatar(backgroundImage: NetworkImage("https://thumbs.dreamstime.com/b/pretty-model-using-cosmetics-stone-roller-face-massage-woman-caring-her-beauty-face-cosmetic-tool-skin-care-concept-213963195.jpg"),
        radius: 50,),
      ),
    );
  }
}