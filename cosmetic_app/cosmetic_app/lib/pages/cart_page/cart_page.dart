import 'package:cosmetic_app/components/constants.dart';
import 'package:cosmetic_app/components/my_title.dart';
import 'package:cosmetic_app/models/bonus_offers.dart';
import 'package:cosmetic_app/models/cart_item.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_number_picker/flutter_number_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../models/best_seller.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  void addToCart(data) {
    setState(() {
      if (!listCartItem.contains(data))
      {
      listCartItem.add(Product(image: data.image,
                                name: data.name,
                                subTitle: data.subTitle,
                                price: data.price,
                                shades: data.shades,
                                description: data.description,
                                ingredient: data.ingredient,
                                sale: data.sale,
                                rate: data.rate,
                                reviews: data.reviews,));
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 0,
          bottom: Get.height * 0.11,
          child: Container(
            width: Get.width,
            height: Get.height * 0.85,
            decoration: const BoxDecoration(color: Colors.white70),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TotalSaving(),
                  for (int i = 0; i < listCartItem.length; i++) ItemInCart(data: listCartItem[i]),
                  const MyTitle(title: "BONUS OFFERS"),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          for (int i = 0; i < listBonusOffers.length; i++) BonusOfferItem(data: listBonusOffers[i])
                        ],
                      ),
                    ),
                  ),
                  const MyTitle(title: "APPLIED COUPON"),
                  CouponBar(),
                  const MyTitle(title: "PRICE DETAILS"),
                  Bill()
                ],
              ),
            ),
          ),
        ),
        Positioned(
            bottom: 0,
            child: Container(
              width: Get.width,
              height: Get.height * 0.11,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12, spreadRadius: 1, blurRadius: 10),
                  ],
                  color: Colors.white),
              child: PayBar(),
            ))
      ],
    );
  }

  Widget Bill() {
    return Container(
                  width: Get.width - 20,
                  padding: const EdgeInsets.only(top: 20, bottom: 10),
                  margin: const EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    children: [
                      BillCount(
                          title: "Subtotal",
                          cost: 699,
                          color: Colors.black26),
                      BillCount(
                          title: "Tax", cost: 106.63, color: Colors.black26),
                      BillCount(
                          title: "Discount", cost: 0, color: Colors.green),
                      BillCount(
                          title: "Loyalty Discount",
                          cost: 0,
                          color: Colors.green,
                          note: true),
                      BillCount(
                          title: "Shipping",
                          cost: 0,
                          color: Colors.green,
                          isFree: true),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                            child: const DottedLine(direction: Axis.horizontal,
                            dashLength: 3,
                            dashGapLength: 2,
                            dashColor: Colors.black26,),
                          ),
                          BillCount(title: "Total", cost: 699, color: Colors.black, fontSize: 18, isBold: true)
                    ],
                  ),
                );
  }

  Widget BillCount(
      {required String title,
      required double cost,
      required Color color,
      bool note = false,
      bool isFree = false,
      double fontSize = 15,
      bool isBold = false}) {
    return Container(
      width: Get.width - 20,
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            title,
            style: TextStyle(
                color: isBold == true ? Colors.black : Colors.black26,
                fontSize: fontSize,
                fontWeight:
                    isBold == true ? FontWeight.w500 : FontWeight.normal),
          ),
          const SizedBox(
            width: 10,
          ),
          if (note == true)
            const Icon(
              Icons.info_outline,
              color: Colors.red,
              size: 20,
            ),
          const Spacer(),
          isFree == true
              ? Text(
                  "Free",
                  style: TextStyle(
                      color: color,
                      fontSize: fontSize,
                      fontWeight:
                          isBold == true ? FontWeight.w500 : FontWeight.normal),
                )
              : Text(
                  "\$${cost.toStringAsFixed(2)}",
                  style: TextStyle(
                      color: color,
                      fontSize: fontSize,
                      fontWeight:
                          isBold == true ? FontWeight.w500 : FontWeight.normal),
                ),
        ],
      ),
    );
  }

  Widget CouponBar() {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      width: Get.width - 20,
      height: 140,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            child: SizedBox(
              height: 80,
              width: Get.width - 20,
              child: Image.network(
                "https://thumbs.dreamstime.com/b/abstract-red-pink-color-strip-paper-background-248339201.jpg",
                fit: BoxFit.cover,
              ),
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                padding: const EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                width: Get.width * 0.6,
                child: const TextField(
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Enter Gift code or discount code",
                      hintStyle: TextStyle(
                        color: Colors.black54,
                        fontSize: 12,
                      )),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
                  decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(10)),
                  child: const Text(
                    "Apply",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            top: 70,
            left: Get.width / 2 - 10,
            child: Container(
                clipBehavior: Clip.none,
                child: const Text(
                  "Or",
                  style: TextStyle(
                      color: Colors.lightGreen,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                )),
          ),
          Positioned(
              top: 80,
              child: Container(
                width: Get.width - 20,
                height: 60,
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                child: Row(children: [
                  SvgPicture.asset(
                    "assets/icons/offer.svg",
                    width: 30,
                    height: 30,
                    color: Colors.red,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  const Text(
                    "View All Offers",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                  ),
                  const Spacer(),
                  GestureDetector(
                      onTap: () {},
                      child: const Icon(
                        Icons.arrow_forward_ios,
                        size: 16,
                      )),
                ]),
              ))
        ],
      ),
    );
  }

  Widget BonusOfferItem({required Product data}) {
    return Container(
      margin: EdgeInsets.only(right: 10),
      width: Get.width * 0.4,
      height: 310,
      decoration: BoxDecoration(
          border: Border.all(width: 0.1),
          boxShadow: [
            BoxShadow(spreadRadius: 1, blurRadius: 1, color: Colors.black12),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Container(
            width: Get.width * 0.4,
            height: 180,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                data.image[0],
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Column(
              children: [
                Text(
                  data.description,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      "\$${data.price.toStringAsFixed(2)}",
                      style: TextStyle(
                          decoration: TextDecoration.lineThrough,
                          color: Colors.black54),
                    ),
                    Text(
                      "\$${(data.price * (100 - data.sale) / 100).toStringAsFixed(2)}",
                      style: TextStyle(fontSize: 15),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () => addToCart(data),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Text(
                      "ADD TO CART",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Container ItemInCart({required data}) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.symmetric(vertical: 10),
      width: Get.width - 20,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          Container(
            width: Get.width * 0.1,
            height: Get.height * 0.1,
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: Image.network(
              data.image[0],
              fit: BoxFit.contain,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                data.name,
                style: TextStyle(fontSize: 13),
              ),
              Text(
                data.subTitle,
                style: TextStyle(color: Colors.black26, fontSize: 12),
              ),
              Container(
                width: Get.width * 0.7,
                padding: EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "\$${(data.price * (100 - data.sale) / 100).toStringAsFixed(2)}",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                    ),
                    Spacer(),
                    Icon(
                      Icons.delete,
                      size: 20,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    CustomNumberPicker(
                      step: 1,
                      initialValue: 1,
                      minValue: 1,
                      maxValue: 10,
                      onValue: (value) {
                        setState(() {});
                      },
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Container TotalSaving() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(width: 0.3, color: Colors.amber)),
      width: Get.width - 20,
      height: Get.height * 0.09,
      child: Stack(
        children: [
          Container(
            width: Get.width - 20,
            height: Get.height * 0.09,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Image.network(
                "https://thumbs.dreamstime.com/b/celebration-background-golden-champagne-bottle-confetti-stars-party-streamers-christmas-birthday-wedding-flat-lay-161039153.jpg",
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
              child: Container(
                  height: Get.height * 0.09,
                  width: Get.width * 0.5,
                  child: Center(
                      child: Text(
                    "\$599.00 total savings",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ))))
        ],
      ),
    );
  }

  Row PayBar() {
    return Row(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          width: Get.width * 0.45,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [Text("Pay Using"), Icon(Icons.keyboard_arrow_up)],
              ),
              Text(
                "Select Payment Method",
                style: TextStyle(fontSize: 12),
              )
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            showDialog(context: context, builder: (context) {
              return AlertDialog(
                title: Column(
                  children: [
                    Text("Payment Successful", textAlign: TextAlign.center,),
                    SizedBox(height: 10,),
                    Container(
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(color: Colors.black26, shape: BoxShape.circle),
                      child: Icon(Icons.check, size: 50, color: Colors.white,))
                  ],
                ),
                content: Text("Your bill has been paid successfully", textAlign: TextAlign.center,),
                actions: [Center(child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(color: Colors.blue, borderRadius: BorderRadius.circular(20)),
                  child: TextButton(onPressed: () => Get.back(), child: Text("OK", style: TextStyle(color: Colors.white),))))], 
              );
            });
          },
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: kPrimaryColor),
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                    text: "\$699",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                  TextSpan(
                    text: "  PLACE ORDER",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ]),
              )),
        ),
      ],
    );
  }
}
