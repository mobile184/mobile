import 'dart:ffi';

import 'package:carousel_indicator/carousel_indicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cosmetic_app/components/constants.dart';
import 'package:cosmetic_app/components/my_title.dart';
import 'package:cosmetic_app/models/cart_item.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../models/best_seller.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var data = Get.arguments;
  int _curIndex = 0;
  int _curShade = 0;
  void addToCart(data) {
    setState(() {
      listCartItem.add(Product(image: data["image"],
                                name: data["name"],
                                subTitle: data["subTitle"],
                                price: data["price"],
                                shades: data["shades"],
                                description: data["description"],
                                ingredient: data["ingredient"],
                                sale: data["sale"],
                                rate: data["rate"],
                                reviews: data["reviews"],));
    });
  }
  int calTotalReviews() {
    int total = 0;
    for (int i = 0; i < data["rate"].length; i++)
    total += data["rate"]![i] as int;
    return total;
  }
  double calTotalRate() {
    int totalRate = 0;
    for (int i = 0; i < data["rate"].length; i++)
    totalRate += data["rate"]![i] * (i + 1) as int;
    return totalRate / calTotalReviews();
  } 
  void _changeSlider(int index) {
    setState(() {
      _curIndex = index;
    });
  }

  void _changeShade(int index) {
    setState(() {
      _curShade = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: Get.width,
          height: Get.height,
          decoration: BoxDecoration(color: Colors.white70),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                ItemOverView(data["image"]),
                ShadeList(),
                MyTitle(title: "DESCRIPTIONS"),
                DescriptionDetail(),
                MyTitle(title: "INGREDIENT"),
                IngredientDetail(),
                MyTitle(
                  title: "CUSTOMER REVIEWS",
                  subTitle: "WRITE A REVIEW",
                ),
                ReviewItem(),
                Footer()
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 16,
          width: Get.width,
          child: CartBottomButton(),
        )
      ],
    );
  }

  Widget ReviewItem() {
    return Container(
                width: Get.width - 20,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 1,
                          blurRadius: 10)
                    ]),
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          width: Get.width * 0.35,
                          child: Column(
                            children: [
                              Text(
                                calTotalRate().toStringAsFixed(2),
                                style: TextStyle(
                                    fontSize: 32, fontWeight: FontWeight.bold),
                              ),
                              AbsorbPointer(
                                absorbing: true,
                                child: RatingBar.builder(
                                  initialRating: calTotalRate(),
                                  itemSize: 16,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: true,
                                  itemCount: 5,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: kPrimaryColor,
                                  ),
                                  onRatingUpdate: (rating) {
                                    print(rating);
                                  },
                                ),
                              ),
                              SizedBox(height: 10,),
                              Text("${calTotalReviews()} REVIEWS", style: TextStyle(fontSize: 14, color: Colors.black54),)
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                                for (int i = 0; i < data["rate"].length; i++)
                            Container(
                              padding: EdgeInsets.only(bottom: 5),
                              child: Row(
                                children: [
                                  Text("$i", style: TextStyle(fontSize: 12),),
                                  SizedBox(width: 5,),
                                  Icon(Icons.star, color: Colors.black54, size: 20),
                                  SizedBox(width: 5,),
                                  Stack(
                                    children: [
                                      Container(
                                        width: Get.width * 0.35,
                                        height: 10,
                                        decoration: BoxDecoration(color: Colors.black54),
                                      ),
                                      Container(
                                          width: Get.width * 0.35 * (data["rate"][i] / calTotalReviews()),
                                          height: 10,
                                          decoration: BoxDecoration(color: Colors.black)),
                                    ],
                                  ),
                                  SizedBox(width: 5,),
                                  Text(data["rate"][i].toString(), style: TextStyle(fontSize: 12),)
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    DottedLine(
                      direction: Axis.horizontal,
                      dashColor: Colors.black26,
                    ),
                    MyTitle(title: "CUSTOMER REVIEWS", subTitle: "See All Reviews", fontSize: 14,),
                    for (int i = 0; i < data["reviews"].length; i ++)
                    CustomerReview(i)
                  ],
                ),
              );
  }

  Container CustomerReview(int i) {
    return Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(color: Colors.white70, borderRadius: BorderRadius.circular(10),
                      boxShadow: [BoxShadow(color: Colors.black12, spreadRadius: 1, blurRadius: 10)]),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                            child: Row(
                              children: [
                              CircleAvatar(
                                backgroundColor: Colors.pink,
                                radius: 25,
                                child: Text(data["reviews"][i].short_name, style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),),
                              ),
                              SizedBox(width: 10,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(data["reviews"][i].name, style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                                  AbsorbPointer(
                                    absorbing: true,
                                    child: RatingBar.builder(
                                    initialRating: data["reviews"][i].rate,
                                    itemSize: 12,
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemPadding:
                                        EdgeInsets.symmetric(horizontal: 4.0),
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: kPrimaryColor,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                                                  ),
                                  ),
                                ],
                              ),
                              Spacer(),
                              Container(
                                height: 40,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(data["reviews"][i].date, style: TextStyle(fontSize: 12, color: Colors.black54),),
                                  ],
                                ),
                              )
                            ],),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 10, bottom: 10),
                            child: Text(data["reviews"][i].review)),
                          ]
                        ),
                      ),
                    );
  }

  Widget DescriptionDetail() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      width: Get.width - 20,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: Colors.black12, spreadRadius: 1, blurRadius: 10)
          ],
          color: Colors.white),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(data["description"], style: TextStyle(fontSize: 14,),),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "+ More",
                style: TextStyle(color: Colors.pink),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget IngredientDetail() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      width: Get.width - 20,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: Colors.black12, spreadRadius: 1, blurRadius: 10)
          ],
          color: Colors.white),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(data["ingredient"], style: TextStyle(fontSize: 14,),),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "+ More",
                style: TextStyle(color: Colors.pink),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget Footer() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
          width: Get.width,
          decoration: BoxDecoration(color: Colors.white),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(Icons.delivery_dining, size: 30, color: Colors.black,),
              Text("Fast Delivery"),
              Spacer(),
              Icon(Icons.high_quality, size: 30, color: Colors.black,),
              Text("Quality Ensurance"),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          width: Get.width,
          height: Get.height * 0.2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "View more products in our app store",
                style: TextStyle(),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Container ShadeList() {
    return Container(
      width: Get.width - 20,
      padding: EdgeInsets.only(top: 20, bottom: 16, left: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Row(
        children: [
          Container(
            width: Get.width * 0.7,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(children: [
                for (int i = 0; i < data["shades"].length; i++)
                  Container(
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                            width: 2,
                            color:
                                _curShade == i ? Colors.black : Colors.white)),
                    child: GestureDetector(
                      onTap: () {
                        _changeShade(i);
                      },
                      child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                              color: data["shades"][i],
                              borderRadius: BorderRadius.circular(10))),
                    ),
                  )
              ]),
            ),
          ),
          Container(
            width: Get.width * 0.2,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              "+${data["shades"].length} shades",
              style: TextStyle(color: Colors.pink, fontSize: 16),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Container ItemOverView(List<String> listSlider) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      width: Get.width - 20,
      height: Get.height * 0.6,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Stack(children: [
        Positioned(
          top: 16,
          left: (Get.width - 20) / 2 - Get.width * 0.25,
          width: Get.width * 0.5,
          child: Column(
            children: [
              CarouselSlider(
                options: CarouselOptions(
                    autoPlay: true,
                    aspectRatio: 0.8,
                    viewportFraction: 1,
                    height: Get.height * 0.35,
                    onPageChanged: (index, reason) {
                      _changeSlider(index);
                    }),
                items: listSlider.map((data) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(horizontal: 5.0),
                          decoration: BoxDecoration(color: Colors.amber),
                          child: Image.network(
                            data,
                            fit: BoxFit.cover,
                          ));
                    },
                  );
                }).toList(),
              ),
              CarouselIndicator(
                index: _curIndex,
                count: listSlider.length,
                color: Colors.black12,
                activeColor: Colors.red,
              ),
            ],
          ),
        ),
        Positioned(
          top: 10,
          right: 10,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 0.1),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(children: [
              Icon(
                Icons.star,
                size: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "${calTotalRate().toStringAsFixed(2)} (${calTotalReviews()})",
                style: TextStyle(fontSize: 12),
              )
            ]),
          ),
        ),
        Positioned(
            bottom: 0,
            child: Container(
              height: Get.height * 0.2,
              width: Get.width - 20,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    data["name"],
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    data["subTitle"],
                    style: TextStyle(fontSize: 14, color: Colors.black54),
                    textAlign: TextAlign.center,
                  ),
                  DottedLine(
                    direction: Axis.horizontal,
                    dashColor: Colors.black26,
                  ),
                  Text(
                    "\$${(data["price"] * (100 - data["sale"]) / 100).toStringAsFixed(2)}",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ))
      ]),
    );
  }

  Row CartBottomButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(width: 1),
              color: Colors.white),
          padding: EdgeInsets.all(5),
          child: SvgPicture.asset(
            "assets/icons/favourite.svg",
            width: 30,
          ),
        ),
        SizedBox(
          width: 10,
        ),
        GestureDetector(
          onTap: () => addToCart(data),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 11, horizontal: 30),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5), color: kPrimaryColor),
            child: Text(
              "ADD TO CART",
              style: TextStyle(
                  color: Colors.white, fontSize: 16, fontWeight: FontWeight.w400),
            ),
          ),
        )
      ],
    );
  }
}
