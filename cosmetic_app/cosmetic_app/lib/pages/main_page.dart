import 'package:cosmetic_app/components/constants.dart';
import 'package:cosmetic_app/pages/cart_page/cart_page.dart';
import 'package:cosmetic_app/pages/categories_page/categories_page.dart';
import 'package:cosmetic_app/pages/profile_page/profile_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'home_page/home_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

List<Widget> _listBody = [
  HomePage(),
  CategoriesPage(),
  CartPage(),
  ProfilePage()
];

class _MainPageState extends State<MainPage> {
  int _curIndex = 0;
  _onTap(int index) {
    setState(() {
      _curIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white70,
      body: _listBody.elementAt(_curIndex),
      bottomNavigationBar: MyBottomBar(),
    ));
  }


  Widget MyBottomBar() {
    return BottomNavigationBar(
        currentIndex: _curIndex,
        onTap: _onTap,
        unselectedItemColor: Colors.white,
        selectedItemColor: kBottomIcon,
        type: BottomNavigationBarType.fixed,
        backgroundColor: kPrimaryColor,
        items: [
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                "assets/icons/home.svg",
                width: 20,
                color: _curIndex == 0 ? kBottomIcon : Colors.white,
              ),
              label: "Home"),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                "assets/icons/category.svg",
                width: 20,
                color: _curIndex == 1 ? kBottomIcon : Colors.white,
              ),
              label: "Categories"),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                "assets/icons/bag.svg",
                width: 20,
                color: _curIndex == 2 ? kBottomIcon : Colors.white,
              ),
              label: "Cart"),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                "assets/icons/user.svg",
                width: 20,
                color: _curIndex == 3 ? kBottomIcon : Colors.white,
              ),
              label: "Profile"),
        ]);
  }
}
