import 'package:cosmetic_app/models/categories.dart';
import 'package:cosmetic_app/models/category.dart';
import 'package:cosmetic_app/pages/category_page/category_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class CategoriesPage extends StatelessWidget {
  const CategoriesPage({Key? key}) : super(key: key);

  List getListByTitle(title) {
    switch(title) {
          case "LIPS": return listLips;
          case "EYES": return listEyes;
          case "FACE": return listFace;
          case "HAIR": return listHair;
          case "SKINCARE": return listSkincare;
          case "NAIL": return listNail;
        }
        return listLips;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height,
      decoration: BoxDecoration(color: Colors.white),
      child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              for (int i = 0; i < listCategories.length; i++)
                Category(i, title: listCategories[i].name, image: listCategories[i].image)
            ],
          )),
    );
  }

  Widget Category(int i, {required String title, required String image}) {
    return GestureDetector(
      onTap: () {
        Get.to(() => CategoryPage(title: title,), arguments: {"listCategory": getListByTitle(title)
        });
      },
      child: Container(
                  height: Get.height * 0.15,
                  width: double.maxFinite,
                  margin: EdgeInsets.all(5),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Stack(children: [
                      Image.network(
                        image,
                        width: double.maxFinite,
                        fit: BoxFit.cover,
                      ),
                      i % 2 == 0
                          ? Positioned(
                              left: 10,
                              width: Get.width * 0.15,
                              height: Get.height * 0.15,
                              child: Center(
                                child: Text(
                                  title,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            )
                          : Positioned(
                              right: 10,
                              width: Get.width * 0.15,
                              height: Get.height * 0.15,
                              child: Center(
                                child: Text(
                                  title,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            )
                    ]),
                  ),
                ),
    );
  }
}
