import 'package:cosmetic_app/components/constants.dart';
import 'package:cosmetic_app/components/my_title.dart';
import 'package:cosmetic_app/models/frequently_search.dart';
import 'package:cosmetic_app/models/hot_pick.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [
        MySeachBar(),
        Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(children: [
              SizedBox(
                height: 32,
              ),
              MyTitle(title: "HOT PICKS"),
              Container(
                width: Get.width - 20,
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(children: [
                    for (int i = 0; i < listHotPick.length; i++) HotPickItem(i)
                  ]),
                ),
              ),
              MyTitle(title: "FREQUENTLY SEARCHED"),
              Container(
                width: Get.width - 20,
                margin: EdgeInsets.symmetric(horizontal: 10),
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12,
                          spreadRadius: 1,
                          blurRadius: 10)
                    ]),
                child: Wrap(children: [
                  for (int i = 0; i < listFrequentlySearch.length; i++)
                    Container(
                      margin: EdgeInsets.only(right: 10, bottom: 16),
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                      decoration: BoxDecoration(
                          color: Colors.black12,
                          borderRadius: BorderRadius.circular(20)),
                      child: Text(
                        listFrequentlySearch[i],
                      ),
                    )
                ]),
              )
            ]),
          ),
        ),
      ]),
    );
  }

  Container HotPickItem(int i) {
    return Container(
      margin: EdgeInsets.only(right: 10),
      child: Column(children: [
        Container(
          width: Get.width * 0.25,
          height: Get.width * 0.25,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.network(
              listHotPick[i].image,
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          listHotPick[i].name,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 13, fontWeight: FontWeight.w400),
        ),
      ]),
    );
  }

  Container MySeachBar() {
    return Container(
      height: Get.height * 0.08,
      decoration: BoxDecoration(color: kPrimaryColor),
      child: Row(
        children: [
          GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                  size: 30,
                )),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: Get.width * 0.8,
            decoration: BoxDecoration(
              color: Colors.white24,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(width: 2, color: Colors.white54),
            ),
            child: TextField(
              autofocus: true,
              decoration: InputDecoration(
                  isDense: true,
                  border: InputBorder.none,
                  hintText: "Search...",
                  hintStyle: TextStyle(color: Colors.white, fontSize: 16)),
            ),
          )
        ],
      ),
    );
  }
}
