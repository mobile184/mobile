import 'package:carousel_indicator/carousel_indicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cosmetic_app/components/clipper.dart';
import 'package:cosmetic_app/models/best_seller.dart';
import 'package:cosmetic_app/models/shortcut.dart';
import 'package:cosmetic_app/pages/detail_page/detail_page.dart';
import 'package:flutter/material.dart';
import 'package:cosmetic_app/components/my_title.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../search_page/search_page.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
    int _curIndex = 0;
    void _changeSlider(int index) {
    setState(() {
      _curIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<String> listHotDeals = ["https://thumbs.dreamstime.com/b/black-friday-sale-escalator-shopping-mall-moving-motion-world-hot-deal-132909399.jpg",
    "https://thumbs.dreamstime.com/b/piece-paper-sticks-out-his-shirt-pocket-inscription-hot-deal-business-finance-concept-225684804.jpg",
    "https://thumbs.dreamstime.com/b/hot-deal-offer-advertising-flag-escalator-29031556.jpg"];
    List<String> listSlider = [
      "https://thumbs.dreamstime.com/b/beauty-makeup-nail-art-concept-nai-beautiful-fashion-model-woman-smoky-eye-foundation-unblemished-skin-trendy-60284830.jpg",
      "https://thumbs.dreamstime.com/b/fashion-photo-young-magnificent-woman-hat-girl-posing-studio-perfect-makeup-43190752.jpg",
      "https://thumbs.dreamstime.com/b/portrait-beautiful-sensual-woman-elegant-hairstyle-per-perfect-makeup-fashion-photo-39301993.jpg"
    ];
    return Container(
      height: double.maxFinite,
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: SingleChildScrollView(
          child: Column(
        children: [
          SearchBar(),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ...listShortcut.map((data) =>
                  ShortcutItem(data),
                ).toList(),
              ],
            ),
          ),
          Slider(listSlider: listSlider),
          MyTitle(title: "BEST SELLERS"),
          Container(
            child: Stack(children: [
              Image.network("https://thumbs.dreamstime.com/b/abstract-watercolor-background-painted-blue-mixed-green-39411635.jpg",
              ),
              Positioned(
                left: 0,
                right: 0,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        height: Get.height * 0.38,
                        width: Get.width * 0.4,
                        child: Center(child: Text("GRAB OUR BEST SELLING PRODUCTS", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,)),
                      ),
                      for (int i = 0; i < listBestSeller.length; i++)
                      BestSellerItem(listBestSeller[i])
                    ],
                  ),
                ),
              )
            ]),
          ),
          MyTitle(title: "HOT DEALS"),
          Container(
            width: Get.width,
            height: Get.height * 0.25,
            padding: EdgeInsets.only(left: 10, bottom: 16),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(children: [
                ...listHotDeals.map((data) => 
                Container(
                  margin: EdgeInsets.only(right: 10),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(data,
                    fit: BoxFit.cover,),
                  ),
                )).toList(),
                
              ]),
            ),
          )
        ],
      )),
    );
  }

  Container BestSellerItem(Product data) {
    return Container(
                      height: Get.height * 0.38,
                      margin: EdgeInsets.symmetric(vertical: 40, horizontal: 10),
                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                      width: Get.width * 0.4,
                      child: Stack(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.to(() => DetailPage(), arguments: {
                                "image": data.image,
                                "name": data.name,
                                "subTitle": data.subTitle,
                                "price": data.price,
                                "shades": data.shades,
                                "description": data.description,
                                "ingredient": data.ingredient,
                                "sale": data.sale,
                                "rate": data.rate,
                                "reviews": data.reviews,
                              });
                            },
                            child: Positioned(child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.network(data.image[0],
                              width: double.maxFinite,height: Get.height * 0.23,
                              fit: BoxFit.cover,),
                            ),),
                          ),
                          Positioned(
                            top: 16,
                            child: Column(
                            children: [
                              Container(
                                width: Get.width * 0.2,
                                child: ClipPath(
                                  clipper: MyClipper(distance: 10),
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                                    decoration: BoxDecoration(color: Colors.blue.withOpacity(0.5)),
                                    child: Text("Best Seller", style: TextStyle(color: Colors.white, fontSize: 9, fontWeight: FontWeight.bold,),
                                  ),),
                                ),
                              ),
                              SizedBox(height: 5,),
                              Container(
                                width: Get.width * 0.2,
                                child: ClipPath(
                                  clipper: MyClipper(distance: 10),
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                                    decoration: BoxDecoration(color: Colors.green.withOpacity(0.5)),
                                    child: Text("New", style: TextStyle(color: Colors.white, fontSize: 10, fontWeight: FontWeight.bold,),
                                  ),),
                                ),
                              )
                            ],
                          )),
                          Positioned(
                            right: 5,
                            top: 5,
                            child: Container(
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(5),
                          color: Colors.redAccent),
                          child: Icon(Icons.add, color: Colors.white,),),),
                          Positioned(
                            bottom: 0,
                            height: Get.height * 0.15,
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                              width: Get.width * 0.4,
                              child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(data.name, textAlign: TextAlign.center,),
                                data.shades.length != 1 ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [Container(
                                    width: 10,
                                    height: 10,
                                    decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.pink),
                                  ),
                                  Text(" ${data.shades.length} shades", style: TextStyle(color: Colors.black54, fontSize: 12),),],
                                ): SizedBox(height: 20,),
                                Text("\$${data.price.toStringAsFixed(2)}", style: TextStyle(fontWeight: FontWeight.w600),),
                              ],
                                                        ),
                            ))
                        ],
                      ),
                    );
  }

  Container Slider({required List<String> listSlider}) {
    return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(color: Colors.black12),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: AspectRatio(
              aspectRatio: 1.2,
            child: Stack(
            children: [
              CarouselSlider(
                options: CarouselOptions(
                    autoPlay: true,
                    aspectRatio: 0.8,
                    viewportFraction: 1,
                    height: Get.height * 0.4,
                    onPageChanged: (index, reason) {
                      _changeSlider(index);
                    }),
                items: listSlider.map((data) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(color: Colors.amber),
                          child: Image.network(
                            data,
                            fit: BoxFit.cover,
                          ));
                    },
                  );
                }).toList(),
              ),
              Positioned(
                bottom: 10,
                left: Get.width / 2 - 15 * listSlider.length,
                child: CarouselIndicator(
                  index: _curIndex,
                  count: listSlider.length,
                  color: Colors.black12,
                  activeColor: Colors.red,
                ),
              ),
            ],
          ),),
          ),
        );
  }

  Container ShortcutItem(Shortcut data) {
    return Container(
      width: 60,
      padding: EdgeInsets.only(bottom: 10),
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                    children: [
                      SvgPicture.asset(
                        "assets/icons/${data.image}.svg",
                        width: 40,
                      ),
                      SizedBox(height: 5,),
                      Text(data.name, textAlign: TextAlign.center,),
                    ],
                  ),
                );
  }

  Widget SearchBar() {
    return GestureDetector(
      onTap: () {
        Get.to(() => SearchPage());
      },
      child: Container(
            margin: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
            padding: EdgeInsets.symmetric(horizontal: 10),
            height: 40,
            decoration: BoxDecoration(
                border: Border.all(width: 1),
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              children: [
                SvgPicture.asset(
                  "assets/icons/search.svg",
                  width: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  height: 40,
                  width: Get.width * 0.8,
                  child: TextField(
                    enabled: false,
                    maxLines: 1,
                    decoration: InputDecoration(
                        isDense: true,
                        hintText: "Search your items...",
                        hintStyle: TextStyle(fontSize: 14),
                        border: InputBorder.none),
                  ),
                ),
              ],
            ),
          ),
    );
  }
}
