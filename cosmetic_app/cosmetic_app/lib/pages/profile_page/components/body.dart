import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      color: Colors.purpleAccent,
      child: Column(children: [
        ProfileHeader(),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: Get.width,
            decoration: BoxDecoration(color: Colors.white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10),
            topRight: Radius.circular(10))),
            child: Column(
              
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 16, top: 25),
                  child: Row(
                    children: [
                      Text("PROFILE", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      children: [
                        ProfileLine(icon: Icons.call_outlined, title: "Phone number", subTitle: "Add Number"),
                        ProfileLine(icon: Icons.language_rounded, title: "Language", subTitle: "English (eng)"),
                        ProfileLine(icon: Icons.currency_exchange, title: "Currency", subTitle: "US Dollar (\$)"),
                        ProfileLine(icon: Icons.home_outlined, title: "Country", subTitle: "United State"),
                        ProfileLine(icon: Icons.call_outlined, title: "Phone number", subTitle: "Add Number"),
                        ProfileLine(icon: Icons.notifications, title: "Notifications"),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ]),
    );
  }

  Widget ProfileLine({required IconData icon, required String title, String subTitle = ""}) {
    return Container(
              padding: EdgeInsets.only(bottom : 32),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [Icon(icon, size: 20, color: Colors.black54,),
                SizedBox(width: 5,),
                Text(title, style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),),
                Spacer(),
                subTitle != ""?
                Text(subTitle, style: TextStyle(color: Colors.pink, fontSize: 13),)
                : Icon(Icons.arrow_forward_ios, size: 20, color: Colors.black26,)],
              ),
            );
  }

  Container ProfileHeader() {
    return Container(
        decoration: BoxDecoration(color: Colors.purple),
        child: Column(children: [
          SizedBox(height: Get.height * 0.05,),
          Stack(
            children: [
              CircleAvatar(
                radius: 50,
                backgroundImage: NetworkImage("https://thumbs.dreamstime.com/b/young-teen-girl-laying-her-bed-notebook-26847502.jpg"),
              ),
              Positioned(
                bottom: 0, right: 0,
                child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(shape: BoxShape.circle,
                color: Colors.blue),
                child: Icon(Icons.edit, size: 16, color: Colors.white,),),)
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(color: Colors.white,
                      shape: BoxShape.circle),
                      child: Icon(Icons.swap_horiz_rounded, color: Colors.black, size: 25,),
                    ),
                    Container(
                      width: Get.width * 0.25,
                      child: Text("Switch Account", textAlign: TextAlign.center, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.white),))
                  ],
                ),
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(color: Colors.white,
                      shape: BoxShape.circle),
                      child: Icon(Icons.notifications_outlined, color: Colors.black, size: 25,),
                    ),
                    Container(
                      width: Get.width * 0.25,
                      child: Text("Allow notifications", textAlign: TextAlign.center, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.white),))
                  ],
                ),
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(color: Colors.white,
                      shape: BoxShape.circle),
                      child: Icon(Icons.info_outline, color: Colors.black, size: 25,),
                    ),
                    Container(
                      width: Get.width * 0.25,
                      child: Text("Help", textAlign: TextAlign.center, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.white),))
                  ],
                )
              ],
            ),
          )
        ]),
      );
  }
}