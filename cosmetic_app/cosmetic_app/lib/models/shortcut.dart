class Shortcut {
  String image;
  String name;
  Shortcut({required this.image, required this.name});
}

List<Shortcut> listShortcut = [
  Shortcut(image: "offer", name: "Offers"),
  Shortcut(image: "hot", name: "What's Hot"),
  Shortcut(image: "celebrity", name: "Meet the Shark"),
  Shortcut(image: "product", name: "Your Product"),
  Shortcut(image: "bestseller", name: "Best Selling"),
  Shortcut(image: "rubik", name: "Games"),
];