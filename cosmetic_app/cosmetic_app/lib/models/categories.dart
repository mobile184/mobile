class Categories {
  final String image;
  final String name;
  Categories({required this.name, required this.image});
}

List<Categories> listCategories = [
  Categories(name: "LIPS", image: "https://thumbs.dreamstime.com/b/sexy-beauty-girl-red-lips-nails-provocative-makeup-32449775.jpg"),
  Categories(name: "EYES", image: "https://thumbs.dreamstime.com/b/closeup-woman-eye-visual-effects-white-background-horizontal-wide-66757470.jpg"),
  Categories(name: "FACE", image: "https://thumbs.dreamstime.com/b/beautiful-woman-female-skin-care-healthy-hair-skin-close-up-face-beauty-portrait-beautiful-woman-female-skin-care-healthy-hair-137457631.jpg"),
  Categories(name: "HAIR", image: "https://thumbs.dreamstime.com/b/young-red-haired-woman-voluminous-shiny-wavy-hair-beautiful-model-long-dense-curly-hairstyle-flying-hair-red-100118937.jpg"),
  Categories(name: "SKINCARE", image: "https://thumbs.dreamstime.com/b/beauty-skincare-woman-show-something-to-you-pink-background-beauty-skincare-woman-103969129.jpg"),
  Categories(name: "NAIL", image: "https://thumbs.dreamstime.com/b/colorful-nail-art-manicure-holiday-style-bright-wi-gems-sparkles-polish-fashion-diamond-shine-trendy-accessories-beauty-127894677.jpg"),
];