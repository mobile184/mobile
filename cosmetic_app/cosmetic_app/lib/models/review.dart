class Review {
  final String name;
  final String short_name;
  final double rate;
  final String date;
  final String review;
  Review(
      {required this.name,
      required this.short_name,
      required this.rate,
      required this.date,
      required this.review});
}

Review review1 = Review(
    name: "Tanya Kumari",
    short_name: "TK",
    rate: 4.6,
    date: "Thu 18 Aug 2022",
    review: "Good product, i'll buy more next time");
Review review2 = Review(
    name: "Fukumari Yuuki",
    short_name: "FU",
    rate: 4.8,
    date: "Wed 30 June 2022",
    review: "Ok, suitable for the price");
Review review3 = Review(
    name: "Okari Minamoto",
    short_name: "OM",
    rate: 4,
    date: "Tue 18 May 2022",
    review: "Easy to use, still need to improve but ok");
Review review4 = Review(
    name: "Nana Stingray",
    short_name: "NS",
    rate: 5,
    date: "Fri 03 June 2021",
    review: "Best thing i ever use");
