
import 'package:cosmetic_app/models/best_seller.dart';
import 'package:cosmetic_app/models/review.dart';
import 'package:flutter/material.dart';

import 'rate.dart';

List<Product> listLips = [
  Product(
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/creative-advertising-photo-lipstick-abstract-background-paint-water-photographic-product-177106534.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick rose tangy get everyone's attention",
      price: 1099,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.pink,
        Colors.orange
      ],
      rate: rate1),
      Product(
        sale: 35,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/cosmetics-10873343.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Royal manner for special perso",
      price: 1000,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.black,
        Colors.pink,
        Colors.purple,
        Colors.orange
      ],
      rate: rate2),
      Product(
        sale: 20,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/lipstick-18860581.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick - Friendly to everyone",
      price: 500,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.black,
        Colors.grey,
        Colors.purple,
        Colors.orange
      ],
      rate: rate3),
      Product(
        sale: 16,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/lipstick-pig-6390434.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Easy beauty - Pretty color Comonaric Lipstick",
      price: 350,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      rate: rate4),
];

List<Product> listEyes = [
  Product(
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/young-business-woman-applying-eyes-9755920.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick rose tangy get everyone's attention",
      price: 1099,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.pink,
        Colors.orange
      ],
      rate: rate1),
      Product(
        sale: 35,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/woman-eyedrops-13714841.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Royal manner for special perso",
      price: 1000,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.black,
        Colors.pink,
        Colors.purple,
        Colors.orange
      ],
      rate: rate2),
      Product(
        sale: 20,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/eyes-ears-drop-bottle-isolated-black-71701522.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick - Friendly to everyone",
      price: 500,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.black,
        Colors.grey,
        Colors.purple,
        Colors.orange
      ],
      rate: rate3),
      Product(
        sale: 16,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/pipette-drop-serum-hyaluronic-acid-gold-background-beauty-cosmetic-consept-hydrating-face-masks-skin-care-164374152.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Easy beauty - Pretty color Comonaric Lipstick",
      price: 350,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      rate: rate4),
];

List<Product> listFace = [
  Product(
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/professional-make-up-brushes-set-over-white-background-215066681.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick rose tangy get everyone's attention",
      price: 1099,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.pink,
        Colors.orange
      ],
      rate: rate1),
      Product(
        sale: 35,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/cosmetic-creams-isolated-white-background-facecare-russia-saratov-march-clarins-multi-active-creme-nuit-normal-skin-cosmetic-175378036.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Royal manner for special perso",
      price: 1000,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.black,
        Colors.pink,
        Colors.purple,
        Colors.orange
      ],
      rate: rate2),
      Product(
        sale: 20,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/cosmetic-products-9866232.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick - Friendly to everyone",
      price: 500,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.black,
        Colors.grey,
        Colors.purple,
        Colors.orange
      ],
      rate: rate3),
      Product(
        sale: 16,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/cosmetic-products-green-leaf-9779952.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Easy beauty - Pretty color Comonaric Lipstick",
      price: 350,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      rate: rate4),
];

List<Product> listHair = [
  Product(
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/shampoo-image-bottls-lipstik-isolated-45065174.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick rose tangy get everyone's attention",
      price: 1099,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.pink,
        Colors.orange
      ],
      rate: rate1),
      Product(
        sale: 35,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/shampoo-conditioner-bottles-blue-cap-white-background-96402933.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Royal manner for special perso",
      price: 1000,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.black,
        Colors.pink,
        Colors.purple,
        Colors.orange
      ],
      rate: rate2),
      Product(
        sale: 20,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/beauty-cosmetics-bottles-6386432.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick - Friendly to everyone",
      price: 500,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.black,
        Colors.grey,
        Colors.purple,
        Colors.orange
      ],
      rate: rate3),
      Product(
        sale: 16,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/natural-hair-conditioner-fresh-flowers-homemade-92023285.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Easy beauty - Pretty color Comonaric Lipstick",
      price: 350,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      rate: rate4),
];

List<Product> listSkincare = [
  Product(
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/summer-cosmetic-set-protection-cream-orange-background-top-view-mockup-woman-lotion-sunglasses-90522423.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick rose tangy get everyone's attention",
      price: 1099,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.pink,
        Colors.orange
      ],
      rate: rate1),
      Product(
        sale: 35,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/beauty-cosmetic-cream-19741891.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Royal manner for special perso",
      price: 1000,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.black,
        Colors.pink,
        Colors.purple,
        Colors.orange
      ],
      rate: rate2),
      Product(
        sale: 20,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/spa-collage-series-made-five-images-floral-water-bath-salt-candles-towel-30542000.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick - Friendly to everyone",
      price: 500,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.black,
        Colors.grey,
        Colors.purple,
        Colors.orange
      ],
      rate: rate3),
      Product(
        sale: 16,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/blue-cosmetic-4751294.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Easy beauty - Pretty color Comonaric Lipstick",
      price: 350,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      rate: rate4),
];

List<Product> listNail = [
  Product(
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/manicure-nail-paint-pink-color-flowers-orchid-30757928.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick rose tangy get everyone's attention",
      price: 1099,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.pink,
        Colors.orange
      ],
      rate: rate1),
      Product(
        sale: 35,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/nail-painting-26223389.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Royal manner for special perso",
      price: 1000,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.black,
        Colors.pink,
        Colors.purple,
        Colors.orange
      ],
      rate: rate2),
      Product(
        sale: 20,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/nail-art-handmade-colorful-nails-isolated-white-background-52681283.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Lipstick - Friendly to everyone",
      price: 500,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      shades: [
        Colors.red,
        Colors.black,
        Colors.grey,
        Colors.purple,
        Colors.orange
      ],
      rate: rate3),
      Product(
        sale: 16,
    reviews: [review1, review2],
      image:
          ["https://thumbs.dreamstime.com/b/transparent-small-bottles-glitter-makeup-nail-art-selective-focus-82180353.jpg",
          "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
          "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"],
      name: "Easy beauty - Pretty color Comonaric Lipstick",
      price: 350,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      rate: rate4),
];