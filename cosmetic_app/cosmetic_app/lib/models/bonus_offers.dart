import 'package:flutter/material.dart';

import 'best_seller.dart';
import 'rate.dart';
import 'review.dart';

List<Product> listBonusOffers = [
  Product(
      sale: 10,
      reviews: [review1, review2],
      image: [
        "https://thumbs.dreamstime.com/b/black-brown-cosmetic-product-jars-bottles-branding-mock-up-standing-light-table-wall-background-snail-mucin-139066063.jpg",
        "https://thumbs.dreamstime.com/b/set-various-face-powder-brush-cosmetic-compact-loose-makeup-isolated-white-background-top-view-point-flat-lay-87292252.jpg",
        "https://thumbs.dreamstime.com/b/towel-flour-15580013.jpg"
      ],
      name: "Mettle Priming Balm",
      price: 1099,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      rate: rate1),
  Product(
      sale: 20,
      reviews: [review3, review1],
      image: [
        "https://thumbs.dreamstime.com/b/cosmetic-brushes-10399294.jpg",
        "https://thumbs.dreamstime.com/b/flat-lay-set-bag-cosmetics-paper-colorful-women-beauty-makeup-97421405.jpg"
      ],
      name: "Eyebrow makeup set",
      price: 500,
      shades: [
        Colors.red,
        Colors.black,
        Colors.pink,
        Colors.purple,
        Colors.orange
      ],
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      rate: rate2),
  Product(
      sale: 15,
      reviews: [review4, review2, review1],
      image: [
        "https://thumbs.dreamstime.com/b/different-shades-loose-cosmetic-powder-background-semi-circle-row-jars-filled-makeup-brush-top-view-blue-copy-space-57014532.jpg",
        "https://thumbs.dreamstime.com/b/beautiful-young-vimpire-woman-bloody-kitchen-girl-beautiful-young-vimpire-woman-bloody-kitchen-girl-beauty-girl-jam-flour-185868677.jpg",
        "https://thumbs.dreamstime.com/b/flat-lay-set-bag-cosmetics-paper-colorful-women-beauty-makeup-97421380.jpg"
      ],
      name: "Makeup flour",
      price: 159,
      subTitle: "A product from Calibaba",
      description: "Good for everyone, friendly to environtment",
      ingredient: "Powder, rose, natri, mana, ki, aura",
      rate: rate5),
];
