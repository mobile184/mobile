class HotPick {
  final String image;
  final String name;
  HotPick({required this.image, required this.name});
}

List<HotPick> listHotPick = [
  HotPick(image: "https://thumbs.dreamstime.com/b/makeup-products-cosmetic-bag-color-background-makeup-products-cosmetic-bag-114512623.jpg", name: "Makeup Tools"),
  HotPick(image: "https://thumbs.dreamstime.com/b/cosmetic-nature-skincare-essential-oil-aromatherapy-organic-natural-science-beauty-product-herbal-alternative-medicine-mock-up-140653050.jpg", name: "Coconut Oil"),
  HotPick(image: "https://thumbs.dreamstime.com/b/different-cosmetic-makeup-brushes-pink-ribbon-holographic-glitter-confetti-form-stars-pink-background-flat-131969574.jpg", name: "Eyebrown Brush"),
  HotPick(image: "https://thumbs.dreamstime.com/b/natural-products-hair-concept-two-size-green-plastic-bottle-blank-label-contain-herbal-bergamot-shampoo-decorate-137183280.jpg", name: "Lemon Shampoo"),
  HotPick(image: "https://thumbs.dreamstime.com/b/natural-aromatherapy-marseilles-type-bath-soap-bar-22042942.jpg", name: "Classic Soap"),
];