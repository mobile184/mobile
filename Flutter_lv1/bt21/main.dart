import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

List<Note> list = [new Note(DateTime.parse('2200-05-11'), 'Get Doraemon')];

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyListView(),
    );
  }
}

class MyListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyListView();
  }
}

class Note {
  DateTime date;
  String note;
  Note(this.date, this.note);
}

class _MyListView extends State<MyListView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My app')),
      body: ListView.builder(
        itemExtent: 50,
        itemBuilder: (context, index) {
          return Row(
            children: [
              Flexible(
                child: ListTile(
                  subtitle: Text('Date: ${list[index].date}'),
                  title: Text('Note: ${list[index].note}'),
                ),
              ),
              Icon(Icons.edit),
            ],
          );
        },
        itemCount: list.length,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddNotePage()));
        },
      ),
    );
  }
}

class AddNotePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AddNotePage();
  }
}

class _AddNotePage extends State<AddNotePage> {
  DateTime? time;
  var _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Create note page')),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            textCapitalization: TextCapitalization.sentences,
            controller: _controller,
            decoration: InputDecoration(
                hintText: 'Type note here!', prefixIcon: Icon(Icons.edit)),
          ),
          Row(
            children: [
              Text('Date'),
              TextButton(
                child: Text(time == null ? 'YYYY-MM-DD' : time.toString()),
                onPressed: () {
                  showDatePicker(
                    context: context,
                    initialDate: DateTime(2022),
                    firstDate: DateTime(2022),
                    lastDate: DateTime(2050),
                  ).then((date) {
                    setState(() {
                      time = date!;
                    });
                  });
                },
              ),
            ],
          ),
          TextButton(
            child: Text('Create new note'),
            onPressed: () {
              list.add(new Note(time!, _controller.text));
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text('New note created!'),
                ),
              );
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: ((context) => MyListView())));
            },
          )
        ],
      )),
    );
  }
}
