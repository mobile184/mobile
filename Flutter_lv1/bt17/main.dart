import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyInfiniteListView(),
    );
  }
}

class MyInfiniteListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyInfiniteListView();
  }
}

class _MyInfiniteListView extends State<MyInfiniteListView> {
  List<String> list = [];
  int maxCurLength = 20;
  ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    list = List.generate(20, (index) => 'This is line ${index + 1}');
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _loadMoreData();
      }
    });
  }

  _loadMoreData() {
    for (int i = maxCurLength; i < maxCurLength + 10; i++) {
      list.add('This is line ${i + 1}');
    }
    maxCurLength += 10;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Load more list')),
      body: ListView.builder(
        controller: _scrollController,
        itemExtent: 35,
        itemBuilder: (context, index) {
          if (index == list.length) {
            return CupertinoActivityIndicator();
          }
          return ListTile(
            title: Text('This is line ${index + 1}'),
          );
        },
        itemCount: list.length + 1,
      ),
    );
  }
}
