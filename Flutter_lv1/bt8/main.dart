import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LayoutApp(),
    );
  }
}

class LayoutApp extends StatelessWidget {
  List<String> list = [
    'https://static.wixstatic.com/media/bb1bd6_2a655144684b48579a31437382179cf1~mv2.png/v1/fill/w_924,h_600,al_c/bb1bd6_2a655144684b48579a31437382179cf1~mv2.png',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRT-kRRdStx1k-V_Iq26pfTPz7cKmxj2enycjYoRSsPt_FaKkkKtiuMxrXnDYxOVOfIZPI&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTtRStG4vThU5S3StOZtbi6KU03HEuKhOjiOUEaNbfYgiGMtErmHKlnf2g1watUVIsdryM&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmmBCczCSVZoxhDtis_iI7bIulFbSWbM-r-hjj1PX5BWSq8gJu-AbnuY1bZpLwfSpUmH0&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_OnO-lB9lb7R-hJNK2ziIBd1lNFYybpDronpbZ5fBo4KIbVA9V4IKPJlRJQFolM4gwNk&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQypjp34-XyquTWMvDePv7yCCKHQ9IAT4_xcLkXjeHLLwzsxTlFORKVnaiQjJI4HPY8XU0&usqp=CAU'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Grid view'),
      ),
      body: Center(
        child: GridView.builder(
          itemCount: list.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 5,
          ),
          itemBuilder: (context, index) {
            return Image.network(list[index]);
          },
        ),
      ),
    );
  }
}
