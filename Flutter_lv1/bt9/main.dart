import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: StackLayoutApp(),
    );
  }
}

class StackLayoutApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stack layout'),
      ),
      body: Center(
        child: Stack(alignment: Alignment.center, children: [
          Container(
            color: Colors.black,
            width: 250,
            height: 250,
          ),
          Container(
            color: Colors.white,
            width: 150,
            height: 150,
          ),
          Text(
            'Stack layout',
            style: TextStyle(color: Colors.yellow, fontSize: 20),
          ),
        ]),
      ),
    );
  }
}
