import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
            colorSchemeSeed: const Color(0xff6750a4), useMaterial3: true),
        home: DefaultTabController(
            length: 3,
            child: Scaffold(
              appBar: AppBar(
                title: Text('Tabs layout'),
              ),
              bottomNavigationBar: TabBar(
                tabs: [
                  Tab(
                    icon: Icon(Icons.one_k),
                  ),
                  Tab(
                    icon: Icon(Icons.two_k),
                  ),
                  Tab(
                    icon: Icon(Icons.three_k),
                  ),
                ],
                unselectedLabelColor: Colors.blue,
                indicatorColor: Colors.red,
                labelColor: Colors.red,
              ),
              body: TabBarView(children: [
                Center(child: Text('Tab 1')),
                Center(child: Text('Tab 2')),
                Center(child: Text('Tab 3'))
              ]),
            )));
  }
}
