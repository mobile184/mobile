import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: FirstDrawer(),
    );
  }
}

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: new ListView(
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: Text(
                'Drawer header',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          ListTile(
              title: Text('First drawer'),
              hoverColor: Colors.green,
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new FirstDrawer()));
              }),
          ListTile(
              title: Text('Second drawer'),
              hoverColor: Colors.green,
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new SecondDrawer()));
              }),
        ],
      ),
    );
  }
}

class FirstDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyDrawer(),
      appBar: AppBar(title: Text('First drawer')),
      body: Center(
        child: Text(
          'My first drawer',
        ),
      ),
    );
  }
}

class SecondDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyDrawer(),
      appBar: AppBar(title: Text('Second drawer')),
      body: Center(
        child: Text(
          'My second drawer',
        ),
      ),
    );
  }
}
