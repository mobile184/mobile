import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyHorizontalListView(),
    );
  }
}

class MyHorizontalListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyHorizontalListView();
  }
}

class _MyHorizontalListView extends State<MyHorizontalListView> {
  List<String> list = [
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSCZlf5lc5tX-0gY-y94pGS0mQdL-D0lCH2OQ&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxXnC3fwMwkbIt3ejGRIw3NmbDyUtgS5g2jA&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3euZRwpKfI9QxOCuyXPYbCW7pCXeXT2L_EA&usqp=CAU'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Load more list')),
      body: ListView.builder(
        itemExtent: 100,
        itemBuilder: (context, index) {
          return ListTile(
            title: Padding(
              child: Row(
                children: [
                  Text('Image $index'),
                  Image.network(list[index]),
                ],
              ),
              padding: EdgeInsets.all(10),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
