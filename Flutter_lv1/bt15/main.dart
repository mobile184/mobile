import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyTextViewApp(),
    );
  }
}

class MyTextViewApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyTextViewApp();
  }
}

class _MyTextViewApp extends State<MyTextViewApp> {
  String myText = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My textview app')),
      body: Center(
        child: Column(
          children: [
            Row(
              children: [
                Flexible(
                  child: TextField(
                    textCapitalization: TextCapitalization.sentences,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.keyboard),
                      hintText: 'Type your name',
                    ),
                    onChanged: (text) {
                      myText = text;
                    },
                  ),
                ),
                TextButton(
                  onPressed: () {
                    setState(() {});
                  },
                  child: Text('Show name'),
                ),
              ],
            ),
            Text(myText),
          ],
        ),
      ),
    );
  }
}
