import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyListView(),
    );
  }
}

class MyListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyListView();
  }
}

class _MyListView extends State<MyListView> {
  List<String> list = [];
  @override
  void initState() {
    super.initState();
    list = List.generate(20, (index) => index.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My app')),
      body: ListView.builder(
        itemExtent: 50,
        itemBuilder: (context, index) {
          return GestureDetector(
            child: Text('Item view ${index + 1}'),
            onTap: () {
              Fluttertoast.showToast(
                  msg: 'Item ${index + 1} tapped!',
                  toastLength: Toast.LENGTH_SHORT);
            },
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
