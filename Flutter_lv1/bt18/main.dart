import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyHorizontalListView(),
    );
  }
}

class MyHorizontalListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyHorizontalListView();
  }
}

class _MyHorizontalListView extends State<MyHorizontalListView> {
  List<String> list = [];
  @override
  void initState() {
    super.initState();
    list = List.generate(20, (index) => 'View ${index + 1}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Load more list')),
      body: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemExtent: 35,
        itemBuilder: (context, index) {
          return ListTile(
            title: Card(
              child: Text(list[index]),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
