import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyFontApp(),
    );
  }
}

class MyFontApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My font app')),
      body: Center(
        child: Text(
          'My font text',
          style: GoogleFonts.princessSofia(
            fontSize: 35,
          ),
        ),
      ),
    );
  }
}
