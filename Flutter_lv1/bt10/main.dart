import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          colorSchemeSeed: const Color(0xff6750a4), useMaterial3: true),
      home: Scaffold(
        appBar: AppBar(title: const Text('Card Examples')),
        body: CardLayout(),
      ),
    );
  }
}

class CardLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          child: Center(
            child: Text(
              'Text in card',
            ),
          ),
        ),
        SizedBox(
            width: 200,
            child: ElevatedButton(
              child: Text(
                'Sized button',
              ),
              onPressed: () {},
            )),
      ],
      mainAxisAlignment: MainAxisAlignment.center,
    );
  }
}
