import 'package:flutter/material.dart';

class Hero {
  int id;
  String name;
  String ability;
  Hero(this.id, this.name, this.ability);
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My listview demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LayoutApp(),
    );
  }
}

class LayoutApp extends StatelessWidget {
  List<Hero> heroes = [
    new Hero(1, 'Ironman', 'Have a super modern and powerful armor'),
    new Hero(2, 'Spiderman', 'Can climb on wall and shoot silk'),
    new Hero(3, 'Hulk', "Can change to 'Hulk' type when angry.")
  ];

  LayoutApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My layout app'),
        ),
        body: Center(
            child: ListView.builder(
                itemCount: heroes.length,
                itemBuilder: (_, index) {
                  return Card(
                      child: Row(
                    children: [
                      Flexible(
                        child: ListTile(
                          title: Text(heroes[index].name),
                          subtitle: Text(
                            heroes[index].ability,
                            overflow: TextOverflow.visible,
                          ),
                        ),
                      ),
                      Container(
                          width: 200,
                          height: 150,
                          child: Image.asset(
                            'assets/images/hero$index.png',
                            fit: BoxFit.fill,
                          ))
                    ],
                  ));
                })));
  }
}
