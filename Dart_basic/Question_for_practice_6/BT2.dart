import 'dart:math';

int? generateRandom() {
  int i = Random().nextInt(2);
  //return 100 or null depend on value of i
  return i == 0 ? 100 :null;
}

void main() {
  int? random = generateRandom();
  //if random = null -> status = 0, else 1
  int status = random == null ? 0 : 1;
  print(status); 
}