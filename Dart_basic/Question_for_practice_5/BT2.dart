class House {
  int id;
  String name;
  int price;
  House(this.id, this.name, this.price);
  String toString() {
    return '{id: $id, name: $name, price: $price}';
  }
}
void main() {
  House house1 = new House(1, 'House A', 20);
  House house2 = new House(2, 'House B', 30);
  House house3 = new House(3, 'House C', 40);
  List<House> listHouse = [house1, house2, house3];
  print(listHouse);
}