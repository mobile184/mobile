class Camera {
  int _id;
  bool _brand;
  String _color;
  int _price;
  Camera(this._id, this._brand, this._color, this._price);
  int get id{
    return this._id;
  }
  set id(int id) {
    this._id = id;
  }
  bool get brand{
    return this._brand;
  }
  set brand(bool brand) {
    this._brand = brand;
  }
  String get color{
    return this._color;
  }
  set color(String color) {
    this._color = color;
  }
  int get price{
    return this._price;
  }
  set price(int price) {
    this._price = price;
  }
  String toString() {
    return '{id: $_id, brand: $_brand, color: $_color, price: $_price}';
  }
}
void main() {
  Camera cam1 = new Camera(1, true , 'black', 40);
  Camera cam2 = new Camera(2, false, 'white', 20);
  Camera cam3 = new Camera(3, false, 'gray', 30);
  print(cam1.toString());
  print(cam2.toString());
  print(cam3.toString());
}