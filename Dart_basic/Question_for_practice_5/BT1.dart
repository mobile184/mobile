class Laptop {
  int id;
  String name;
  String ram;
  Laptop(this.id, this.name, this.ram);
  String toString() {
    return '{id: $id, name: $name, ram: $ram}';
  }
}
void main() {
  Laptop lap1 = new Laptop(1, 'lap1', '4GB');
  Laptop lap2 = new Laptop(2, 'lap2', '8GB');
  Laptop lap3 = new Laptop(3, 'lap3', '16GB');
  print(lap1.toString() + '\n'+ lap2.toString() + '\n'+lap3.toString());
}