class Animal {
  int id;
  String name;
  String color;
  Animal(this.id, this.name, this.color);
  String toString() {
    return '{id: $id, name: $name, color: $color}';
  }
}
class Cat extends Animal {
  String? sound;
  Cat(int id, String name, String color, String sound) :super(id, name, color) {
    this.sound = sound;
  }
  String toString() {
    return '{id: $id, name: $name, color: $color, sound: $sound}';
  }
}

void main() {
  Cat cat = new Cat(1, 'mill', 'black', 'meow');
  print(cat);
}