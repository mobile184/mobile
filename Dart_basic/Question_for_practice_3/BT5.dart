import 'dart:io';
import 'dart:math';

double usePythago(double a, double b) {
  return a*a + b*b;
}
void main() {
  double a = double.parse(stdin.readLineSync()!);
  double b = double.parse(stdin.readLineSync()!);
  print(usePythago(a, b));
}