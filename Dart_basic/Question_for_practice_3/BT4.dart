import 'dart:io';

const double pi = 3.14;
double calCircleArea(double r) {
  return pi*r*r;
}
void main() {
  double r = double.parse(stdin.readLineSync()!);
  print(calCircleArea(r).toStringAsFixed(2));
}