import 'dart:io';
import 'dart:math';
num calPow(num n, num exponent) {
  return pow(n, exponent);
}
void main() {
  num n = num.parse(stdin.readLineSync()!);
  num exponent = num.parse(stdin.readLineSync()!);
  print(calPow(n, exponent));
}