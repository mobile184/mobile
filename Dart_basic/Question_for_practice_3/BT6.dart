import 'dart:io';

String reverseString(String str) {
  return str.split('').reversed.join();
}
void main() {
  String str = stdin.readLineSync()!;
  print(reverseString(str));
}