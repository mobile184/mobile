import 'dart:io';
import 'dart:math';

bool isPrime(int n) {
  if (n < 2) return false;
  if (n == 2) return true;
  if (n % 2 == 0) return false;
  for (int i = 3; i <= sqrt(n).ceil(); i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  int n = int.parse(stdin.readLineSync()!);
  if (isPrime(n) == true) {
    print('$n is a prime');
  } else {
    print('$n is not a prime');
  }
}
