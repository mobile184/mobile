import 'dart:io';

void main() {
  num a = double.parse(stdin.readLineSync()!);
  if (a > 0) {
    print('Positive');
  } else if (a < 0) {
    print('Negative');
  } else if (a == 0) {
    print('Zero');
  }
}
