﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BlogApi.Models;
using MySqlConnector;
using System.Data;

namespace BlogApi.Models
{
    public class BlogApiContext
    {
        public string ConnectionString { get; set; }

        public BlogApiContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        /**
         * Get connection from mysql
         * @param null
         * return MysqlConnection
         */
        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        /**
         * Get all blog from database
         * @param null
         * return List<Blog>
         */
        public async Task<List<Blog>> GetAllBlogs()
        {
            List<Blog> list = new List<Blog>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from blog", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Blog()
                        {
                            id = Convert.ToInt32(reader["i"]),
                            title = reader["title"].ToString(),
                            des = reader["des"].ToString(),
                            detail = reader["detail"].ToString(),
                            category = reader["category"].ToString(),
                            isPublic = Convert.ToBoolean(reader["isPublic"]),
                            date = Convert.ToDateTime(reader["date"]),
                            positions = reader["positions"].ToString(),
                        });
                    }
                }

            }
            return list;
        }

        /**
         * Get 1 blog by id
         * @param id
         * return Blog
         */
        public async Task<Blog> GetBlogById(int? id)
        {
            Blog blog = new Blog();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"select * from blog where i={id}", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        blog = new Blog()
                        {
                            id = Convert.ToInt32(reader["i"]),
                            title = reader["title"].ToString(),
                            des = reader["des"].ToString(),
                            detail = reader["detail"].ToString(),
                            category = reader["category"].ToString(),
                            isPublic = Convert.ToBoolean(reader["isPublic"]),
                            date = Convert.ToDateTime(reader["date"]),
                            positions = reader["positions"].ToString(),
                        };
                    }
                }

            }
            return blog;
        }

        /**
         * Add new blog
         * @param blog
         * return bool
         */
        public async Task<bool> AddBlog(Blog blog)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"insert into blog (title, des, detail, category, isPublic, date, positions)" +
                    $" values(\"{blog.title}\", \"{blog.des}\", \"{blog.detail}\", \"{blog.category}\", {blog.isPublic}, \"{blog.date.ToString("yyyy-MM-dd HH:mm:ss.fff")}\", \"{blog.positions}\")", conn);
                MySqlDataReader reader = cmd.ExecuteReader();
            }
            return true;
        }

        /**
         * Remove a blog from database at id
         * @param id
         * return bool
         */
        public async Task<bool> RemoveBlogAt(int id)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"delete from blog where i={id}", conn);
                                MySqlDataReader reader = cmd.ExecuteReader();

            }
            return true;
        }

        /**
         * Update a blog by given blog's id
         * @param blog
         * return bool
         */
        public async Task<bool> UpdateBlog(Blog blog)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"update blog set title=\"{blog.title}\", des=\"{blog.des}\", detail=\"{blog.detail}\", category=\"{blog.category}\", isPublic={blog.isPublic}, date=\"{blog.date.ToString("yyyy-MM-dd HH:mm:ss.fff")}\", positions=\"{blog.positions}\" where i={blog.id}", conn);
                MySqlDataReader reader = cmd.ExecuteReader();
            }
            return true;
        }
    }
}

