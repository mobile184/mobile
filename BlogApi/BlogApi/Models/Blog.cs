﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApi.Models
{
    public class Blog
    {
        [Key]
        public int id { get; set; }
        public string title { get; set;}
        public string des { get; set; }
        public string detail { get; set; }
        public string category { get; set; }
        public bool isPublic { get; set; }
        public DateTime date { get; set; }
        public string positions { get; set; }

    }
}
