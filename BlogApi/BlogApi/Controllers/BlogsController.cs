﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BlogApi.Models;


namespace BlogApi.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class BlogsController : Controller
    {
        private readonly BlogApiContext _context;
        private BlogApiContext db = new BlogApiContext("DefaultConnection");


        public BlogsController(BlogApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        // GET: Blogs
        //get all blogs from database
        //@param null
        //return List<Blog>
        public async Task<List<Blog>> Index()
        {
            return await _context.GetAllBlogs();
        }

        [HttpGet]
        // GET: Blogs/Details/5
        //get 1 blog with id
        //@param id
        //return Blog
        public async Task<Blog> Details(int? id)
        {
            var blog = await _context.GetBlogById(id);
            return blog;
        }


        [HttpPost]
        //POST: Blogs/Post
        //create a new blog
        //@param blog
        //return bool
        public async Task<bool> Create(Blog blog)
        {
            return await _context.AddBlog(blog);
        }

        [HttpDelete]
        //DELETE: Blogs/Delete
        //delete blog with given id
        //@param id
        //return bool
        public async Task<bool> Delete(int id)
        {
            return await _context.RemoveBlogAt(id);
        }

        [HttpPut]
        //PUT: Blogs/Put
        //update blog with given blog's id
        //@param blog
        //return bool
        public async Task<bool> Update(Blog blog)
        {
            return await _context.UpdateBlog(blog);
        }
    }
}
