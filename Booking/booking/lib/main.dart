import 'package:booking/controllers/schedule_page_controller.dart';
import 'package:booking/route_management/bindings.dart';
import 'package:booking/route_management/get_all_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'route_management/all_routes.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Booking App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: kWelcomePage,
      getPages: 
      GetAllRoute.getAllRoute(),
    );
  }
}

