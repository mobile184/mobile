import 'dart:async';
import 'dart:ui';

import 'package:booking/controllers/schedule_page_controller.dart';
import 'package:booking/route_management/all_routes.dart';
import 'package:booking/schedule_page/schedule_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  void initState() {
    Timer(Duration(seconds: 3), () {
      Get.toNamed(kSchedulePage);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: double.maxFinite,
            height: double.maxFinite,
            decoration: BoxDecoration(image: DecorationImage(image: NetworkImage("https://thumbs.dreamstime.com/b/cyber-attack-concept-crime-hacker-circle-global-network-businessman-checking-stock-market-data-tablet-night-backgr-lock-103775091.jpg"), fit: BoxFit.cover),),
            child: BackdropFilter(filter: ImageFilter.blur(
              sigmaX: 5, sigmaY: 5
            ),
            child: Container(color: Colors.white.withOpacity(0.0)),
            ),
          ),
          Center(
            child: CircleAvatar(backgroundImage: NetworkImage("https://thumbs.dreamstime.com/b/cyber-attack-concept-crime-hacker-circle-global-network-businessman-checking-stock-market-data-tablet-night-backgr-lock-103775091.jpg"),
            radius: 40,),
          )
        ],
      ),
    );
  }
}