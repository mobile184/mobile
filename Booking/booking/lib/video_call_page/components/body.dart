import 'package:booking/components/constants.dart';
import 'package:booking/route_management/all_routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var call = Get.arguments;
  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Positioned(
        top: 0,
        left: 0,
        child: Container(
          width: Get.width,
          height: Get.height,
          child: AspectRatio(
            aspectRatio: 3,
            child: Image.network(
              call["avatar"],
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      Positioned(
        width: 100,
        height: 150,
        top: 60,
        right: 20,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: Image.network(
            "https://thumbs.dreamstime.com/b/cyber-attack-concept-crime-hacker-circle-global-network-businessman-checking-stock-market-data-tablet-night-backgr-lock-103775091.jpg",
            fit: BoxFit.cover,
          ),
        ),
      ),
      Positioned(
        bottom: 0,
        height: 230,
        width: Get.width,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: kDefaultPadding * 2),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 250),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                call["name"],
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 16),
              ),
              Text(
                "00:05:24",
                style: TextStyle(color: Colors.white60),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      padding: EdgeInsets.all(kDefaultPadding * 1.5),
                      margin: EdgeInsets.symmetric(
                          vertical: kDefaultPadding * 2,
                          horizontal: kDefaultPadding * 1),
                      decoration: BoxDecoration(
                        color: Colors.redAccent,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.videocam_outlined,
                        color: Colors.white,
                      )),
                  Container(
                      padding: EdgeInsets.all(kDefaultPadding * 1.5),
                      margin: EdgeInsets.symmetric(
                          vertical: kDefaultPadding * 2,
                          horizontal: kDefaultPadding * 1),
                      decoration: BoxDecoration(
                        color: kTextColor,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.call_outlined,
                        color: Colors.white,
                      )),
                  Container(
                      padding: EdgeInsets.all(kDefaultPadding * 1.5),
                      margin: EdgeInsets.symmetric(
                          vertical: kDefaultPadding * 2,
                          horizontal: kDefaultPadding * 1),
                      decoration: BoxDecoration(
                        color: kTextColor,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.mic_outlined,
                        color: Colors.white,
                      )),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              GestureDetector(
                onVerticalDragEnd: ((details) {
                  if (details.primaryVelocity! < 0) Get.toNamed(kChatPage);
                }),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.keyboard_arrow_up,
                      color: Colors.white,
                    ),
                    Text(
                      "Swipe back to menu",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ]);
  }
}
