import 'package:booking/chat_page/chat_page.dart';
import 'package:booking/components/constants.dart';
import 'package:booking/route_management/all_routes.dart';
import 'package:booking/schedule_page/schedule_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../create_page/create_page.dart';
import '../search_page/search_page.dart';

class BottomBar extends StatelessWidget {
  final current_page;
  const BottomBar({Key? key, required this.current_page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(kDefaultPadding * 1.5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        GestureDetector(
          child: Icon(
            Icons.search,
            color: current_page == 1 ? kIconPrimaryColor : kTextColor,
          ),
          onTap: () {
            Get.toNamed(kSearchPage);
          },
        ),
        GestureDetector(
          child: Icon(
            Icons.calendar_month,
            color: current_page == 2 ? kIconPrimaryColor : kTextColor,
          ),
          onTap: () {
            Get.toNamed(kSchedulePage);
          },
        ),
        GestureDetector(
          child: SvgPicture.asset(
        "assets/icons/messenger.svg",
        color: current_page == 3 ? kIconPrimaryColor : kTextColor,
      ),
          onTap: () {
            Get.toNamed(kChatPage);
          },
        ),
      ]),
    );
  }
}
