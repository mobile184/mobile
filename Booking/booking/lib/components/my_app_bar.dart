import 'package:flutter/material.dart';

import 'constants.dart';

AppBar MyAppBar({title, tab1, tab2, tab3, icon}) {
  return AppBar(
    elevation: 0.0,
    backgroundColor: Colors.white,
    automaticallyImplyLeading: false,
    title: Text(
      title,
      style: TextStyle(color: Colors.black),
    ),
    bottom: PreferredSize(
      preferredSize: Size.fromHeight(60),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: kDefaultPadding * 2,),
        child: DecoratedBox(
          decoration: BoxDecoration(color: kSecondaryColor,
          borderRadius: BorderRadius.circular(10)),
          child: TabBar(
              unselectedLabelColor: Colors.black,
              indicator: BoxDecoration(
                  color: kIconPrimaryColor,
                  borderRadius: BorderRadius.circular(10)),
              unselectedLabelStyle:
                  TextStyle(color: Colors.black,),
              tabs: <Widget>[
                Tab(
                  child: Container(
                    child: Text(
                      tab1,
                    ),
                  ),
                ),
                Tab(
                  child: Text(
                    tab2,
                  ),
                ),
                Tab(
                  child: Text(
                    tab3,
                  ),
                ),
              ]),
        ),
      ),
    ),
    actions: [
      Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: Image.network(
            icon,
            width: 20,
            height: 20,
          )),
    ],
  );
}
