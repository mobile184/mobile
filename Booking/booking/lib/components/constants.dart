import 'package:flutter/material.dart';

const kTextColor = Color.fromRGBO(200, 200, 200, 1);
const kIconPrimaryColor = Color.fromRGBO(25, 153, 142, 1);
const kSecondaryColor = Color.fromRGBO(233, 243, 242, 1);
const kIconTextColor = Color.fromRGBO(74, 178, 167, 1);
const bellIcon = "https://cdn-icons-png.flaticon.com/512/35/35300.png";
const searchIcon = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR5a1Z714_l31551HlUT3OqcgooQOGAmNtuKw&usqp=CAU";
const kDefaultPadding = 10.0;