import 'package:booking/route_management/all_routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../../components/constants.dart';
import '../../controllers/doctor_list_page_controller.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  DoctorListPageController controller = Get.find();

  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await this.controller.readData(context);
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: kDefaultPadding * 2,
        ),
        //List Chat
        SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              for (int i = 0; i < controller.doctors.length; i++)
                ListTile(
                  onTap: () {
                    Get.toNamed(kCreatePage, arguments: {
                    "avatar": controller.doctors[i]["avatar"],
                    "name": controller.doctors[i]["name"],
                    "job": controller.doctors[i]["job"],
                    "rate": controller.doctors[i]["rate"],
                    "distance": controller.doctors[i]["distance"],
                    "fee": controller.doctors[i]["fee"],
                  },
                  );
                  },
                  leading: CircleAvatar(
                    radius: 25,
                    backgroundImage:
                        NetworkImage(controller.doctors[i]["avatar"]),
                  ),
                  title: Text(
                    controller.doctors[i]["name"],
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text(
                    controller.doctors[i]["job"],
                    overflow: TextOverflow.ellipsis,
                  ),
                  trailing: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(height: kDefaultPadding / 2,),
                      Text("\$" +
                          controller.doctors[i]["fee"].toStringAsFixed(2)),
                          SizedBox(height: kDefaultPadding / 2,),
                      Text(controller.doctors[i]["distance"], style: TextStyle(color: kTextColor, fontSize: 12),),
                    ],
                  ),
                ),
            ],
          ),
        )
      ],
    );
  }
}
