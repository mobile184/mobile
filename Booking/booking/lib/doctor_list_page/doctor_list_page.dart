import 'package:booking/components/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'components/body.dart';

class DoctorListPage extends StatelessWidget {
  const DoctorListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Body(),
      appBar: AppBar(
    elevation: 0.0,
    backgroundColor: Colors.white,
    automaticallyImplyLeading: false,
    title: Text(
      "Doctor",
      style: TextStyle(color: Colors.black),
    ),
    actions: [
      Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: Image.network(
            searchIcon,
            width: 20,
            height: 20,
          )),
    ],
  )
    );
  }
}