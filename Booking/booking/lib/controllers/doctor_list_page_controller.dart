
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DoctorListPageController extends GetxController {
  RxList doctors = [].obs;
  final time_state = 0;
  readData(BuildContext context) async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/jsons/doctor.json")
        .then((value) {
      doctors.value = json.decode(value);
      update();
    });
  }
}