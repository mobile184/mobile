import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChatPageController extends GetxController {
  RxList chat = [].obs;
  final time_state = 0;
  readData(BuildContext context) async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/jsons/chat.json")
        .then((value) {
      chat.value = json.decode(value);
      update();
    });
  }
}