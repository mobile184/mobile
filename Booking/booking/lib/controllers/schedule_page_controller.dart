import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SchedulePageController extends GetxController {
  RxList schedule = [].obs;
  final time_state = 0;
  readData(BuildContext context) async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/jsons/schedule.json")
        .then((value) {
      schedule.value = json.decode(value);
      update();
    });
  }
}
