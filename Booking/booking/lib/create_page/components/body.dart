import 'package:booking/components/constants.dart';
import 'package:booking/route_management/all_routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var schedule = Get.arguments;
  String? payment_method = "visa";
  @override
  Widget build(BuildContext context) {
    DateTime time = DateTime.now();
    String reason = "Please choose reason you feel not good";
    if (schedule["reason"] != null) reason = schedule["reason"];
    if (schedule["time"] != null) time = DateTime.parse(schedule["time"]);
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: kDefaultPadding * 2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: kDefaultPadding * 3),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(width: 0.1),
              ),
              child:
                  Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Container(
                    margin: EdgeInsets.all(kDefaultPadding),
                    width: 100,
                    height: 120,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.network(
                        schedule["avatar"],
                        fit: BoxFit.cover,
                      ),
                    )),
                Container(
                  margin: EdgeInsets.symmetric(vertical: kDefaultPadding),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        schedule["name"],
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        schedule["job"],
                        style: TextStyle(color: kTextColor),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: kDefaultPadding * 2, bottom: kDefaultPadding),
                        padding: EdgeInsets.symmetric(
                            horizontal: kDefaultPadding / 2),
                        color: kSecondaryColor,
                        child: Row(children: [
                          Icon(
                            Icons.star,
                            color: kIconTextColor,
                          ),
                          Text(
                            schedule["rate"].toString(),
                            style: TextStyle(color: kIconTextColor),
                          )
                        ]),
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            color: kTextColor,
                          ),
                          Text(
                            schedule["distance"] + " away",
                            style: TextStyle(color: kTextColor),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ]),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: kDefaultPadding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Date",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Change",
                    style: TextStyle(color: kTextColor),
                  )
                ],
              ),
            ),
            StatefulBuilder(
              builder: ((context, setState) {
                return Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        DatePicker.showDatePicker(context,
                            showTitleActions: true,
                            minTime: DateTime(2022, 10, 8),
                            maxTime: DateTime(2100, 1, 1),
                            currentTime: DateTime.now(),
                            onChanged: (date) {}, onConfirm: (date) {
                          DatePicker.showTime12hPicker(context,
                              onConfirm: (_time) {
                            setState(() {
                              time = DateTime(date.year, date.month, date.day,
                                  _time.hour, _time.minute);
                            });
                          });
                        }, locale: LocaleType.en);
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: kDefaultPadding),
                        padding: EdgeInsets.all(kDefaultPadding),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: kSecondaryColor),
                        child: Icon(
                          Icons.calendar_month,
                          color: kIconTextColor,
                        ),
                      ),
                    ),
                    Text(
                      DateFormat("EEEE").format(time) +
                          ", " +
                          DateFormat("yMMMMd").format(time) +
                          " | " +
                          DateFormat("jm").format(time),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                );
              }),
            ),
            SizedBox(
              height: kDefaultPadding,
            ),
            Divider(),
            Container(
              margin: EdgeInsets.symmetric(vertical: kDefaultPadding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Reason",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Change",
                    style: TextStyle(color: kTextColor),
                  )
                ],
              ),
            ),
            StatefulBuilder(
              builder: ((context, setState) {
                return Container(
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          showBottomSheet(
                              context: context,
                              builder: (context) {
                                return Container(
                                  padding: EdgeInsets.all(kDefaultPadding),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10),
                                      ),
                                      color: kSecondaryColor),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              right: kDefaultPadding),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: kDefaultPadding),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              border: Border.all(width: 0.1)),
                                          child: TextField(
                                            decoration: InputDecoration(
                                                border: InputBorder.none,
                                                hintText: "Type your reason...",
                                                hintStyle: TextStyle(
                                                    color: kIconTextColor)),
                                            onChanged: (value) {
                                              setState(() {
                                                reason = value;
                                              });
                                            }
                                          ),
                                        ),
                                      ),
                                      TextButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: Text(
                                          "Cancel",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        style: TextButton.styleFrom(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          padding: EdgeInsets.symmetric(
                                              vertical: kDefaultPadding * 1.5),
                                          backgroundColor: Colors.blue,
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              });
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: kDefaultPadding),
                          padding: EdgeInsets.all(kDefaultPadding),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: kSecondaryColor),
                          child: Icon(
                            Icons.edit_calendar,
                            color: kIconTextColor,
                          ),
                        ),
                      ),
                      Text(
                        reason,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                );
              }),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.symmetric(vertical: kDefaultPadding),
              child: Text(
                "Payment Detail",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: kDefaultPadding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Consultation",
                    style: TextStyle(color: kTextColor),
                  ),
                  Text("\$" + schedule["fee"].toStringAsFixed(2)),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: kDefaultPadding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Admin Fee",
                    style: TextStyle(color: kTextColor),
                  ),
                  Text("\$" + (schedule["fee"] * 0.02).toStringAsFixed(2)),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: kDefaultPadding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Additional Discount",
                    style: TextStyle(color: kTextColor),
                  ),
                  Text("-"),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: kDefaultPadding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Total",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "\$" + (schedule["fee"] * 1.02).toStringAsFixed(2),
                    style: TextStyle(color: kIconTextColor),
                  ),
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.symmetric(vertical: kDefaultPadding),
              child: Text(
                "Payment Method",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
            Container(
              width: double.maxFinite,
              padding: EdgeInsets.symmetric(
                  vertical: kDefaultPadding / 2, horizontal: kDefaultPadding),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(width: 0.1),
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                  iconSize: 0.0,
                  icon: Text(
                    "Change",
                    style: TextStyle(fontSize: 14, color: kTextColor),
                  ),
                  value: payment_method,
                  onChanged: ((String? _value) => setState(() {
                        payment_method = _value;
                      })),
                  items: [
                    DropdownMenuItem(
                        child: SvgPicture.asset(
                          "assets/icons/visa.svg",
                          width: 100,
                          height: 40,
                          fit: BoxFit.cover,
                        ),
                        value: "visa"),
                    DropdownMenuItem(
                        child: SvgPicture.asset(
                          "assets/icons/paypal.svg",
                          width: 100,
                          height: 40,
                          fit: BoxFit.cover,
                        ),
                        value: "paypal"),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: kDefaultPadding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Total",
                        style: TextStyle(fontSize: 16, color: kTextColor),
                      ),
                      SizedBox(
                        height: kDefaultPadding / 2,
                      ),
                      Text(
                        "\$" + (schedule["fee"] * 1.02).toStringAsFixed(2),
                        style: TextStyle(
                            fontSize: 19, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  TextButton(
                      style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        padding: EdgeInsets.symmetric(
                            vertical: kDefaultPadding * 2,
                            horizontal: kDefaultPadding * 6),
                        backgroundColor: kIconPrimaryColor,
                      ),
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                alignment: Alignment.center,
                                title: Column(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: kDefaultPadding * 3,
                                          bottom: kDefaultPadding * 2),
                                      padding:
                                          EdgeInsets.all(kDefaultPadding * 3),
                                      decoration: BoxDecoration(
                                          color: Colors.grey.withOpacity(0.2),
                                          shape: BoxShape.circle),
                                      child: Icon(
                                        Icons.check,
                                        color: kIconPrimaryColor,
                                        size: 30,
                                      ),
                                    ),
                                    Text(
                                      "Payment success",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                content: Text(
                                  textAlign: TextAlign.center,
                                  "Your payment has been successful, you can have a consultation session with your trusted doctor",
                                  style: TextStyle(
                                    color: kTextColor,
                                  ),
                                ),
                                actions: [
                                  Container(
                                    margin: EdgeInsets.only(
                                        bottom: kDefaultPadding * 2),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        TextButton(
                                            style: TextButton.styleFrom(
                                                padding: EdgeInsets.symmetric(
                                                    vertical:
                                                        kDefaultPadding * 2,
                                                    horizontal:
                                                        kDefaultPadding * 6),
                                                backgroundColor:
                                                    kIconPrimaryColor,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            30))),
                                            onPressed: () {
                                              Get.toNamed(kChatPage);
                                            },
                                            child: Text(
                                              "Chat Doctor",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            )),
                                      ],
                                    ),
                                  )
                                ],
                              );
                            });
                      },
                      child: Text("Booking",
                          style: TextStyle(
                            color: Colors.white,
                          )))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
