import 'dart:async';

import 'package:booking/components/bottom_bar.dart';
import 'package:booking/components/constants.dart';
import 'package:booking/route_management/all_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import '../components/my_app_bar.dart';
import '../controllers/schedule_page_controller.dart';
import 'components/body.dart';

class SchedulePage extends StatefulWidget {
  const SchedulePage({Key? key}) : super(key: key);

  @override
  State<SchedulePage> createState() => _SchedulePageState();
}

class _SchedulePageState extends State<SchedulePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: kIconPrimaryColor,
          child: Icon(Icons.post_add_sharp,),
        onPressed: () {
          Get.toNamed(kDoctorListPage);
        },),
        appBar: MyAppBar(
          title: "Schedule",
          tab1: "Upcoming",
          tab2: "Completed",
          tab3: "Canceled",
          icon: bellIcon,
        ),
        body: TabBarView(children: [
          Body(),
          Container(),
          Container(),
        ]),
        bottomNavigationBar: BottomBar(
          current_page: 2,
        ),
      ),
    );
  }
}
