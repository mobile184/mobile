import 'dart:async';
import 'dart:convert';

import 'package:booking/components/constants.dart';
import 'package:booking/controllers/schedule_page_controller.dart';
import 'package:booking/route_management/all_routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';

class Body extends StatefulWidget {
  Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
    SchedulePageController controller = Get.find();
  
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
        await this.controller.readData(context);
        setState(() { });
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
            decoration: BoxDecoration(color: Colors.white),
            child: Column(children: [
              //Schedule List
              SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: kDefaultPadding,),
                    for (int i = 0; i < controller.schedule.length; i++)
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: kDefaultPadding * 2, vertical: kDefaultPadding),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(width: 0.1),
                        ),
                        child: Column(children: [
                          ListTile(
                            title: Text(
                              controller.schedule[i]["name"],
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(
                              controller.schedule[i]["job"],
                            ),
                            trailing: CircleAvatar(
                              backgroundImage:
                                  NetworkImage(controller.schedule[i]["avatar"]),
                            ),
                          ),
                          SizedBox(
                            height: kDefaultPadding,
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: kDefaultPadding,
                              ),
                              Icon(Icons.calendar_month, size: 20,),
                              Text(controller.schedule[i]["time"].substring(0, 10)),
                              SizedBox(
                                width: kDefaultPadding,
                              ),
                              Icon(Icons.access_time_outlined, size: 20,),
                              Text(controller.schedule[i]["time"].substring(10, 16)),
                              SizedBox(
                                width: kDefaultPadding,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                width: 10,
                                height: 10,
                                decoration: BoxDecoration(
                                    color: controller.schedule[i]["state"] == "Confirmed"
                                        ? Colors.green
                                        : kSecondaryColor,
                                    shape: BoxShape.circle),
                              ),
                              Text(controller.schedule[i]["state"]),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                                vertical: kDefaultPadding * 1.5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: kDefaultPadding * 3,
                                      vertical: kDefaultPadding * 1.5),
                                  decoration: BoxDecoration(
                                      color: kSecondaryColor,
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Text("Cancel"),
                                ),
                                SizedBox(
                                  width: kDefaultPadding,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Get.toNamed(kCreatePage, arguments: {
                                      "avatar": controller.schedule[i]["avatar"],
                                      "name": controller.schedule[i]["name"],
                                      "job": controller.schedule[i]["job"],
                                      "rate": controller.schedule[i]["rate"],
                                      "fee": controller.schedule[i]["fee"],
                                      "distance": controller.schedule[i]["distance"],
                                      "reason": controller.schedule[i]["reason"],
                                      "time": controller.schedule[i]["time"],
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: kDefaultPadding * 3,
                                        vertical: kDefaultPadding * 1.5),
                                    decoration: BoxDecoration(
                                        color: kIconPrimaryColor,
                                        borderRadius: BorderRadius.circular(10)),
                                    child: Text(
                                      "Reschedule",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ]),
                      ),
                  ],
                ),
              ),
            ]),
          );
  }
}
