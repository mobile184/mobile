import 'package:booking/components/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatefulWidget {
  final name;
  final avatar;
  final message;
  const Body({Key? key, required this.name, this.avatar, this.message})
      : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                //Consultion Start
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 0.1)),
                  width: double.maxFinite,
                  padding: EdgeInsets.all(kDefaultPadding * 2),
                  margin: EdgeInsets.all(kDefaultPadding * 2),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Consultion Start",
                          style: TextStyle(
                              color: kIconTextColor,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: kDefaultPadding,
                        ),
                        Text(
                          "You can consult your problem to your doctor",
                          style: TextStyle(
                              color: kTextColor, fontWeight: FontWeight.w500),
                        )
                      ]),
                ),
                //Message Content
                for (int i = 0; i < widget.message.length; i++)
                  widget.message[i]["isSender"] == true
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  color: kIconPrimaryColor,
                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10), bottomLeft: Radius.circular(10))),
                              margin: EdgeInsets.symmetric(
                                  horizontal: kDefaultPadding * 2,
                                  vertical: kDefaultPadding),
                              padding: EdgeInsets.all(kDefaultPadding),
                              child: Container(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Container(
                                        constraints: BoxConstraints(
                                            maxWidth: Get.width * 0.5),
                                        child: Text(
                                          widget.message[i]["text"],
                                          style: TextStyle(color: Colors.white),
                                        )),
                                    if (widget.message[i]["state"] == "viewed")
                                      Container(
                                          alignment: Alignment.bottomCenter,
                                          child: SvgPicture.asset(
                                            "assets/icons/done.svg",
                                            width: 16,
                                            color: Colors.white,
                                          )),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      : Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: kDefaultPadding * 2),
                          padding:
                              EdgeInsets.symmetric(vertical: kDefaultPadding),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      CircleAvatar(
                                        backgroundImage:
                                            NetworkImage(widget.avatar),
                                        radius: 20,
                                      ),
                                      SizedBox(
                                        width: kDefaultPadding,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            widget.name,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            widget.message[i]["time"],
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 12),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  Container(
                                    constraints: BoxConstraints(
                                        maxWidth:
                                            MediaQuery.of(context).size.width *
                                                0.6),
                                    decoration: BoxDecoration(
                                        color: kSecondaryColor,
                                        borderRadius:
                                            BorderRadius.only(topRight: Radius.circular(10), bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10))),
                                    margin:
                                        EdgeInsets.only(top: kDefaultPadding),
                                    padding: EdgeInsets.all(kDefaultPadding),
                                    child: Text(widget.message[i]["text"]),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
              ],
            ),
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: kDefaultPadding),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 0.1)),
                margin: EdgeInsets.symmetric(horizontal: kDefaultPadding * 2, vertical: kDefaultPadding),
                child: Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Type message...",
                            hintStyle: TextStyle(color: Colors.grey)),
                      ),
                    ),
                    SvgPicture.asset("assets/icons/link.svg", width: 20, color: Colors.grey,),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: kDefaultPadding * 2),
              padding: EdgeInsets.symmetric(horizontal: kDefaultPadding * 3.5, vertical: kDefaultPadding * 1.75),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(30),
              color: kIconPrimaryColor),
              child: Text("Send", style: TextStyle(color: Colors.white),),
            )
          ],
        )
      ],
    );
  }
}
