import 'package:booking/components/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../route_management/all_routes.dart';
import 'components/body.dart';

class ChatDetailPage extends StatelessWidget {
  const ChatDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var message = Get.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onTap: () {
            Get.back();
          },
        ),
        title: Text(
          message["name"],
          style: TextStyle(color: Colors.black, fontSize: 18),
        ),
        actions: [
          GestureDetector(
              onTap: () {
                Get.toNamed(kVideoCallPage, arguments: {
                  "avatar": message["avatar"],
                  "name": message["name"],
                });
              },
              child: Icon(
                Icons.videocam_rounded,
                color: Colors.black,
              )),
          SizedBox(
            width: kDefaultPadding,
          ),
          Icon(
            Icons.call,
            color: Colors.black,
          ),
          SizedBox(
            width: kDefaultPadding,
          ),
          Icon(
            Icons.more_vert,
            color: Colors.black,
          ),
          SizedBox(
            width: kDefaultPadding,
          ),
        ],
      ),
      body: Body(
          name: message["name"],
          avatar: message["avatar"],
          message: message["message"]),
    );
  }
}
