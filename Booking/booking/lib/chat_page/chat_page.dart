import 'package:booking/components/bottom_bar.dart';
import 'package:booking/components/constants.dart';
import 'package:booking/components/my_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'components/body.dart';

class ChatPage extends StatelessWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: MyAppBar(title: "Message", tab1: "All", tab2: "Group", tab3: "Private", icon: searchIcon),
        body: TabBarView(children: [
          Body(),
          Container(),
          Container(),
        ]),
        bottomNavigationBar: BottomBar(current_page: 3),
        floatingActionButton: FloatingActionButton(
          backgroundColor: kIconPrimaryColor,
          onPressed: () {},
        child: SvgPicture.asset(
          "assets/icons/messenger.svg",
          color: Colors.white,
        ),
        ),
      ),
    );
  }
}