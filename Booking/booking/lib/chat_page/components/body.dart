import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import '../../components/constants.dart';
import '../../controllers/chat_page_controller.dart';
import '../../controllers/schedule_page_controller.dart';
import '../../route_management/all_routes.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final state = 0;
  ChatPageController controller = Get.find();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await controller.readData(context);
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: kDefaultPadding * 2,),
        //List Chat
        SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              for (int i = 0; i < controller.chat.length; i++)
                ListTile(
                  onTap: () {
                    Get.toNamed(kChatDetailPage, arguments: 
                    {
                      "avatar": controller.chat[i]["avatar"],
                      "name": controller.chat[i]["name"],
                      "message": controller.chat[i]["message"],
                  });
                  },
                  leading: CircleAvatar(
                    radius: 25,
                    backgroundImage:
                        NetworkImage(controller.chat[i]["avatar"]),
                  ),
                  title: Text(
                    controller.chat[i]["name"],
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text(
                    controller.chat[i]["message"]
                        [controller.chat[i]["message"].length - 1]["text"],
                    overflow: TextOverflow.ellipsis,
                  ),
                  trailing: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(controller.chat[i]["last_send"]),
                      SizedBox(
                        height: kDefaultPadding / 2,
                      ),
                      controller.chat[i]["message"][
                                  controller.chat[i]["message"].length -
                                      1]["state"] ==
                              "viewed"
                          ? Icon(
                              Icons.check,
                              color: kTextColor,
                            )
                          : Icon(
                              Icons.access_time_filled,
                              color: kIconPrimaryColor,
                              size: 18,
                            ),
                    ],
                  ),
                ),
            ],
          ),
        )
      ],
    );
  }
}
