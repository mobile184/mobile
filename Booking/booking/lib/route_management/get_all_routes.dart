import 'package:booking/chat_page/chat_page.dart';
import 'package:booking/create_page/create_page.dart';
import 'package:booking/route_management/all_routes.dart';
import 'package:booking/route_management/bindings.dart';
import 'package:booking/schedule_page/schedule_page.dart';
import 'package:booking/search_page/search_page.dart';
import 'package:get/get.dart';

import '../chat_detail_page/chat_detail_page.dart';
import '../doctor_list_page/doctor_list_page.dart';
import '../video_call_page/video_call_page.dart';
import '../welcome_page/welcome_page.dart';

class GetAllRoute {
  static List<GetPage> getAllRoute() {
    return [
      GetPage(
        binding: DoctorListPageBindings(),
          name: kDoctorListPage,
          page: () => DoctorListPage(),
          transitionDuration: Duration(seconds: 0)),
      GetPage(
          binding: ScheduleBindings(),
          name: kSchedulePage,
          page: () => SchedulePage(),
          transitionDuration: Duration(seconds: 0)),
      GetPage(
          binding: ChatBindings(),
          name: kChatPage,
          page: () => ChatPage(),
          transitionDuration: Duration(seconds: 0)),
      GetPage(
          name: kChatDetailPage,
          page: () => ChatDetailPage(),
          transitionDuration: Duration(seconds: 0)),
      GetPage(
          name: kVideoCallPage,
          page: () => VideoCallPage(),
          transitionDuration: Duration(seconds: 0)),
      GetPage(
          name: kCreatePage,
          page: () => CreatePage(),
          transitionDuration: Duration(seconds: 0)),
      GetPage(
          name: kWelcomePage,
          page: () => WelcomePage(),
          transitionDuration: Duration(seconds: 0)),
      GetPage(
          name: kSearchPage,
          page: () => SearchPage(),
          transitionDuration: Duration(seconds: 0)),
    ];
  }
}
