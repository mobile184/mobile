import 'package:booking/controllers/schedule_page_controller.dart';
import 'package:get/get.dart';

import '../controllers/chat_page_controller.dart';
import '../controllers/doctor_list_page_controller.dart';

class ScheduleBindings extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut(() => SchedulePageController());
  }
}

class ChatBindings extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut(() => ChatPageController());
  }
}

class DoctorListPageBindings extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut(() => DoctorListPageController());
  }
}
