const kWelcomePage = "/";
const kSchedulePage = "/schedule";
const kChatPage = "/chat";
const kCreatePage = "/create";
const kSearchPage = "/search";
const kChatDetailPage = "/chat/detail";
const kVideoCallPage = "/chat/detail/video_call";
const kDoctorListPage = "/doctor";