import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:shop_app/core/const.dart';
import 'package:shop_app/models/shoe_model.dart';
import 'package:shop_app/pages/category_detail_page/detail_page.dart';
import 'package:shop_app/widgets/app_clipper.dart';

import '../../core/flutter_icons.dart';
import 'dart:math' as math;

class CategoryPage extends StatefulWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  List<ShoeModel> shoeList = ShoeModel.list;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Icon(
          FlutterIcons.menu,
          color: Colors.black,
        ),
      ),
      body: ListView(
        children: [
          _categoriesHeader(),
          SizedBox(
            height: 30,
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 16),
            height: 260,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: shoeList.length,
              physics: BouncingScrollPhysics(),
              itemBuilder: ((context, index) {
                return _buildItemWithClipper(index);
              }),
            ),
          ),
          _itemListHeader(),
          SizedBox(
            height: 16,
          ),
          ...shoeList.map((data) {
            return _buildItem(data);
          })
        ],
      ),
      bottomNavigationBar: _buildBottomBar(),
    );
  }

  Widget _buildBottomBar() {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(
              30,
            ),
            topRight: Radius.circular(30),
          ),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              spreadRadius: 1,
              blurRadius: 10,
            )
          ]),
      child: BottomNavigationBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          selectedItemColor: AppColors.greenColor,
          unselectedItemColor: Colors.black26,
          currentIndex: 1,
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: [
            BottomNavigationBarItem(
                icon: Icon(FlutterIcons.compass), label: "Data"),
            BottomNavigationBarItem(
                icon: Icon(FlutterIcons.list), label: "Data"),
            BottomNavigationBarItem(
                icon: Icon(FlutterIcons.bag), label: "Data"),
            BottomNavigationBarItem(
                icon: Icon(FlutterIcons.person_outline), label: "Data"),
          ]),
    );
  }

  Widget _buildItem(ShoeModel data) {
    return GestureDetector(
      onTap: () {
        Get.to(() => DetailPage(shoeModel: data));
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 10,
                spreadRadius: 1,
              )
            ]),
        margin: EdgeInsets.only(left: 16, right: 16, bottom: 12),
        padding: EdgeInsets.all(10),
        child: Row(children: [
          Image(
            image: AssetImage("assets/${data.imgPath}"),
            width: 80,
            height: 60,
          ),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    width: Get.width * 0.4,
                    child: Text(
                      data.name,
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    )),
                Text(
                  data.brand,
                  style: TextStyle(
                    color: Colors.black54,
                  ),
                ),
              ],
            ),
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Text(
                "\$" + data.price.toString(),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              )),
        ]),
      ),
    );
  }

  Widget _itemListHeader() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "JUST FOR YOU",
            style:
                TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),
          ),
          Text(
            "VIEW ALL",
            style: TextStyle(color: AppColors.greenColor),
          ),
        ],
      ),
    );
  }

  Widget _buildItemWithClipper(int index) {
    return GestureDetector(
      onTap: () {
        Get.to(() => DetailPage(shoeModel: shoeList[index]));
      },
      child: Container(
        margin: EdgeInsets.only(right: 16),
        width: 190,
        child: Stack(
          children: [
            _buildBackground(index, 200),
            Positioned(
              bottom: 130,
              right: 10,
              child: Hero(
                tag: "hero${shoeList[index].imgPath}",
                child: Transform.rotate(
                    angle: -math.pi / 7,
                    child: Image(
                      width: 190,
                      image: AssetImage("assets/${shoeList[index].imgPath}"),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _categoriesHeader() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Categories",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 28),
          ),
          IconButton(
            icon: Icon(
              FlutterIcons.search,
              color: Colors.black26,
            ),
            onPressed: () {},
          ),
        ],
      ),
    );
  }

  Widget _buildBackground(int index, double width) {
    return ClipPath(
      clipper: AppClipper(cornerSize: 25, diagonalHeight: 90),
      child: Container(
        color: shoeList[index].color,
        width: width,
        height: 300,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.all(16),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Icon(
                          shoeList[index].brand == "Nike"
                              ? FlutterIcons.nike
                              : FlutterIcons.converse,
                          size: 30,
                          color: Colors.white,
                        )),
                    Expanded(child: SizedBox()),
                    Container(
                        width: 100,
                        child: Text(
                          shoeList[index].name,
                          style: TextStyle(color: Colors.white),
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      shoeList[index].price.toString(),
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    )
                  ]),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  color: AppColors.greenColor,
                ),
                child: Center(
                    child: Icon(
                  FlutterIcons.add,
                  color: Colors.white,
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
