import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:shop_app/core/const.dart';
import 'package:shop_app/models/category_model.dart';
import 'package:shop_app/models/flash_sale_model.dart';
import 'package:shop_app/models/shortcut_model.dart';
import 'package:shop_app/models/suggestion_model.dart';
import 'package:shop_app/pages/category_page/category_page.dart';

import '../../../widgets/flash_sale_clipper.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<String> images = [
    "https://thumbs.dreamstime.com/b/mascara-lipsticks-testers-skin-care-products-cosmetic-store-mascara-lipsticks-testers-skin-care-products-134617855.jpg",
    "https://thumbs.dreamstime.com/b/set-decorative-cosmetic-powder-concealer-eye-shadow-brush-blush-foundation-cosmetic-cream-beauty-care-cosmetic-white-113351358.jpg",
    "https://thumbs.dreamstime.com/b/white-cosmetic-tubes-pink-background-copy-space-skin-care-body-treatment-beauty-concept-136890884.jpg"
  ];
  @override
  Widget build(BuildContext context) {
    double _curSliderValue = 0;
    int activePage = 1;
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        child: Column(children: [
          Container(
            height: 270,
            child: Stack(
              children: [
                _buildSlider(),
                SizedBox(
                  height: 10,
                ),
                _buildIndicator(activePage),
                _buildWalletInfo(),
                _buildSearchBar(),
              ],
            ),
          ),
          Container(
            height: 180,
            padding: EdgeInsets.only(left: 16),
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: (shortcutList.length / 2).floor(),
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      _buildShortcutItem(index),
                      SizedBox(
                        height: 16,
                      ),
                      _buildShortcutItem(
                          (shortcutList.length / 2).floor() + index)
                    ],
                  );
                }),
          ),
          _buildHeader(title: "FLASH SALE", isSale: true, canMore: true),
          Container(
            height: 220,
            decoration: BoxDecoration(color: Colors.white),
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: flashSaleList.length,
                itemBuilder: (context, index) {
                  return _buildFlashSale(index);
                }),
          ),
          SizedBox(
            height: 10,
          ),
          _buildHeader(title: "DANH MỤC", isSale: false, canMore: true),
          Container(
            height: 280,
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
            decoration: BoxDecoration(color: Colors.white),
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: (categoryList.length / 2).floor(),
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      _buildCategory(index),
                      SizedBox(
                        height: 10,
                      ),
                      _buildCategory((categoryList.length / 2).floor() + index),
                    ],
                  );
                }),
          ),
          SizedBox(
            height: 10,
          ),
          _buildHeader(title: "GỢI Ý", isSale: false, canMore: false),
          Container(
            height: 90,
            decoration: BoxDecoration(color: Colors.white),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: suggestionList.length,
                itemBuilder: (context, index) {
                  return _buildSuggestion(index);
                }),
          ),
        ]),
      ),
    );
  }

  Widget _buildSuggestion(int index) {
    return Container(
      margin: EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 0.5, color: Colors.red)),
      width: 60,
      child: Column(
        children: [
          Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: SvgPicture.asset(
                "assets/icons/${suggestionList[index].image}.svg",
                width: 40,
                height: 40,
              )),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: Text(
              suggestionList[index].name,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.red, fontSize: 10),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCategory(int index) {
    return GestureDetector(
      onTap: () {
        Get.to(() => CategoryPage());
      },
      child: Container(
        margin: EdgeInsets.only(right: 16),
        child: Column(
          children: [
            CircleAvatar(
              backgroundColor: Colors.black12,
              backgroundImage: NetworkImage(categoryList[index].image),
              radius: 40,
            ),
            SizedBox(
              height: 16,
            ),
            Text(categoryList[index].name,
                style: TextStyle(color: Colors.black54)),
          ],
        ),
      ),
    );
  }

  Widget _buildFlashSale(int index) {
    return Container(
      margin: EdgeInsets.only(right: 5),
      color: Colors.white,
      child: Column(
        children: [
          Container(
            height: 150,
            width: 150,
            child: Stack(children: [
              AspectRatio(
                aspectRatio: 1,
                child: Image.network(
                  flashSaleList[index].image,
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                right: 0,
                top: 0,
                child: ClipPath(
                  clipper: FlashSaleClipper(distance: 8),
                  child: Container(
                    width: 40,
                    height: 40,
                    padding: EdgeInsets.only(top: 3),
                    decoration: BoxDecoration(color: Colors.amber),
                    child: Column(children: [
                      Text(
                        "${flashSaleList[index].sale}%",
                        style: TextStyle(color: Colors.red, fontSize: 12),
                      ),
                      Text(
                        "Sale",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 13),
                      ),
                    ]),
                  ),
                ),
              ),
            ]),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Text(
                  "${flashSaleList[index].price}",
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                ),
                Text(
                  "đ",
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.red,
                      decoration: TextDecoration.underline),
                ),
              ],
            ),
          ),
          LinearPercentIndicator(
            barRadius: Radius.circular(10),
            percent: 0.3,
            center: Text(
              "${flashSaleList[index].sold} đã bán",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 11,
                  fontWeight: FontWeight.bold),
            ),
            width: 150,
            lineHeight: 15,
          ),
        ],
      ),
    );
  }

  Widget _buildHeader({title, isSale, canMore}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      decoration: BoxDecoration(boxShadow: [appBoxShadow], color: Colors.white),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            title,
            style: TextStyle(
                fontSize: 14, fontWeight: FontWeight.w800, color: Colors.red),
          ),
          SizedBox(
            width: 10,
          ),
          if (isSale == true)
            Row(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 2),
                  decoration: BoxDecoration(color: Colors.black),
                  child: Text(
                    "10",
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                ),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 5),
                    child: Text(":")),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 2),
                  decoration: BoxDecoration(color: Colors.black),
                  child: Text(
                    "29",
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                ),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 5),
                    child: Text(":")),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 2),
                  decoration: BoxDecoration(color: Colors.black),
                  child: Text(
                    "36",
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                ),
              ],
            ),
          Spacer(),
          if (canMore == true)
            GestureDetector(
              onTap: () {},
              child: Row(
                children: [
                  Text(
                    "See all",
                    style: TextStyle(color: subTextColor),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: subTextColor,
                    size: 16,
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildShortcutItem(int index) {
    return Container(
      margin: EdgeInsets.only(right: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(bottom: 5),
            decoration: BoxDecoration(
                boxShadow: [appBoxShadow],
                color: Colors.white,
                borderRadius: BorderRadius.circular(10)),
            child: SvgPicture.asset(
              "assets/icons/${shortcutList[index].image}.svg",
              width: 30,
              height: 30,
            ),
          ),
          Text(shortcutList[index].name),
        ],
      ),
    );
  }

  Widget _buildSearchBar() {
    return Container(
      margin: EdgeInsets.only(top: 30, left: 16, right: 16),
      child: Row(children: [
        Container(
          width: Get.width * 0.7,
          decoration: (BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          )),
          child: TextField(
              decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
            border: InputBorder.none,
            hintStyle: TextStyle(fontSize: 13, color: Colors.red),
            hintText: "SEACH FOR YOUR FAVOURITE ITEM",
          )),
        ),
        SizedBox(
          width: 16,
        ),
        SvgPicture.asset(
          "assets/icons/cart.svg",
          width: 22,
          color: Colors.white,
        ),
        SizedBox(
          width: 16,
        ),
        Stack(
          children: [
            SvgPicture.asset(
              "assets/icons/chat.svg",
              width: 22,
              color: Colors.white,
            ),
            Positioned(
                right: 0,
                top: 0,
                child: Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                        color: Colors.white, shape: BoxShape.circle),
                    child: Container(
                      width: 8,
                      height: 8,
                      decoration: BoxDecoration(
                          color: Colors.red, shape: BoxShape.circle),
                      child: Center(
                          child: Text(
                        "2",
                        style: TextStyle(fontSize: 8, color: Colors.white),
                      )),
                    ))),
          ],
        ),
      ]),
    );
  }

  Widget _buildWalletInfo() {
    return Positioned(
      top: 190,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        decoration: BoxDecoration(
            boxShadow: [appBoxShadow],
            color: Colors.white,
            borderRadius: BorderRadius.circular(10)),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          SvgPicture.asset(
            "assets/icons/scan.svg",
            width: 30,
          ),
          VerticalDivider(
            width: 20,
            thickness: 0.1,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Icon(
                    Icons.wallet,
                    size: 20,
                    color: Colors.red,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "Ví của bạn",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              Text(
                "\$100",
                style: TextStyle(fontSize: 13, color: subTextColor),
              )
            ],
          ),
          VerticalDivider(
            width: 20,
            thickness: 0.1,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Icon(
                    Icons.monetization_on_outlined,
                    size: 20,
                    color: Colors.amber,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "0 xu",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              Text(
                "Đổi xu lấy voucher",
                style: TextStyle(color: subTextColor, fontSize: 13),
              )
            ],
          ),
        ]),
      ),
    );
  }

  Widget _buildIndicator(int activePage) {
    return Positioned(
      top: 170,
      left: Get.width / 2 - 20,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: indicators(images.length, activePage)),
    );
  }

  Widget _buildSlider() {
    return CarouselSlider(
      options: CarouselOptions(
        autoPlay: true,
        viewportFraction: 1,
        aspectRatio: 2.0,
        height: 200.0,
        enlargeCenterPage: true,
        onPageChanged: (position, reason) {},
        enableInfiniteScroll: false,
      ),
      items: images.map<Widget>((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
                width: Get.width,
                height: double.maxFinite,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(i), fit: BoxFit.cover)));
          },
        );
      }).toList(),
    );
  }
}

List<Widget> indicators(imagesLength, currentIndex) {
  return List<Widget>.generate(imagesLength, (index) {
    return Container(
      margin: EdgeInsets.all(3),
      width: 8,
      height: 8,
      decoration: BoxDecoration(
          color: currentIndex == index ? Colors.black : Colors.black26,
          shape: BoxShape.circle),
    );
  });
}
