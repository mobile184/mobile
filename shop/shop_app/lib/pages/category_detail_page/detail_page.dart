import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:shop_app/core/const.dart';
import 'package:shop_app/core/flutter_icons.dart';
import 'package:shop_app/models/shoe_model.dart';
import 'package:shop_app/widgets/app_clipper.dart';
import 'dart:math' as math;

class DetailPage extends StatefulWidget {
  final ShoeModel shoeModel;
  const DetailPage({Key? key, required this.shoeModel}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.shoeModel.color,
      appBar: AppBar(
        backgroundColor: widget.shoeModel.color,
        elevation: 0.0,
        title: Text("CATEGORIES"),
        leading: IconButton(icon: Icon(FlutterIcons.left_open_1),
        onPressed: () {
          Get.back();
        },),
      ),
      body: Container(
        height: Get.height,
        width: Get.width,
        child: Stack(children: [
          Positioned(
            bottom: 0,
            child: _buildAddCart(),
          ),
          Positioned(
            bottom: 0,
            child: _buildBottom(),
          ),
          _buildItemImage(),
        ]),
      ),
    );
  }

  Widget _buildItemImage() {
    return Padding(
          padding: EdgeInsets.only(top: 20),
          child: Hero(
            tag: "hero${widget.shoeModel.imgPath}",
            child: Transform.rotate(
              angle: -math.pi / 7,
              child: Image(
                    
                width: Get.width * 0.75,
                image: AssetImage("assets/${widget.shoeModel.imgPath}"),
              ),
            ),
          ),
        );
  }

  Widget _buildAddCart() {
    return Container(
            height: Get.height * 0.82,
            width: Get.width,
            child: ClipPath(
              clipper: AppClipper(
                  cornerSize: 50, diagonalHeight: 150, roundedBottom: false),
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.only(top: 160, left: 16, right: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 250,
                      child: Text(
                        widget.shoeModel.name,
                        style: TextStyle(fontSize: 28),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    _buildRating(),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "DETAILS",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      widget.shoeModel.desc,
                      style: TextStyle(color: Colors.black38),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "COLOR OPTIONS",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        _buildColor(AppColors.blueColor),
                        _buildColor(AppColors.greenColor),
                        _buildColor(AppColors.orangeColor),
                        _buildColor(AppColors.redColor),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
  }

  Widget _buildBottom() {
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              spreadRadius: 1,
              blurRadius: 10,
            )
          ]),
      padding: EdgeInsets.all(14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "PRICE",
                style: TextStyle(color: Colors.black26),
              ),
              Text(
                "\$" + widget.shoeModel.price.toString(),
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 28),
              ),
            ],
          ),
          Container(
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 14),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: AppColors.greenColor,
              ),
              child: Text(
                "ADD CART",
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
    );
  }

  Widget _buildColor(Color color) {
    return Container(
      margin: EdgeInsets.only(right: 8),
      width: 20,
      height: 20,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(50), color: color),
    );
  }

  Widget _buildRating() {
    return Row(
      children: [
        RatingBar.builder(
          initialRating: 3,
          minRating: 1,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          itemSize: 20,
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: Colors.amber,
          ),
          onRatingUpdate: (rating) {
            print(rating);
          },
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          "134 Reviews",
          style: TextStyle(color: Colors.black12),
        )
      ],
    );
  }
}
