import 'package:flutter/cupertino.dart';

class FlashSaleClipper extends CustomClipper<Path> {
  final distance;
  FlashSaleClipper({required this.distance});
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.moveTo(size.width / 2, size.height - distance);
    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.moveTo(0, 0);
    path.addRect(Rect.fromLTWH(0, 0, size.width - distance, size.height));
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }

}