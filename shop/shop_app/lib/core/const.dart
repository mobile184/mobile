import 'package:flutter/material.dart';

class AppColors {
  static final Color blueColor = Color(0XFF0c99c3);
  static final Color greenColor = Color(0XFF3dc39d);
  static final Color yellowColor = Color(0XFFdac007);
  static final Color redColor = Color(0XFFbe0b2b);
  static final Color orangeColor = Color(0XFFbe740b);
}

const BoxShadow appBoxShadow = BoxShadow(
  color: Colors.black12,
  blurRadius: 10,
  spreadRadius: 1,
);
const Color subTextColor = Colors.black54;
