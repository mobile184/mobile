class Categories {
  final String name;
  final String image;
  Categories({required this.name, required this.image});
}

List<Categories> categoryList = [
  Categories(name: "Quần áo", image: "https://thumbs.dreamstime.com/b/child-girl-bright-summer-clothes-isolated-yellow-blue-colors-set-baby-collage-spring-clothing-63354098.jpg"),
  Categories(name: "Giày", image: "https://thumbs.dreamstime.com/b/blue-shoes-29507491.jpg"),
  Categories(name: "Mỹ phẩm", image: "https://thumbs.dreamstime.com/b/cosmetic-part-face-beauty-9445152.jpg"),
  Categories(name: "Đồ gia dụng", image: "https://thumbs.dreamstime.com/b/kitchen-utensil-various-utensils-wooden-table-44331281.jpg"),
  Categories(name: "Quần áo", image: "https://thumbs.dreamstime.com/b/child-girl-bright-summer-clothes-isolated-yellow-blue-colors-set-baby-collage-spring-clothing-63354098.jpg"),
  Categories(name: "Giày", image: "https://thumbs.dreamstime.com/b/blue-shoes-29507491.jpg"),
  Categories(name: "Mỹ phẩm", image: "https://thumbs.dreamstime.com/b/cosmetic-part-face-beauty-9445152.jpg"),
  Categories(name: "Đồ gia dụng", image: "https://thumbs.dreamstime.com/b/kitchen-utensil-various-utensils-wooden-table-44331281.jpg"),
];