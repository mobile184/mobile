class Suggestion {
  final String image;
  final String name;
  Suggestion({required this.image, required this.name});
}

List<Suggestion> suggestionList = [
  Suggestion(image: "all", name: "Tất cả"),
  Suggestion(image: "exchange", name: "Hoàn xu đơn bất kỳ"),
  Suggestion(image: "voucher", name: "Mã giảm giá"),
  Suggestion(image: "utensil", name: "Đồ gia dụng"),
  Suggestion(image: "all", name: "Tất cả"),
  Suggestion(image: "exchange", name: "Hoàn xu đơn bất kỳ"),
  Suggestion(image: "voucher", name: "Mã giảm giá"),
  Suggestion(image: "utensil", name: "Đồ gia dụng"),
];