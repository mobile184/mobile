class FlashSale {
  final String name;
  final String image;
  final String sold;
  final int sale;
  final String price;
  FlashSale({required this.image, required this.name, required this.sold, required this.sale, required this.price});
}

List<FlashSale> flashSaleList = [
  FlashSale(image: "https://thumbs.dreamstime.com/b/makeup-brush-color-powder-white-background-88350996.jpg", name: "Stamp", sold: "3.66k", sale: 35, price: "15.000"),
  FlashSale(image: "https://thumbs.dreamstime.com/b/cosmetic-products-3-529895.jpg", name: "Liquid", sold: "2.6k", sale: 10, price: "120.000"),
  FlashSale(image: "https://thumbs.dreamstime.com/b/spring-set-lipsticks-pink-flowers-beauty-cosmetic-collection-fashion-trends-cosmetics-bright-lips-beautiful-bacground-88496319.jpg", name: "Lipsticks", sold: "1.08k", sale: 50, price: "80.000"),
];