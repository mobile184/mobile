class Chat {
  final String name, last_message, image, time;
  final bool isActive;

  Chat({
    required this.name,
    required this.last_message,
    required this.image,
    required this.time,
    required this.isActive,
  });
}

List chatsData = [
  Chat(
      name: 'James',
      last_message: 'Hope you are doing well...',
      image: 'assets/images/james.png',
      time: '3m ago',
      isActive: false),
  Chat(
      name: 'John',
      last_message: 'Hello Abdullar',
      image: 'assets/images/John.png',
      time: '8m ago',
      isActive: true),
  Chat(
      name: 'Marry',
      last_message: 'Hello Abdullar',
      image: 'assets/images/marry.png',
      time: '8m ago',
      isActive: true),
  Chat(
      name: 'Rosy',
      last_message: 'Hello Abdullar',
      image: 'assets/images/rosy.png',
      time: '8m ago',
      isActive: true),
];
