import 'package:travel/models/User.dart';

class Friend {
  User data;
  int mutual_friends_num;
  bool? isActive;
  Friend(
      {required this.data,
      required this.mutual_friends_num,
      this.isActive = false});
}

List<Friend> friends = [
  Friend(data: user1, mutual_friends_num: 20, isActive: false),
  Friend(data: user2, mutual_friends_num: 165, isActive: true),
  Friend(data: user3, mutual_friends_num: 38, isActive: true),
  Friend(data: user4, mutual_friends_num: 160, isActive: true),
  Friend(data: user1, mutual_friends_num: 20, isActive: false),
  Friend(data: user2, mutual_friends_num: 165, isActive: true),
  Friend(data: user3, mutual_friends_num: 38, isActive: true),
  Friend(data: user4, mutual_friends_num: 160, isActive: true),
];
