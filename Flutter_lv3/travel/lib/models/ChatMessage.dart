import 'package:flutter/material.dart';

enum MessageStatus { not_sent, not_view, viewed }

class ChatMessage {
  final String text;
  final MessageStatus messageStatus;
  final bool isSender;
  ChatMessage({
    required this.text,
    required this.messageStatus,
    required this.isSender,
  });
}

List demoChatMessage = [
  ChatMessage(
      text: 'Hi Sajo', messageStatus: MessageStatus.viewed, isSender: false),
  ChatMessage(
      text: 'Hello, how are u',
      messageStatus: MessageStatus.viewed,
      isSender: true),
  ChatMessage(
      text: 'Error happened',
      messageStatus: MessageStatus.not_sent,
      isSender: true),
  ChatMessage(
      text: 'This looks great',
      messageStatus: MessageStatus.viewed,
      isSender: false),
  ChatMessage(
      text: 'Glad you like it',
      messageStatus: MessageStatus.not_view,
      isSender: true),
];
