import 'package:flutter/material.dart';
import 'package:travel/models/User.dart';

class TravelSpot {
  String name, image;
  DateTime date;
  List<User> users;

  TravelSpot({
    required this.users,
    required this.name,
    required this.image,
    required this.date,
  });
}

List<TravelSpot> travelSpots = [
  TravelSpot(
      users: users..shuffle(),
      name: 'Red Moutains',
      image: 'assets/images/Red_Moutains.png',
      date: DateTime(2020, 10, 15)),
  TravelSpot(
      users: users..shuffle(),
      name: 'Magical World',
      image: 'assets/images/Magical_World.png',
      date: DateTime(2020, 3, 10)),
  TravelSpot(
      users: users..shuffle(),
      name: 'Red Moutains',
      image: 'assets/images/Red_Moutains.png',
      date: DateTime(2020, 10, 15)),
];

List<User> users = [user1, user2, user3];
