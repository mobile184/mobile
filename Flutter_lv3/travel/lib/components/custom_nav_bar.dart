import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:travel/screens/chats/chats_screen.dart';
import 'package:travel/screens/events/events_screen.dart';

import '../screens/constants.dart';
import '../screens/friends/friends_screen.dart';
import '../screens/home/home_screen.dart';
import '../screens/size_config.dart';

class CustomNavBar extends StatelessWidget {
  const CustomNavBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(kDefaultPadding)),
        child: SafeArea(
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            NavItem(
              icon: 'assets/icons/calendar.svg',
              title: 'Events',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EventsScreen(),
                  ),
                );
              },
            ),
            NavItem(
              icon: 'assets/icons/chat.svg',
              title: 'Chat',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ChatsScreen(),
                  ),
                );
              },
            ),
            NavItem(
              icon: 'assets/icons/friendship.svg',
              title: 'Friends',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FriendshipScreen(),
                  ),
                );
              },
            ),
          ]),
        ),
      ),
    );
  }
}

class NavItem extends StatelessWidget {
  final String icon, title;
  final isActive;
  final GestureTapCallback press;
  const NavItem({
    Key? key,
    required this.icon,
    required this.title,
    this.isActive,
    required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Expanded(
        child: Container(
          padding: EdgeInsets.all(5),
          width: getProportionateScreenWidth(60),
          height: getProportionateScreenHeight(60),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [if (isActive == true) kDefaultShadow],
          ),
          child: Column(children: [
            SvgPicture.asset(
              this.icon,
              height: getProportionateScreenHeight(28),
              color: kTextColor,
            ),
            Text(
              this.title,
              style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold),
            )
          ]),
        ),
      ),
    );
  }
}
