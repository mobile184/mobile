import 'package:flutter/material.dart';
import 'package:travel/screens/size_config.dart';

import '../screens/constants.dart';

AppBar buildAppBar({bool isTransparent = false, required String title}) {
  return AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0.0,
    title: Text(
      title,
      style: TextStyle(
        color: kTextColor,
        fontWeight: FontWeight.bold,
      ),
    ),
    leading: IconButton(
      icon: Icon(
        Icons.menu,
        color: kIconColor,
      ),
      onPressed: () {},
    ),
    actions: [
      MaterialButton(
        onPressed: () {},
        child: ClipOval(
          child: Image.asset(
            'assets/images/profile.png',
            fit: BoxFit.cover,
            width: getProportionateScreenWidth(55),
            height: getProportionateScreenHeight(55),
          ),
        ),
      ),
    ],
  );
}
