import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:travel/components/app_bar.dart';
import 'package:travel/components/custom_nav_bar.dart';
import 'package:travel/screens/chats/chats/components/body.dart';
import 'package:travel/screens/constants.dart';

class ChatsScreen extends StatefulWidget {
  @override
  State<ChatsScreen> createState() => _ChatsScreenState();
}

class _ChatsScreenState extends State<ChatsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.white,
        child: Icon(
          Icons.chat_bubble_outline,
          color: Colors.grey,
        ),
      ),
      appBar: buildAppBar(title: 'Chats'),
      body: Body(),
      bottomNavigationBar: CustomNavBar(),
    );
  }
}
