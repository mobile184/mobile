import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:travel/models/ChatMessage.dart';
import 'package:travel/screens/chats/messages/components/message.dart';
import 'package:travel/screens/constants.dart';

import 'chat_input_field.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
          child: ListView.builder(
              itemCount: demoChatMessage.length,
              itemBuilder: (context, index) {
                return Message(message: demoChatMessage[index]);
              }),
        )),
        ChatInputField(),
      ],
    );
  }
}
