import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:travel/screens/constants.dart';
import 'package:travel/screens/size_config.dart';

import '../../components/app_bar.dart';
import '../../components/custom_nav_bar.dart';
import 'components/body.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBar(isTransparent: true, title: ""),
      body: Body(),
      bottomNavigationBar: CustomNavBar(),
    );
  }

  
}
