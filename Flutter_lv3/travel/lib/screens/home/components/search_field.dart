import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../size_config.dart';

class SearchField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: getProportionateScreenWidth(-25),
      child: Container(
        width: getProportionateScreenWidth(313),
        height: getProportionateScreenHeight(50),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: Colors.grey),
            boxShadow: [
              BoxShadow(
                offset: Offset(3, 3),
                blurRadius: 10,
                color: Colors.black.withOpacity(0.16),
                spreadRadius: -2,
              )
            ]),
        child: TextField(
          onChanged: (value) {},
          decoration: InputDecoration(
              hintText: 'Search your destination...',
              hintStyle: TextStyle(
                fontSize: getProportionateScreenHeight(12),
              ),
              suffixIcon: Icon(Icons.search),
              contentPadding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(kDefaultPadding),
                  vertical: getProportionateScreenHeight(kDefaultPadding / 2))),
        ),
      ),
    );
  }
}
