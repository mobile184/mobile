import 'package:flutter/material.dart';
import 'package:travel/components/section_title.dart';
import 'package:travel/models/TravelSpot.dart';
import 'package:travel/screens/size_config.dart';

import 'place_card.dart';
import 'travellers.dart';


class PopularPlaces extends StatelessWidget {
  const PopularPlaces({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SectionTitle(
        title: "Right Now At Spark",
        press: () {},
      ),
      VerticalSpacing(of: getProportionateScreenHeight(20)),
      SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        clipBehavior: Clip.none,
        child: Row(
          children: [
            ...List.generate(travelSpots.length, (index) {
              return Padding(
                padding: EdgeInsets.only(left: getProportionateScreenWidth(20)),
                child: PlaceCard(
                  travelSpot: travelSpots[index],
                  press: () {},
                ),
              );
            })
          ],
        ),
      ),
    ]);
  }
}
