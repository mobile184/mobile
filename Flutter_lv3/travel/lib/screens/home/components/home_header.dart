import 'package:flutter/material.dart';

import '../../size_config.dart';
import 'search_field.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      alignment: Alignment.center,
      children: [
        Image.asset(
          'assets/images/home_bg.png',
          height: getProportionateScreenHeight(315),
          width: double.infinity,
          fit: BoxFit.cover,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: getProportionateScreenHeight(80),
            ),
            Text(
              'Travelers',
              style: TextStyle(
                  fontSize: getProportionateScreenWidth(73),
                  color: Colors.white,
                  height: getProportionateScreenHeight(0.5)),
            ),
            Text(
              'Travel Comunity App',
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
        SearchField(),
      ],
    );
  }
}
