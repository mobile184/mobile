import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:travel/models/TravelSpot.dart';

import '../../constants.dart';
import '../../size_config.dart';

import 'travellers.dart';

class PlaceCard extends StatelessWidget {
  final TravelSpot travelSpot;
  final GestureTapCallback press;
  final bool isFullCard;
  const PlaceCard({
    Key? key,
    required this.travelSpot,
    required this.press,
    this.isFullCard = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getProportionateScreenWidth(isFullCard == true ? 158 : 137),
      child: Column(children: [
        AspectRatio(
          aspectRatio: isFullCard == true ? 1.29 : 1.06,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
              image: DecorationImage(
                image: AssetImage(travelSpot.image),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Container(
          width: getProportionateScreenWidth(isFullCard ? 158 : 137),
          padding: EdgeInsets.all(getProportionateScreenWidth(kDefaultPadding)),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
            boxShadow: [kDefaultShadow],
          ),
          child: Column(children: [
            Text(
              travelSpot.name,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: isFullCard
                      ? getProportionateScreenHeight(17)
                      : getProportionateScreenHeight(11),
                  fontWeight: FontWeight.w600),
            ),
            if (isFullCard == true)
              Text(
                travelSpot.date.day.toString(),
                style: Theme.of(context)
                    .textTheme
                    .headline4
                    ?.copyWith(fontWeight: FontWeight.bold),
              ),
            if (isFullCard == true)
              Text(
                DateFormat.MMMM().format(travelSpot.date) +
                    " " +
                    travelSpot.date.year.toString(),
              ),
            VerticalSpacing(
              of: getProportionateScreenHeight(20),
            ),
            Travellers(
              users: travelSpot.users,
            ),
          ]),
        ),
      ]),
    );
  }
}
