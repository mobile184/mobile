import 'package:flutter/material.dart';
import 'package:travel/models/TravelSpot.dart';
import 'package:travel/models/User.dart';

import '../../constants.dart';
import '../../size_config.dart';

class Travellers extends StatelessWidget {
  final List<User> users;
  const Travellers({Key? key, required this.users}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int totalUser = 0;
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenHeight(30),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          ...List.generate(users.length, (index) {
            totalUser++;
            return Positioned(
              left: getProportionateScreenWidth(22.0 * index),
              child: buildTravellerFaces(index),
            );
          }),
          Positioned(
            left: getProportionateScreenWidth(22.0 * totalUser),
            child: SizedBox(
              width: getProportionateScreenWidth(28),
              height: getProportionateScreenHeight(28),
              child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  padding: EdgeInsets.zero,
                  color: kPrimaryColor,
                  onPressed: () {},
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  )),
            ),
          )
        ],
      ),
    );
  }

  ClipOval buildTravellerFaces(int index) {
    return ClipOval(
      child: Image.asset(
        travelSpots[0].users[index].image,
        width: getProportionateScreenWidth(28),
        height: getProportionateScreenHeight(28),
        fit: BoxFit.cover,
      ),
    );
  }
}
