import 'package:flutter/material.dart';
import 'package:travel/components/section_title.dart';
import 'package:travel/models/User.dart';

import '../../constants.dart';
import '../../size_config.dart';

class TopTravellers extends StatelessWidget {
  const TopTravellers({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SectionTitle(title: "Top Travellers on Spark", press: () {}),
        Container(
          width: double.infinity,
          margin: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(kDefaultPadding),
          ),
          padding: EdgeInsets.all(
            getProportionateScreenWidth(24),
          ),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [kDefaultShadow],
              borderRadius: BorderRadius.circular(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ...List.generate(
                topTravelers.length,
                (index) => UserCard(
                  user: topTravelers[index],
                ),
              ),
            ],
          ),
        ),
        VerticalSpacing(
          of: getProportionateScreenHeight(10),
        ),
      ],
    );
  }
}

class UserCard extends StatelessWidget {
  final User user;
  const UserCard({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipOval(
          child: Image.asset(
            user.image,
            width: getProportionateScreenWidth(55),
            height: getProportionateScreenHeight(55),
            fit: BoxFit.cover,
          ),
        ),
        VerticalSpacing(
          of: getProportionateScreenHeight(10),
        ),
        Text(
          user.name,
          style: TextStyle(
              fontSize: getProportionateScreenHeight(11),
              fontWeight: FontWeight.w600),
        )
      ],
    );
  }
}
