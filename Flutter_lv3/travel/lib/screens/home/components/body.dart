import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:travel/models/TravelSpot.dart';
import 'package:travel/models/User.dart';
import 'package:travel/screens/constants.dart';
import 'package:travel/screens/size_config.dart';

import 'home_header.dart';
import 'popular_places.dart';
import 'search_field.dart';
import 'top_travellers.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          HomeHeader(),
          VerticalSpacing(
            of: getProportionateScreenHeight(20),
          ),
          PopularPlaces(),
          VerticalSpacing(
            of: getProportionateScreenHeight(20),
          ),
          VerticalSpacing(
            of: getProportionateScreenHeight(20),
          ),
          TopTravellers(),
        ],
      ),
    );
  }
}
