import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:travel/components/app_bar.dart';
import 'package:travel/components/custom_nav_bar.dart';

import 'components/body.dart';

class EventsScreen extends StatelessWidget {
  const EventsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(title: "New Events"),
      bottomNavigationBar: CustomNavBar(),
      body: Body(),
    );
  }
}
