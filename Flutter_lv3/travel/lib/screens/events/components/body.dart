import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:travel/models/TravelSpot.dart';
import 'package:travel/screens/constants.dart';
import 'package:travel/screens/home/components/place_card.dart';
import 'package:travel/screens/size_config.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(
            vertical: getProportionateScreenHeight(kDefaultPadding / 2),
            horizontal: getProportionateScreenWidth(kDefaultPadding)),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Padding(
            padding: EdgeInsets.only(bottom: getProportionateScreenHeight(25)),
            child: Wrap(
              alignment: WrapAlignment.spaceBetween,
              runSpacing: getProportionateScreenHeight(20),
              children: [
                ...List.generate(
                  travelSpots.length,
                  (index) => PlaceCard(
                      travelSpot: travelSpots[index],
                      press: () {},
                      isFullCard: true),
                ),
                Container(
                  width: getProportionateScreenWidth(158),
                  height: getProportionateScreenHeight(350),
                  decoration: BoxDecoration(
                    color: Color(0xFF6A6C93).withOpacity(0.09),
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                      width: 2,
                      color: Color(0xFFEBE8F6),
                    ),
                  ),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: getProportionateScreenHeight(53),
                          width: getProportionateScreenWidth(53),
                          child: MaterialButton(
                            color: kPrimaryColor,
                            padding: EdgeInsets.zero,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(60)),
                            onPressed: () {},
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                              size: getProportionateScreenWidth(35),
                            ),
                          ),
                        ),
                        VerticalSpacing(
                          of: 10,
                        ),
                        Text(
                          'Add New Place',
                          style: TextStyle(
                              fontSize: 11, fontWeight: FontWeight.bold),
                        )
                      ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
