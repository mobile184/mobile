import 'package:flutter/material.dart';

import 'package:travel/screens/constants.dart';
import 'package:travel/screens/friends/components/friend_list.dart';
import 'package:travel/screens/size_config.dart';

import 'search_friend.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Padding(
        padding: EdgeInsets.symmetric(
            vertical: getProportionateScreenHeight(kDefaultPadding / 2),
            horizontal: getProportionateScreenWidth(kDefaultPadding / 2)),
        child: Column(
          children: [
            SearchFriend(),
            VerticalSpacing(
              of: getProportionateScreenHeight(kDefaultPadding),
            ),
            FriendList(),
          ],
        ),
      ),
    );
  }
}
