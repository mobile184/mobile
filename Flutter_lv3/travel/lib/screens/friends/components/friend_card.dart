import 'package:flutter/material.dart';

import 'package:travel/models/Friend.dart';
import 'package:travel/screens/constants.dart';

import '../../size_config.dart';

class FriendCard extends StatelessWidget {
  final Friend friend;
  const FriendCard({Key? key, required this.friend}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Stack(
        children: [
          ClipOval(
            child: Image.asset(
              friend.data.image,
              width: getProportionateScreenWidth(55),
              height: getProportionateScreenHeight(55),
              fit: BoxFit.cover,
            ),
          ),
          if (friend.isActive == true)
            Positioned(
                bottom: 0,
                right: 0,
                child: Container(
                  width: 16,
                  height: 16,
                  decoration: BoxDecoration(
                      border: Border.all(width: 3, color: Colors.white),
                      color: kPrimaryColor,
                      shape: BoxShape.circle),
                ))
        ],
      ),
      title: Text(
        friend.data.name,
        style: TextStyle(fontWeight: FontWeight.w600),
      ),
      subtitle: Text(
        '${friend.mutual_friends_num} mutual friends',
        style: TextStyle(
          color: kTextColor.withOpacity(0.64),
        ),
      ),
      trailing: Icon(Icons.more_vert),
    );
  }
}
