import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../size_config.dart';

class SearchFriend extends StatelessWidget {
  const SearchFriend({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(width: 1)),
      height: getProportionateScreenHeight(60),
      child: Padding(
        padding: EdgeInsets.symmetric(
            vertical: getProportionateScreenHeight(kDefaultPadding / 2)),
        child: TextField(
          decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(kDefaultPadding),
                vertical: getProportionateScreenHeight(kDefaultPadding / 2),
              ),
              hintText: 'Search your friends..',
              suffixIcon: Icon(Icons.search),
              border: InputBorder.none,
              hintStyle: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              )),
        ),
      ),
    );
  }
}
