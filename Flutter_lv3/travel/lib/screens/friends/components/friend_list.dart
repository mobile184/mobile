import 'package:flutter/material.dart';

import 'package:travel/models/Friend.dart';

import '../../size_config.dart';
import 'friend_card.dart';

class FriendList extends StatefulWidget {
  const FriendList({Key? key}) : super(key: key);

  @override
  State<FriendList> createState() => _FriendListState();
}

class _FriendListState extends State<FriendList> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${friends.length} friends',
          style: TextStyle(
              fontSize: getProportionateScreenWidth(22),
              fontWeight: FontWeight.bold),
        ),
        ...List.generate(
          friends.length,
          (index) => FriendCard(friend: friends[index]),
        ),
      ],
    );
  }
}
