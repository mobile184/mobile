import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:travel/components/app_bar.dart';
import 'package:travel/components/custom_nav_bar.dart';
import 'package:travel/screens/constants.dart';

import 'components/body.dart';

class FriendshipScreen extends StatelessWidget {
  const FriendshipScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(title: 'Friends', isTransparent: true),
      body: Body(),
      bottomNavigationBar: CustomNavBar(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.person_add),
        backgroundColor: kIconColor,
      ),
    );
  }
}
