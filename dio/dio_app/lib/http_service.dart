import 'package:dio/dio.dart';

class HttpService {
  late Dio _dio;

  final baseUrl = "http://reqres.in";

  HttpService() {
    _dio = Dio(BaseOptions(
      baseUrl: baseUrl,
    ));
    initializeInterceptors();
  }

  Future<Response> getRequest(String endPoint) async {
    Response response;

    try {
      response = await _dio.get(endPoint);
    } on DioError catch (e) {
      print(e.message);
      throw Exception(e.message);
    }

    return response;
  }

  initializeInterceptors() {
    _dio.interceptors
        .add(InterceptorsWrapper(onError: (error, errorInterceptorHandler) async {
      print(error.message);
      return errorInterceptorHandler.next(error);
      
    }, onRequest: (request, requestInterceptorHandler) {
      print("${request.method} ${request.path}");
      return requestInterceptorHandler.next(request);
    }, onResponse: (response, responseInterceptorHandler) {
      print(response.data);
      return responseInterceptorHandler.next(response);
    }));
  }
}
