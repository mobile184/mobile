import 'package:chec_in_app/controllers/shared_preference.dart';
import 'package:chec_in_app/route_managements/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;
import '../components/show_dialog.dart';
import '../models/user_response.dart';
import '../service/service.dart';

class LogInController extends GetxController {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  RxString usernameWarning = "Invalid username".obs;
  RxString passwordWarning = "Invalid Password".obs;
  RxBool isCorrectUser = false.obs;
  late HttpService http = HttpService();
  var data;

  /// This function is used to check if [emailController.text] is
  /// valid form or not
  void isValidUsername() {
    usernameController.text != ""
        ? usernameWarning.value = ""
        : usernameWarning.value = "Invalid Username";
  }

  /// This function is used to check if [passwordController.text] is
  /// valid form or not
  void isValidPassword() {
    passwordController.text == ""
        ? passwordWarning.value = "Invalid Password"
        : passwordWarning.value = "";
  }

  /// This function is used to post [logIn] request to server
  Future logIn() async {
    dio.Response response;
    try {
      response = await http.postLogInRequest(
          "api/Login/UserLogin", usernameController.text, passwordController.text);
      if (response.statusCode == 200) {
        data = response.data;
        isCorrectUser.value = true;
        return response.data;
      } else {
        print("There is some problem status code ${response.statusCode}");
        isCorrectUser.value = false;
        return response.data;
      }
    } on Exception catch (e) {
      print(e);
      isCorrectUser.value = false;
    }
    
  }

  /// This function is used activate [logIn] function when user
  /// click the Login button
  /// 
  /// @param BuildContext context
  /// return void
  void onLoginButtonClick(context) async {
    showLoaderDialog(context);
    await logIn().then((dynamic value) {
      Get.back();
      if (value == null) {
        Get.closeCurrentSnackbar();
        Get.snackbar("Warning", "Fail To Connect To Server!", backgroundColor: Colors.white60);
      } else if (isCorrectUser.value == true) {
        Login login = Login.fromJson(value);
        User? user = login.user;
        
        if (login.token == "0") {
          Get.closeCurrentSnackbar();
          Get.snackbar("Warning", "Username Not Exists!", backgroundColor: Colors.white60);
        } else if (login.token == "1") {
          Get.closeCurrentSnackbar();
          Get.snackbar("Warning", "Wrong Password!", backgroundColor: Colors.white60);
        } else{
          ///Save data to local storage to automatically login 
        ///next time
        MySharedPreference.setUserId(idUser: user!.id);
        MySharedPreference.setToken(token: login.token);
        Get.offNamed(kHomePage,
            arguments: {"idUser": user.id, "token": login.token});
        }
      } else {
        Get.closeCurrentSnackbar();
        Get.snackbar("Warning", "Wrong Username or Password!", backgroundColor: Colors.white60);
      }
    });
  }
}
