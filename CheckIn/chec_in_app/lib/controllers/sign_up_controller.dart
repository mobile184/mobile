import 'dart:async';

import 'package:chec_in_app/models/sign_up_response.dart';
import 'package:chec_in_app/route_managements/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;
import '../components/show_dialog.dart';
import '../service/service.dart';

class SignUpController extends GetxController {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmController = TextEditingController();
  TextEditingController fullnameController = TextEditingController();
  RxString fullnameWarning = "Invalid Username".obs;
  RxString passwordWarning = "Invalid Password".obs;
  RxString usernameWarning = "Invalid Username".obs;
  RxString passwordConfirmWarning = "Password Mismatch".obs;
  bool isSuccessful = false;
  late HttpService http = HttpService();

  /// Check if [usernameController.text] is valid or not
  void isValidFullname() {
    fullnameController.text == ""
        ? fullnameWarning.value = "Invalid Fullname"
        : fullnameWarning.value = "";
  }

  /// Check if [passwordController.text] is valid or not
  void isValidPassword() {
    passwordController.text == ""
        ? passwordWarning.value = "Invalid Password"
        : passwordWarning.value = "";
  }

  /// Check if [usernameController.text] is valid or not
  void isValidUsername() {
    usernameController.text == ""
        ? usernameWarning.value = "Invalid Username"
        : usernameWarning.value = "";
  }

  /// Check if [passwordConfirmController.text] is valid or not
  void isValidPasswordConfirm() {
    passwordConfirmController.text != passwordController.text
        ? passwordConfirmWarning.value = "Password Mismatch"
        : passwordConfirmWarning.value = "";
  }

  /// Function is used to post [signUp] request to server
  Future signUp(BuildContext  context) async {
    dio.Response response;
    try {

      response = await http.postSignUpRequest(
          endPoint: "api/Login/Register",
          fullname: fullnameController.text,
          password: passwordController.text,
          userName: usernameController.text); 
          
      if (response.statusCode == 200) {
        SignUpResponse signup = SignUpResponse.fromJson(response.data);
        if (signup.userName != "" && signup.id == 0) {
          Get.closeCurrentSnackbar();
          Get.snackbar("Warning", "Acount Exitsted!", backgroundColor: Colors.white60);
            isSuccessful == false;
        } else if (signup.userName == "" && signup.id == 0) {
          Get.closeCurrentSnackbar();
          Get.snackbar("Warning", "Register Failed!", backgroundColor: Colors.white60);
            isSuccessful == false;
        } else {
        isSuccessful = true;
        update();
        }
      } else {
        isSuccessful = false;
        print("There is some problem status code not 200");
      }
    } on Exception catch (e) {
      isSuccessful = false;
      print(e);
    }
  }

  /// Function is used when user click [Sign Up] button in
  /// SignUpPage
  void onSignUpButtonClick(context) async {
    showLoaderDialog(context);
    var data = await signUp(context).then((value) {
      Get.back();
      if (isSuccessful == true) {
        Get.closeCurrentSnackbar();
        Get.snackbar("Success", "Sign Up Successful!",
        backgroundColor: Colors.white60);
        Get.offNamed(kLogInPage);
      }
    });
  }
}
