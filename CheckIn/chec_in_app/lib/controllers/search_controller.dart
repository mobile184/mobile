import 'dart:async';

import 'package:chec_in_app/components/show_dialog.dart';
import 'package:chec_in_app/models/search_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../service/service.dart';
import 'package:dio/dio.dart' as dio;

class SearchController extends GetxController {
  HttpService http = HttpService();
  String token = "";
  DateTime beginTime = DateTime.now().subtract(Duration(days: 30));
  DateTime endTime = DateTime.now();
  RxString beginTimeText = "Begin Time".obs;
  RxString endTimeText = "End Time".obs;
  var searchResponse;
  var listDateResponse;

  /// This function is used to load data of the [SearchPage] 
  /// before load screen
  ///
  /// @param int idUser
  /// @param DateTime fromdate
  /// @param DateTime toDate
  /// @param BuildContext context
  /// @param String token
  /// return Future
  Future loadSearchData(
      {required int idUser,
      required fromDate,
      required toDate,
      required context,
      required String token}) async {
    dio.Response response;
    try {
      response = await http.getSearchResponseRequest(
          "api/Search/Search?idUser=$idUser&fromDate=$fromDate&toDate=$toDate",
          token: token);
      if (response.statusCode == 200) {
        searchResponse = SearchResponse.fromJson(response.data);
        listDateResponse = searchResponse.listDateResponse;
      } else {
        print("There is some problem status code not 200");
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  /// Choose time begin of interval to search data
  /// 
  /// @param context
  /// return void
  void chooseBeginTime(BuildContext context) async {
    await DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime.now().subtract(Duration(days: 90)),
        maxTime: DateTime.now(),
        currentTime: DateTime.now(),
        onChanged: (date) {}, onConfirm: (date) {
      beginTime = DateTime(date.year, date.month, date.day);
      beginTimeText.value = DateFormat.yMd().format(beginTime);
      update();
    }, locale: LocaleType.en);
  }

  /// Choose time begin of interval to search data
  /// 
  /// @param context
  /// return void
  void chooseEndTime(BuildContext context) async {
    await DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: beginTime,
        maxTime: DateTime.now(),
        currentTime: DateTime.now(),
        onChanged: (date) {}, onConfirm: (date) {
      endTime = DateTime(date.year, date.month, date.day);
      endTimeText.value = DateFormat.yMd().format(endTime);
      update();
    }, locale: LocaleType.en);
  }

  /// Search data for [SearchPage] screen depends on chosen
  /// [beginTime] and [endTime]
  /// 
  /// @param context
  /// @param int idUser
  /// @param String token
  /// return void
  Future onSearchButtonClick(BuildContext context,
      {required int idUser, required String token}) async {
    await loadSearchData(
        idUser: idUser,
        fromDate: beginTime,
        toDate: endTime,
        context: context,
        token: token);
  }
}
