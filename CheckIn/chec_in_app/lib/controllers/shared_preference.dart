import 'package:shared_preferences/shared_preferences.dart';

class MySharedPreference {

  /// Get [idUser] saved in local storage
  /// 
  /// return int
  static Future getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    int? value = prefs.getInt("idUser");
    return value;
  }

  /// Get [token] saved in local storage
  /// 
  /// return String
  static Future getToken() async {
    final prefs = await SharedPreferences.getInstance();
    String? value = prefs.getString("token");
    return value;
  }

  /// Set [idUser] value to local storage in case user first log in
  static setUserId({required int idUser}) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt("idUser", idUser);
  }

  /// Set [token] value to local storage in case user first log in
  /// or token is reset
  static setToken({required String token}) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("token", token);
  }
}
