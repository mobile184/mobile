import 'dart:math';

import 'package:chec_in_app/models/check_in_response.dart';
import 'package:chec_in_app/models/home_response.dart';
import 'package:chec_in_app/route_managements/routes.dart';
import 'package:chec_in_app/service/service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;
import 'dart:io' as io;
import 'package:image_downloader/image_downloader.dart';

import '../components/show_dialog.dart';

class HomeController extends GetxController {
  int curSession = -1;
  String avatar =
      "https://thumbs.dreamstime.com/b/world-map-technological-background-best-internet-concept-global-business-elements-image-furnished-bright-lines-66529240.jpg";
  RxBool isLoading = false.obs;
  HttpService http = HttpService();
  String token = "";
  HomeResponse? homeResponse;

  /// Latitude and Longtitude of company on map
  final double LAT = 21.035365;
  final double LNG = 105.767357;

  /// Min distance from company to use [checkIn]/[checkOut] method
  final double MIN_DISTANCE = 50 / 1000;
  Position? position;

  /// This function is used to load data of the [HomePage]
  /// before load screen
  ///
  /// @param int idUser
  /// @param BuildContext context
  /// @param String token
  /// return Future
  Future loadHomeData(
      {required int idUser,
      required BuildContext context,
      required String token}) async {
    dio.Response response;
    try {
      response = await http.getHomeResponseRequest(
          context: context, "api/Home/Home?idUser=${idUser}", token: token);
      if (response.statusCode == 200) {
        homeResponse = HomeResponse.fromJson(response.data);
        update();
      } else {
        print("There is some problem status code ${response.statusCode}");
      }
    } on Exception catch (e) {
      print(e);
    }
    update();
  }

  /// This function is used to calculate the distance
  /// between 2 location on map (unit Km)
  ///
  /// @param double lat1
  /// @param double lon1
  /// @param double lat2
  /// @param double lon2
  /// return double
  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  ///This function is used to post checkIn request to server
  ///in case user click on [Check In] button
  ///
  ///@param BuildContext context
  ///@param int idUser
  ///@param String token
  ///return Future
  Future checkIn(BuildContext context,
      {required int idUser, required String token}) async {
    if (kIsWeb == false) {
      LocationPermission permission;

      permission = await Geolocator.checkPermission();
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.deniedForever)
          permission = await Geolocator.requestPermission();
      }
      if (calculateDistance(position.latitude, position.longitude, LAT, LNG) <=
          MIN_DISTANCE) {
        dio.Response response;
        try {
          response = await http.postCheckInRequest("api/Home/Checkin",
              idUser: idUser, dateTime: DateTime.now(), token: token);
          await loadHomeData(idUser: idUser, context: context, token: token);
          update();
          if (response.statusCode == 200) {
            curSession = CheckInResponse.fromJson(response.data).id;
            update();
            Get.closeCurrentSnackbar();
            Get.snackbar("Success", "Check In Successfully!",
                backgroundColor: Colors.white60);
          } else {
            print("There is some problem status code not 200");
            Get.snackbar("Warning", "Please check your connection!",
                backgroundColor: Colors.white60);
          }
        } on Exception catch (e) {
          print(e);
        }
        update();
      } else {
        Get.closeCurrentSnackbar();
        Get.snackbar("Warning", "Please Get Close To Company!",
            backgroundColor: Colors.white60);
      }
    } else {
      dio.Response response;
      try {
        response = await http.postCheckInRequest("api/Home/Checkin",
            idUser: idUser, dateTime: DateTime.now(), token: token);
        await loadHomeData(idUser: idUser, context: context, token: token);
        update();
        if (response.statusCode == 200) {
          curSession = CheckInResponse.fromJson(response.data).id;
          update();
          Get.closeCurrentSnackbar();
          Get.snackbar("Success", "Check In Successfully!",
              backgroundColor: Colors.white60);
        } else {
          print("There is some problem status code not 200");
          Get.snackbar("Warning", "Please check your connection!",
              backgroundColor: Colors.white60);
        }
      } on Exception catch (e) {
        print(e);
        Get.snackbar("Warning", "Please check your connection!",
            backgroundColor: Colors.white60);
      }
      update();
    }
  }

  ///This function is used to put checkIn request to server
  ///in case user click on [Check Out] button
  ///
  ///@param BuildContext context
  ///@param int idUser
  ///@param String token
  ///return Future
  Future checkOut(BuildContext context,
      {required int idUser, required String token}) async {
    dio.Response response;
    if (kIsWeb == false) {
      LocationPermission permission;

      permission = await Geolocator.checkPermission();
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) Get.closeCurrentSnackbar();
        Get.snackbar("Warning", "Location Permission Not Granted",
            backgroundColor: Colors.white60);
      } else if (calculateDistance(
              position.latitude, position.longitude, LAT, LNG) <=
          MIN_DISTANCE) {
        try {
          response = await http.putCheckOutRequest(
              "api/Home/Checkout?id=${homeResponse!.idLatestSession}",
              idUser: idUser,
              dateTime: DateTime.now(),
              token: token);
          if (response.statusCode == 200 || response.statusCode == 204) {
            await loadHomeData(idUser: idUser, context: context, token: token);
            Get.closeCurrentSnackbar();
            Get.snackbar("Success", "Check Out Successfully!",
                backgroundColor: Colors.white60);
            update();
          } else {
            print("There is some problem status code ${response.statusCode}");
          }
        } on Exception catch (e) {
          print(e);
        }
        update();
      } else {
        Get.closeCurrentSnackbar();
        Get.snackbar("Warning", "Please Get Closer To Company!",
            backgroundColor: Colors.white60);
      }
    } else {
      try {
        response = await http.putCheckOutRequest(
            "api/Home/Checkout?id=${homeResponse!.idLatestSession}",
            idUser: idUser,
            dateTime: DateTime.now(),
            token: token);
        if (response.statusCode == 200 || response.statusCode == 204) {
          await loadHomeData(idUser: idUser, context: context, token: token);
          Get.closeCurrentSnackbar();
          Get.snackbar("Success", "Check Out Successfully!",
              backgroundColor: Colors.white60);
          update();
        } else {
          print("There is some problem status code ${response.statusCode}");
        }
      } on Exception catch (e) {
        print(e);
      }
      update();
    }
  }
}
