import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'constant.dart';

Widget buildAppBar(
    {bool canBack = false,
    required String avatar,
    required String title,
    required String subTitle,
    bool isOnTime = true,
    bool isCheckedToday = true}) {
  return Container(
    padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(color: Colors.white),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
        SizedBox(width: 16,),
        Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(shape: BoxShape.circle),
          child: Stack(
            children: [
              ClipOval(
                child: Image.asset(
                  "assets/icons/avatar.png",
                  fit: BoxFit.cover,
                  width: 50,
                  height: 50,
                ),
              ),
              if (canBack == false)
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: Container(
                    width: 16,
                    height: 16,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: isCheckedToday == true
                          ? kIconPrimaryColor
                          : isOnTime
                              ? Colors.amberAccent
                              : Colors.grey.shade300,
                    ),
                  ),
                )
            ],
          ),
        ),
        SizedBox(width: 16,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(
                  fontSize: 16, fontWeight: FontWeight.bold, color: kTextColor),
            ),
            Text(
              "Code: $subTitle",
              style: TextStyle(fontSize: 14, color: kIconSubText),
            ),
          ],
        ),
        Spacer(),
        PopupMenuButton(
            itemBuilder: (context) => [
                  PopupMenuItem(
                    onTap: () {
                      Future.delayed(
                        Duration.zero,
                        () => showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text("About App"),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Icon(
                                      Icons.info_outline,
                                      color: Colors.red,
                                      size: 20,
                                    ),
                                  ],
                                ),
                                content: Text("This is an app from Ha and Duc"),
                              );
                            }),
                      );
                    },
                    value: 1,
                    child: Text("This is an app from Ha and Duc"),
                  ),
                ],
            child: Icon(Icons.more_horiz)),
            SizedBox(width: 16,)
      ]));
}
