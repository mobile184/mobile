import 'package:chec_in_app/components/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class CheckInCard extends StatelessWidget {
  final String time;
  final String time_start;
  final String time_end;
  final bool isCheck;
  final Color color;
  final countTime;
  const CheckInCard(
      {Key? key,
      this.time = "Today",
      this.isCheck = false,
      required this.color,
      this.time_start = "",
      this.time_end = "",
      required this.countTime})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Text(
          time,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          width: 16,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  "Time In: ",
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
                Text(
                  time_start,
                  style:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  "Time Out: ",
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
                Text(
                  time_end,
                  style:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  "Time Worked: ",
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
                Text(
                  "${countTime["hours"]}h : ${countTime["minutes"].remainder(60)}m",
                  style:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
        Spacer(),
        isCheck == true
            ? SvgPicture.asset("assets/icons/checked.svg", width: 30,)
            : SvgPicture.asset("assets/icons/cancel.svg", width: 30,)
      ]),
    );
  }
}
