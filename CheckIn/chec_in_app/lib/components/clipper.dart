import 'package:flutter/material.dart';

class ArrowBoxClipper extends CustomClipper<Path> {
  final double distance;
  final double radius;
  ArrowBoxClipper({required this.distance, required this.radius});

  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.addRect(
        Rect.fromLTWH(radius, 0, size.width - distance - radius, size.height));
    path.addRRect(RRect.fromRectAndRadius(
        Rect.fromLTWH(0, 0, size.width - distance, size.height),
        Radius.circular(radius)));
    path.moveTo(size.width - distance, 0);
    path.lineTo(size.width, size.height / 2);
    path.lineTo(size.width - distance, size.height);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

class ParalellogramClipper extends CustomClipper<Path> {
  final double distance;
  ParalellogramClipper({required this.distance});

  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.addRect(
        Rect.fromLTWH(distance, 0, size.width - distance * 2, size.height));
    path.moveTo(distance, 0);
    path.lineTo(0, size.height);
    path.lineTo(distance, size.height);
    path.moveTo(size.width, 0);
    path.lineTo(size.width - distance, size.height);
    path.lineTo(size.width - distance, 0);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
