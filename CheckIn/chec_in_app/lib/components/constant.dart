import 'package:flutter/material.dart';

const Color kSecondaryColor = Color.fromRGBO(32, 30, 30, 1);
const Color kIconSubText = Colors.black54;
const Color kCardColor = Colors.black12;
const kIconPrimaryColor = Color(0xFF66BB6A);
const kErrorColor = Color.fromARGB(0, 248, 70, 70);
const kPrimaryColor = Color(0xFF3E4067);
const kTextColor = Color(0xFF3F4168);
const kIconColor = Color(0xFF5E5E5E);
const kDefaultPadding = 20.0;
final kDefaultShadow = BoxShadow(
  offset: Offset(5, 5),
  blurRadius: 10,
  color: Color(0xFFE9E9E9).withOpacity(0.56),
);
