const String kWelcomPage = "/";
const String kHomePage = "/home";
const String kSignUpPage = "/sign_up";
const String kLogInPage = "/log_in";
const String kSearchPage = "/search";
