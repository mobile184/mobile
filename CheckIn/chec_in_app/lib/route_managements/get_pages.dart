import 'package:chec_in_app/route_managements/bindings.dart';

import '../pages/home_page/home_page.dart';
import '../pages/log_in_page/login_page.dart';
import '../pages/search_page/search_page.dart';
import '../pages/sign_up_page/sign_up_page.dart';
import '../pages/welcome_page/welcome_page.dart';
import 'routes.dart';
import 'package:get/get.dart';

class GetAllPage {
  static List<GetPage> getAllPages() {
    return [
      GetPage(name: kWelcomPage, page: () => const WelcomPage()),
      GetPage(
          name: kLogInPage,
          page: () => const LogInPage(),
          binding: LogInBinding()),
      GetPage(
          name: kSignUpPage,
          page: () => const SignUpPage(),
          binding: SignUpBinding()),
      GetPage(
          name: kHomePage,
          page: () => const HomePage(),
          binding: HomePageBinding(),
          transition: Transition.cupertino),
      GetPage(
          name: kSearchPage,
          page: () => const SearchPage(),
          binding: SearchPageBinding(),
          transition: Transition.cupertino),
    ];
  }
}
