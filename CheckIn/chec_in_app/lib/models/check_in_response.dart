// To parse this JSON data, do
//
//     final checkInResponse = checkInResponseFromJson(jsonString);

import 'dart:convert';

CheckInResponse checkInResponseFromJson(String str) =>
    CheckInResponse.fromJson(json.decode(str));

String checkInResponseToJson(CheckInResponse data) =>
    json.encode(data.toJson());

class CheckInResponse {
  CheckInResponse({
    required this.timeIn,
    required this.timeOut,
    required this.countTime,
    required this.idUser,
    required this.code,
    required this.id,
  });

  DateTime timeIn;
  dynamic timeOut;
  dynamic countTime;
  int idUser;
  String code;
  int id;

  factory CheckInResponse.fromJson(Map<String, dynamic> json) =>
      CheckInResponse(
        timeIn: DateTime.parse(json["timeIn"]),
        timeOut: json["timeOut"],
        countTime: json["countTime"],
        idUser: json["idUser"],
        code: json["code"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "timeIn": timeIn.toIso8601String(),
        "timeOut": timeOut,
        "countTime": countTime,
        "idUser": idUser,
        "code": code,
        "id": id,
      };
}
