// To parse this JSON data, do
//
//     final signUpResponse = signUpResponseFromJson(jsonString);

import 'dart:convert';

SignUpResponse signUpResponseFromJson(String str) => SignUpResponse.fromJson(json.decode(str));

String signUpResponseToJson(SignUpResponse data) => json.encode(data.toJson());

class SignUpResponse {
    SignUpResponse({
        this.fullname,
        this.avatar,
        required this.userName,
        this.password,
        this.role,
        this.code,
        required this.id,
    });

    dynamic fullname;
    dynamic avatar;
    dynamic userName;
    dynamic password;
    dynamic role;
    dynamic code;
    int id;

    factory SignUpResponse.fromJson(Map<String, dynamic> json) => SignUpResponse(
        fullname: json["fullname"],
        avatar: json["avatar"],
        userName: json["userName"],
        password: json["password"],
        role: json["role"],
        code: json["code"],
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "fullname": fullname,
        "avatar": avatar,
        "userName": userName,
        "password": password,
        "role": role,
        "code": code,
        "id": id,
    };
}
