// To parse this JSON data, do
//
//     final searchResponse = searchResponseFromJson(jsonString);

import 'dart:convert';

SearchResponse searchResponseFromJson(String str) =>
    SearchResponse.fromJson(json.decode(str));

String searchResponseToJson(SearchResponse data) => json.encode(data.toJson());

class SearchResponse {
  SearchResponse({
    required this.user,
    required this.workedDays,
    required this.absentDays,
    required this.workedTimes,
    required this.listDateResponse,
  });

  User user;
  int workedDays;
  int absentDays;
  Map<String, dynamic> workedTimes;
  List<ListDateResponse> listDateResponse;

  factory SearchResponse.fromJson(Map<String, dynamic> json) => SearchResponse(
        user: User.fromJson(json["user"]),
        workedDays: json["workedDays"],
        absentDays: json["absentDays"],
        workedTimes: Map.from(json["workedTimes"])
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        listDateResponse: List<ListDateResponse>.from(
            json["listDateResponse"].map((x) => ListDateResponse.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "user": user.toJson(),
        "workedDays": workedDays,
        "absentDays": absentDays,
        "workedTimes": Map.from(workedTimes)
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        "listDateResponse":
            List<dynamic>.from(listDateResponse.map((x) => x.toJson())),
      };
}

class ListDateResponse {
  ListDateResponse({
    required this.userDto,
    required this.isCheckedNow,
    required this.isChecked,
    required this.checkInTime,
    required this.checkOutTime,
    required this.countTime,
    required this.dateString,
  });

  User userDto;
  bool isCheckedNow;
  bool isChecked;
  DateTime checkInTime;
  DateTime? checkOutTime;
  Map<String, dynamic> countTime;
  String dateString;

  factory ListDateResponse.fromJson(Map<String, dynamic> json) =>
      ListDateResponse(
        userDto: User.fromJson(json["userDto"]),
        isCheckedNow: json["isCheckedNow"],
        isChecked: json["isChecked"],
        checkInTime: DateTime.parse(json["checkInTime"]),
        checkOutTime: json["checkOutTime"] == null
            ? null
            : DateTime.parse(json["checkOutTime"]),
        countTime: Map.from(json["countTime"])
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        dateString: json["dateString"],
      );

  Map<String, dynamic> toJson() => {
        "userDto": userDto.toJson(),
        "isCheckedNow": isCheckedNow,
        "isChecked": isChecked,
        "checkInTime": checkInTime.toIso8601String(),
        "checkOutTime": checkOutTime,
        "countTime":
            Map.from(countTime).map((k, v) => MapEntry<String, dynamic>(k, v)),
        "dateString": dateString,
      };
}

class User {
  User({
    required this.id,
    required this.fullname,
    required this.avatar,
    required this.userName,
    required this.role,
    required this.code,
  });

  int id;
  String fullname;
  String avatar;
  String userName;
  String role;
  String code;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        fullname: json["fullname"],
        avatar: json["avatar"],
        userName: json["userName"],
        role: json["role"],
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "fullname": fullname,
        "avatar": avatar,
        "userName": userName,
        "role": role,
        "code": code,
      };
}
