// To parse this JSON data, do
//
//     final login = loginFromJson(jsonString);

import 'dart:convert';

Login loginFromJson(String str) => Login.fromJson(json.decode(str));

String loginToJson(Login data) => json.encode(data.toJson());

class Login {
  Login({
    required this.token,
    required this.user,
  });

  String token;
  User? user;

  factory Login.fromJson(Map<String, dynamic> json) => Login(
        token: json["token"],
        user: json["user"] == null ? null:User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "token": token,
        "user": user?.toJson(),
      };
}

class User {
  User({
    required this.id,
    required this.fullname,
    required this.avatar,
    required this.userName,
    required this.role,
    required this.code,
  });

  int id;
  String fullname;
  String avatar;
  String userName;
  String role;
  String code;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        fullname: json["fullname"],
        avatar: json["avatar"],
        userName: json["userName"],
        role: json["role"],
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "fullname": fullname,
        "avatar": avatar,
        "userName": userName,
        "role": role,
        "code": code,
      };
}
