// To parse this JSON data, do
//
//     final homeResponse = homeResponseFromJson(jsonString);

import 'dart:convert';

HomeResponse homeResponseFromJson(String str) =>
    HomeResponse.fromJson(json.decode(str));

String homeResponseToJson(HomeResponse data) => json.encode(data.toJson());

class HomeResponse {
  HomeResponse({
    required this.userDto,
    required this.idLatestSession,
    required this.isCheckedToday,
    required this.isCheckedNow,
    required this.isOnTime,
    required this.checkInTime,
    required this.checkOutTime,
    required this.listSessions,
  });

  UserDto userDto;
  int idLatestSession;
  bool isCheckedToday;
  bool isCheckedNow;
  bool isOnTime;
  DateTime checkInTime;
  DateTime? checkOutTime;
  List<ListSession> listSessions;

  factory HomeResponse.fromJson(Map<String, dynamic> json) => HomeResponse(
        userDto: UserDto.fromJson(json["userDto"]),
        idLatestSession: json["idLatestSession"],
        isCheckedToday: json["isCheckedToday"],
        isCheckedNow: json["isCheckedNow"],
        isOnTime: json["isOnTime"],
        checkInTime: DateTime.parse(json["checkInTime"]),
        checkOutTime: json["checkOutTime"] == null
            ? null
            : DateTime.parse(json["checkOutTime"]),
        listSessions: List<ListSession>.from(
            json["listSessions"].map((x) => ListSession.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "userDto": userDto.toJson(),
        "idLatestSession": idLatestSession,
        "isCheckedToday": isCheckedToday,
        "isCheckedNow": isCheckedNow,
        "isOnTime": isOnTime,
        "checkInTime": checkInTime.toIso8601String(),
        "checkOutTime": checkOutTime?.toIso8601String(),
        "listSessions": List<dynamic>.from(listSessions.map((x) => x.toJson())),
      };
}

class ListSession {
  ListSession({
    required this.timeIn,
    required this.timeOut,
    required this.countTime,
    required this.idUser,
    required this.code,
    required this.id,
  });

  DateTime timeIn;
  DateTime? timeOut;
  Map<String, double>? countTime;
  int idUser;
  String code;
  int id;

  factory ListSession.fromJson(Map<String, dynamic> json) => ListSession(
        timeIn: DateTime.parse(json["timeIn"]),
        timeOut:
            json["timeOut"] == null ? null : DateTime.parse(json["timeOut"]),
        countTime: json["countTime"] == null
            ? null
            : Map.from(json["countTime"])
                .map((k, v) => MapEntry<String, double>(k, v.toDouble())),
        idUser: json["idUser"],
        code: json["code"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "timeIn": timeIn.toIso8601String(),
        "timeOut": timeOut?.toIso8601String(),
        "countTime": Map.from(countTime as Map)
            .map((k, v) => MapEntry<String, dynamic>(k, v)),
        "idUser": idUser,
        "code": code,
        "id": id,
      };
}

class UserDto {
  UserDto({
    required this.id,
    required this.fullname,
    required this.avatar,
    required this.userName,
    required this.role,
    required this.code,
  });

  int id;
  String fullname;
  String avatar;
  String userName;
  String role;
  String code;

  factory UserDto.fromJson(Map<String, dynamic> json) => UserDto(
        id: json["id"],
        fullname: json["fullname"],
        avatar: json["avatar"],
        userName: json["userName"],
        role: json["role"],
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "fullname": fullname,
        "avatar": avatar,
        "userName": userName,
        "role": role,
        "code": code,
      };
}
