import 'package:chec_in_app/components/constant.dart';
import 'package:chec_in_app/controllers/log_in_controller.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;
import '../../../route_managements/routes.dart';
import '../../../service/service.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> with WidgetsBindingObserver {
  LogInController controller = Get.find();
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            FocusManager.instance.primaryFocus?.unfocus();
          }
        },
        onVerticalDragDown: (drag) {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            FocusManager.instance.primaryFocus?.unfocus();
          }
        },
        child: Container(
          width: Get.width,
          height: Get.height,
          decoration: const BoxDecoration(
            color: kSecondaryColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: Get.height * 0.1,
              ),
              Image.asset(
                "assets/icons/logo.png",
                width: Get.height * 0.3,
              ),
              SizedBox(height: Get.height * 0.08),
              _buildInput(controller: controller),
              _buildLoginButton(),
              _buildSignUpSuggestion(),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSignUpSuggestion() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RichText(
            text: TextSpan(children: [
          const TextSpan(
            text: "Don't have an account? ",
            style: TextStyle(color: Colors.white70),
          ),
          TextSpan(
              text: "Sign up",
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  Get.toNamed(kSignUpPage);
                },
              style: const TextStyle(color: Colors.blueAccent)),
        ])),
        const SizedBox(
          width: 16,
        )
      ],
    );
  }

  Container _buildLoginButton() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: kIsWeb? 10: 24),
      decoration: BoxDecoration(
        color: Colors.blueAccent,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 12),
      child: GestureDetector(
        onTap: () {
          controller.onLoginButtonClick(context);
        },
        child: const Text(
          "Log In",
          style: TextStyle(
              color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget _buildInput({required LogInController controller}) {
    return Column(
      children: [
        Container(
          width: Get.width - 32,
          margin: const EdgeInsets.only(
            left: 16,
            right: 16,
            top: 30,
          ),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Obx(
            () => TextField(
              onChanged: (text) {
                controller.isValidUsername();
              },
              controller: controller.usernameController,
              decoration: InputDecoration(
                prefixIcon: const Icon(
                  Icons.email_outlined,
                  size: 25,
                ),
                contentPadding: EdgeInsets.symmetric(vertical: 16),
                hintText: "Username",
                border: InputBorder.none,
                suffixText: controller.usernameWarning.value,
                suffixStyle: const TextStyle(color: Colors.red, fontSize: 13),
              ),
            ),
          ),
        ),
        Container(
          width: Get.width - 32,
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Obx(
            () => TextField(
              onChanged: (text) {
                controller.isValidPassword();
              },
              controller: controller.passwordController,
              obscureText: true,
              decoration: InputDecoration(
                prefixIcon: const Icon(
                  Icons.lock_outline,
                  size: 25,
                ),
                contentPadding: EdgeInsets.symmetric(vertical: 16),
                hintText: "Password",
                border: InputBorder.none,
                suffixText: controller.passwordWarning.value,
                suffixStyle: const TextStyle(color: Colors.red, fontSize: 13),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
