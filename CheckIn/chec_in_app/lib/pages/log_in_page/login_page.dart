import 'package:chec_in_app/components/constant.dart';
import 'package:flutter/material.dart';
import 'components/body.dart';

class LogInPage extends StatelessWidget {
  const LogInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
        child: Scaffold(
      backgroundColor: kSecondaryColor,
      resizeToAvoidBottomInset: false,
      body: Body(),
    ));
  }
}
