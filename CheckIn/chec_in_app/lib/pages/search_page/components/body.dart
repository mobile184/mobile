import 'dart:async';

import 'package:chec_in_app/components/app_bar.dart';
import 'package:chec_in_app/components/check_in_card.dart';
import 'package:chec_in_app/components/clipper.dart';
import 'package:chec_in_app/components/constant.dart';
import 'package:chec_in_app/controllers/search_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  SearchController controller = Get.find();
  var data = Get.arguments;
  late final AnimationController _controller;
  @override
  void initState() {
    controller
        .loadSearchData(
            idUser: data["idUser"],
            fromDate:new DateTime(DateTime.now().year, DateTime.now().month, 1),
            toDate: DateTime.now(),
            context: context,
            token: data["token"])
        .then((value) {
      setState(() {});
    });
    _controller = AnimationController(vsync: this);
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return controller.searchResponse == null
        ? Center(
            child: Lottie.asset(
              'assets/icons/indicator.json',
              width: 120,
              controller: _controller,
              onLoaded: (composition) {
                _controller
                  ..duration = composition.duration
                  ..forward();
              },
            ),
          )
        : Column(
            children: [
              buildAppBar(
                  avatar: controller.searchResponse.user.avatar,
                  title: controller.searchResponse.user.fullname,
                  subTitle: controller.searchResponse.user.code.toString(),
                  canBack: true),
              TimeOverView(),
              SearchBar(),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(color: Colors.white),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(children: [
                      if (controller.listDateResponse.length == 0)
                        Text(
                          "No data",
                          style: TextStyle(fontSize: 18),
                        )
                      else
                        for (int i = controller.listDateResponse.length - 1;
                            i >= 0;
                            i--)
                          Column(
                            children: [
                              CheckInCard(
                                countTime: controller.searchResponse
                                    .listDateResponse[i].countTime,
                                color: i % 2 == 0
                                    ? Colors.grey.shade300
                                    : Colors.brown.shade100,
                                time: controller.listDateResponse[i].dateString,
                                time_start:
                                    controller.listDateResponse[i].isChecked ==
                                            true
                                        ? DateFormat.jm().format(controller
                                            .listDateResponse[i]
                                            .checkInTime as DateTime)
                                        : "No Data",
                                time_end: controller
                                            .listDateResponse[i].checkOutTime ==
                                        null
                                    ? "No Data"
                                    : DateFormat.jm().format(controller
                                        .listDateResponse[i]
                                        .checkOutTime as DateTime),
                                isCheck:
                                    controller.listDateResponse[i].isChecked,
                              ),
                              Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 16),
                                  child: Divider()),
                            ],
                          ),
                      SizedBox(
                        height: 70,
                      ),
                    ]),
                  ),
                ),
              ),
            ],
          );
  }

  Container MyAppBar() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
      child: Row(
        children: [
          GestureDetector(
              onTap: () => Get.back(),
              child: Icon(
                Icons.arrow_back_ios,
                size: 20,
                color: Colors.black,
              )),
          SizedBox(
            width: 10,
          ),
          Text(
            controller.searchResponse.user.fullname,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget SearchBar() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12, spreadRadius: 0.5, blurRadius: 10),
              ],
              borderRadius: BorderRadius.circular(5)),
          child: Container(
            decoration: BoxDecoration(color: Colors.yellow.shade300),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () => controller.chooseBeginTime(context),
                  child: ClipPath(
                    clipper: ArrowBoxClipper(distance: 20, radius: 5),
                    child: Container(
                      width: kIsWeb ? 225 : Get.width * 0.5,
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 105, 187, 224)),
                      padding: EdgeInsets.symmetric(vertical: 16),
                      child: Obx(
                        () => Text(
                          controller.beginTimeText.value,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () => controller.chooseEndTime(context),
                  child: Container(
                    width: kIsWeb ? 193 : Get.width / 2 - 32,
                    padding: EdgeInsets.symmetric(vertical: 16),
                    child: Obx(
                      () => Text(
                        controller.endTimeText.value,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: Colors.blue.shade300),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              backgroundColor: kIconPrimaryColor,
              padding: EdgeInsets.symmetric(horizontal: 32, vertical: 10)),
          onPressed: () async {
            await controller.onSearchButtonClick(context,
                idUser: data["idUser"], token: data["token"]);
            setState(() {});
          },
          child: Text(
            "Search",
            style: TextStyle(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  Widget TimeOverView() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: ClipPath(
        clipper: ParalellogramClipper(distance: 30),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(color: Colors.black12, spreadRadius: 1, blurRadius: 10)
          ]),
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ClipPath(
                    child: Text(
                      "Worked days: ${controller.searchResponse.workedDays}",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                  ),
                  Text(
                    "Absent days: ${controller.searchResponse.absentDays}",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                "Total time worked: ${controller.searchResponse.workedTimes['hours']}",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              SizedBox(
                height: 16,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
