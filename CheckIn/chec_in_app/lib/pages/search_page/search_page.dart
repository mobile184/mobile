import 'package:chec_in_app/components/constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../route_managements/routes.dart';
import 'components/body.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var data = Get.arguments;
    return SafeArea(
      child: Scaffold(
        body: Body(),
        floatingActionButton: FloatingActionButton(
            child: Icon(
              Icons.home_outlined,
              color: Colors.white,
            ),
            backgroundColor: kIconPrimaryColor,
            onPressed: () {
              Get.back();
            }),
      ),
    );
  }
}
