import 'package:chec_in_app/components/app_bar.dart';
import 'package:chec_in_app/components/constant.dart';
import 'package:chec_in_app/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../route_managements/routes.dart';
import 'components/body.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int idUser = Get.arguments["idUser"];
    String token = Get.arguments["token"];
    return SafeArea(
        child: Scaffold(
      resizeToAvoidBottomInset: false,
      body: Body(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: kIconPrimaryColor,
        onPressed: () {
          Get.toNamed(
            kSearchPage,
            arguments: {
              "idUser": idUser,
              "token": token,
            },
          );
        },
        child: Icon(
          Icons.search,
          color: Colors.white,
        ),
      ),
    ));
  }
}
