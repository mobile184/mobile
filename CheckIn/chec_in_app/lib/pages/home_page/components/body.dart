import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:chec_in_app/components/app_bar.dart';
import 'package:chec_in_app/components/constant.dart';
import 'package:chec_in_app/controllers/home_controller.dart';
import 'package:chec_in_app/models/home_response.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_button/flutter_animated_button.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  HomeController controller = Get.find();
  var data = Get.arguments;
  late final AnimationController _controller;
  @override
  void initState() {
    /// Load data for HomePage when it is initialized
    controller
        .loadHomeData(
            idUser: data["idUser"], context: context, token: data["token"])
        .then((value) {
      setState(() {});
    });
    _controller = AnimationController(vsync: this);
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return controller.homeResponse == null
        ? Center(
            child: Lottie.asset(
              'assets/icons/indicator.json',
              width: 120,
              controller: _controller,
              onLoaded: (composition) {
                _controller
                  ..duration = composition.duration
                  ..forward();
              },
            ),
          )
        : Container(
            width: Get.width,
            height: Get.height,
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              children: [
                buildAppBar(
                    avatar: controller.homeResponse!.userDto.avatar,
                    isOnTime: controller.homeResponse!.isOnTime,
                    isCheckedToday: controller.homeResponse!.isCheckedToday,
                    title: controller.homeResponse!.userDto.fullname,
                    subTitle: controller.homeResponse!.userDto.code.toString()),
                SizedBox(
                  height: kIsWeb ? 10 : 16,
                ),
                AnimatedTextKit(
                  pause: Duration(milliseconds: 600),
                  animatedTexts: [
                    ColorizeAnimatedText(
                      "Current Date",
                      textStyle: TextStyle(
                          fontSize: kIsWeb ? 22 : 28,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                      colors: [Colors.blue, Colors.green, Colors.yellow],
                    ),
                  ],
                  isRepeatingAnimation: true,
                  repeatForever: true,
                ),
                SizedBox(
                  height: kIsWeb ? 10 : 30,
                ),
                Text(
                  DateFormat.yMd().format(DateTime.now()),
                  style: TextStyle(
                      fontSize: kIsWeb ? 22 : 28, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 30,
                ),
                CheckTime(context),
                SizedBox(
                  height: 10,
                ),
                Container(
                    width: Get.width - 32,
                    child: Divider(
                      thickness: 1,
                    )),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        width: kDefaultPadding * 3.5,
                      ),
                      Text(
                        "Time In",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      Spacer(),
                      Text(
                        "Time Out",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: kDefaultPadding * 1.5,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          for (int i =
                                  controller.homeResponse!.listSessions.length -
                                      1;
                              i >= 0;
                              i--)
                            CheckListToday(
                                controller.homeResponse!.listSessions[i]),
                          SizedBox(
                            height: 80,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  Widget CheckListToday(ListSession data) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          padding: EdgeInsets.symmetric(
              horizontal: kDefaultPadding * 0.5,
              vertical: kDefaultPadding * 0.75),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
            child:
                Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
              Icon(
                Icons.calendar_month,
                size: 30,
                color: kIconPrimaryColor,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                DateFormat.jm().format(data.timeIn),
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: kIconPrimaryColor),
              ),
              Spacer(),
              Text(
                (data.timeOut == null)
                    ? "Not Checked Out"
                    : DateFormat.jm().format(data.timeOut as DateTime),
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.orange),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ]),
          ),
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 16), child: Divider()),
      ],
    );
  }

  Row CheckTime(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            AnimatedButton(
              onPress: () async {
                if (controller.homeResponse!.isCheckedNow == false)
                  await controller.checkIn(context,
                      idUser: data["idUser"], token: data["token"]);
                setState(() {});
              },
              height: 50,
              width: 150,
              text: "Check In",
              textStyle: TextStyle(fontSize: 16, color: Colors.white),
              transitionType: TransitionType.LEFT_TO_RIGHT,
              selectedBackgroundColor: Colors.black26,
              isSelected: controller.homeResponse!.isCheckedNow,
              selectedTextColor: Colors.white,
              backgroundColor: kIconPrimaryColor,
              borderRadius: 10,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              controller.homeResponse!.isCheckedToday == false
                  ? "Not Checked In"
                  : "${DateFormat.jm().format(controller.homeResponse!.checkInTime)}",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
          ],
        ),
        SizedBox(
          width: 10,
        ),
        Column(
          children: [
            AnimatedButton(
              onPress: () async {
                if (controller.homeResponse!.isCheckedNow == true)
                  await controller.checkOut(context,
                      idUser: data["idUser"], token: data["token"]);
                setState(() {});
              },
              height: 50,
              width: 150,
              text: "Check Out",
              textStyle: TextStyle(fontSize: 16, color: Colors.white),
              transitionType: TransitionType.LEFT_TO_RIGHT,
              selectedBackgroundColor: Colors.black26,
              isSelected: !controller.homeResponse!.isCheckedNow,
              selectedTextColor: Colors.white,
              backgroundColor: Colors.orange,
              borderRadius: 10,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              controller.homeResponse!.checkOutTime == null
                  ? "Not Checked Out"
                  : "${DateFormat.jm().format(controller.homeResponse!.checkOutTime as DateTime)}",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
          ],
        ),
      ],
    );
  }
}
