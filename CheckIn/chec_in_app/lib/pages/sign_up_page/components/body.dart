import 'package:chec_in_app/controllers/sign_up_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../../../components/constant.dart';
import '../../../route_managements/routes.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> with WidgetsBindingObserver {
  SignUpController controller = Get.find();
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            FocusManager.instance.primaryFocus?.unfocus();
          }
        },
        onVerticalDragDown: (drag) {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            FocusManager.instance.primaryFocus?.unfocus();
          }
        },
        child: Container(
          width: Get.width,
          height: Get.height,
          decoration: const BoxDecoration(
            color: kSecondaryColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: kIsWeb? 10: Get.height * 0.1,
              ),
              Image.asset(
                "assets/icons/logo.png",
                width: kIsWeb? Get.width * 0.15: Get.width * 0.5,
              ),
              _buildInput(),
              _buildSignUpButton(),
              _buildLogInSuggestion(),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildLogInSuggestion() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RichText(
            text: TextSpan(children: [
          const TextSpan(
            text: "Already have an account? ",
            style: TextStyle(color: Colors.white70),
          ),
          TextSpan(
              text: "Login",
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  Get.back();
                },
              style: const TextStyle(color: Colors.blueAccent)),
        ])),
        const SizedBox(
          width: 16,
        )
      ],
    );
  }

  Container _buildSignUpButton() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: kIsWeb? 5: 24),
      decoration: BoxDecoration(
        color: Colors.blueAccent,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 12),
      child: GestureDetector(
        onTap: () => controller.onSignUpButtonClick(
          context,
        ),
        child: const Text(
          "Sign Up",
          style: TextStyle(
              color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Column _buildInput() {
    return Column(
      children: [
        Container(
          width: Get.width - 32,
          margin: const EdgeInsets.only(
            left: 16,
            right: 16,
            top: kIsWeb? 10: 30,
          ),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Obx(
            () => TextField(
              controller: controller.usernameController,
              onChanged: (text) {
                controller.isValidUsername();
              },
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.email_outlined,
                  size: 25,
                ),
                hintText: "Username",
                contentPadding: EdgeInsets.symmetric(vertical: 16),
                border: InputBorder.none,
                suffixText: controller.usernameWarning.value,
                suffixStyle: TextStyle(color: Colors.red, fontSize: 13),
              ),
            ),
          ),
        ),
        Container(
          width: Get.width - 32,
          margin: const EdgeInsets.only(
            left: 16,
            right: 16,
            top: kIsWeb? 10: 16,
          ),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Obx(
            () => TextField(
              controller: controller.fullnameController,
              onChanged: (text) {
                controller.isValidFullname();
              },
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.person_outline,
                  size: 25,
                ),
                contentPadding: EdgeInsets.symmetric(vertical: 16),
                hintText: "Fullname",
                border: InputBorder.none,
                suffixText: controller.fullnameWarning.value,
                suffixStyle: TextStyle(color: Colors.red, fontSize: 13),
              ),
            ),
          ),
        ),
        Container(
          width: Get.width - 32,
          margin: const EdgeInsets.only(left: 16, right: 16,top: kIsWeb? 10: 16,),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Obx(
            () => TextField(
              controller: controller.passwordController,
              onChanged: (text) {
                controller.isValidPassword();
              },
              obscureText: true,
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.lock_outline,
                  size: 25,
                ),
                contentPadding: EdgeInsets.symmetric(vertical: 16),
                hintText: "Password",
                border: InputBorder.none,
                suffixText: controller.passwordWarning.value,
                suffixStyle: TextStyle(color: Colors.red, fontSize: 13),
              ),
            ),
          ),
        ),
        Container(
          width: Get.width - 32,
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: kIsWeb?10: 16),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Obx(
            () => TextField(
              controller: controller.passwordConfirmController,
              onChanged: (text) {
                controller.isValidPasswordConfirm();
              },
              obscureText: true,
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.lock_outline,
                  size: 25,
                ),
                contentPadding: EdgeInsets.symmetric(vertical: 16),
                hintText: "Confirm Password",
                border: InputBorder.none,
                suffixText: controller.passwordConfirmWarning.value,
                suffixStyle: TextStyle(color: Colors.red, fontSize: 13),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
