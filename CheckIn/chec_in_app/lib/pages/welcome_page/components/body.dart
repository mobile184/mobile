import 'dart:async';

import 'package:chec_in_app/components/constant.dart';
import 'package:chec_in_app/controllers/shared_preference.dart';
import 'package:chec_in_app/route_managements/routes.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  initState() {
    super.initState();
    Timer(const Duration(seconds: 3), () async {
      int? id = await MySharedPreference.getUserId();
      String? token = await MySharedPreference.getToken();
      if (id == null || token == null)
        Get.offNamed(kLogInPage);
      else
        Get.offNamed(kHomePage, arguments: {"idUser": id, "token": token});
    });
    _controller = AnimationController(
        duration: const Duration(milliseconds: 3000),
        vsync: this,
        value: 0,
        lowerBound: 0,
        upperBound: 1);
    _animation =
        CurvedAnimation(parent: _controller, curve: Curves.fastOutSlowIn);

    _controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: kSecondaryColor,
      ),
      child: Center(
          child: FadeTransition(
        opacity: _animation,
        child: Image.asset(
          "assets/icons/logo.png",
          width: kIsWeb? Get.width * 0.3 : Get.width * 0.6,
        ),
      )),
    );
  }
}
