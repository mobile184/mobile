import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as getx;

import '../route_managements/routes.dart';

class HttpService {
  late Dio _dio;

  final baseUrl = "http://api-time-checkin.devfast.us/";

  HttpService() {
    _dio = Dio(BaseOptions(
      baseUrl: baseUrl,
    ));
    initializeInterceptors();
  }

  Future<Response> getHomeResponseRequest(String endPoint,
      {required String token, required BuildContext context}) async {
    Response response;

    try {
      _dio.options.headers["Authorization"] = "Bearer ${token}";
      response = await _dio.get(endPoint);
    } on DioError catch (e) {
      print(e.message);
      if (e.response!.statusCode == 401) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("Your Session Is Expired, Please Log In!"),
          ),
        );
        getx.Get.offNamed(kLogInPage);
      }

      throw Exception(e.message);
    }

    return response;
  }

  Future<Response> getSearchResponseRequest(String endPoint,
      {required String token}) async {
    Response response;

    try {
      _dio.options.headers["Authorization"] = "Bearer ${token}";
      response = await _dio.get(endPoint);
    } on DioError catch (e) {
      print(e.message);
      throw Exception(e.message);
    }

    return response;
  }

  Future<Response> putCheckOutRequest(String endPoint,
      {required DateTime dateTime,
      required int idUser,
      required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer ${token}";
      response = await _dio.put(endPoint,
          data: {"timeOut": dateTime.toIso8601String(), "idUser": idUser});
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  Future<Response> postCheckInRequest(String endPoint,
      {required DateTime dateTime,
      required int idUser,
      required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer ${token}";
      response = await _dio.post(
        endPoint,
        data: {
          "timeIn": dateTime == null ? null : dateTime.toIso8601String(),
          "idUser": idUser,
        },
      );
    } on DioError catch (e) {
      throw Exception(e.message);
    }

    return response;
  }

  Future<Response> postLogInRequest(
      String endPoint, String userName, String password) async {
    Response response;
    try {
      response = await _dio
          .post(endPoint, data: {"userName": userName, "password": password});
    } on DioError catch (e) {
      throw Exception(e.message);
    }

    return response;
  }

  Future<Response> postSignUpRequest(
      {required String endPoint,
      required String userName,
      required String fullname,
      required String password,
      String avatar =
          ""}) async {
    Response response;
    try {
      response = await _dio.post(endPoint, data: {
        "fullname": fullname,
        "avatar": avatar,
        "userName": userName,
        "password": password,
        "code": "",
      });
    } on DioError catch (e) {
      throw Exception(e.message);
    }

    return response;
  }

  Future<Response> putRequest(String endPoint) async {
    Response response;

    try {
      response = await _dio.put(endPoint);
    } on DioError catch (e) {
      print(e.message);
      throw Exception(e.message);
    }

    return response;
  }

  initializeInterceptors() {
    _dio.interceptors.add(
        InterceptorsWrapper(onError: (error, errorInterceptorHandler) async {
      print(error.message);
      return errorInterceptorHandler.next(error);
    }, onRequest: (request, requestInterceptorHandler) {
      print("${request.method} ${request.path}");
      return requestInterceptorHandler.next(request);
    }, onResponse: (response, responseInterceptorHandler) {
      print(response.data);
      return responseInterceptorHandler.next(response);
    }));
  }
}
