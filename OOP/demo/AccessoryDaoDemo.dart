import '../dao/Database.dart';
import '../muck_entity/Accessory.dart';
import '../muck_entity/BaseRow.dart';

class AccessoryDaoDemo {
  Database database = Database.instance;
  /** create data 
   * return void
   */
  void init() {
    for (int i = 0; i < 10; i++) {
      Database.instance.accessoryTable.add(new BaseRow(i, "access $i"));
    }
  }

  /** test insert to database
   * return void
   */
  void insertTest() {
    BaseRow row = (new Accessory(20, 'accessDAOInsert'));
    Database.instance.insertTable('accessory', row);
  }

  /**test update 
   * return void
  */
  void updateTest() {
    BaseRow row = new BaseRow(22, 'accessDAOUpdate');
    Database.instance.updateTable('accessory', row);
  }

  /** test delete
   * return void
   */
  void deleteTest() {
    BaseRow row = new BaseRow(25, 'accessDAODelete');
    Database.instance.deleteTable('accessory', row);
  }

  /**test findAll
   * return void
   */
  void findAllTest() {
    List<BaseRow> list = Database.instance.selectTable('accessory');
    print(list);
  }

  /** test find by id
   * return void
   */
  void findByIdTest() {
    int id = 5;
    Database.instance.findById('accessory', id);
  }

  /** truncate database
   * return void
   */
  void truncateTable() {
    Database.instance.truncateTable('accessory');
  }
}

void main() {
  AccessoryDaoDemo demo = new AccessoryDaoDemo();
  demo.init();
  demo.insertTest();
  demo.findAllTest();
}
