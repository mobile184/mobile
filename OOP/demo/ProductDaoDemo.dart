import '../dao/Database.dart';
import '../muck_entity/BaseRow.dart';
import '../muck_entity/Product.dart';

class ProductDaoDemo {
  Database database = Database.instance;
  /** create data 
   * return void
   */
  void init() {
    for (int i = 0; i < 10; i++) {
      Database.instance.productTable.add(new Product(i, "pro $i", i));
    }
  }

  /** test insert to database
   * return void
   */
  void insertTest() {
    BaseRow row = new BaseRow(20, 'proDAOInsert', 5);
    Database.instance.insertTable('product', row);
  }

  /**test update 
   * return void
  */
  void updateTest() {
    BaseRow row = new BaseRow(22, 'proDAOUpdate', 6);
    Database.instance.updateTable('product', row);
  }

  /** test delete
   * return void
   */
  void deleteTest() {
    BaseRow row = new BaseRow(25, 'proDAODelete', 8);
    Database.instance.deleteTable('product', row);
  }

  /**test findAll
   * return void
   */
  void findAllTest() {
    List<BaseRow> list = Database.instance.selectTable('product');
    print(list);
  }

  /** test find by id
   * return void
   */
  void findByIdTest() {
    int id = 5;
    Database.instance.findById('product', id);
  }

  /** truncate database
   * return void
   */
  void truncateTable() {
    Database.instance.truncateTable('product');
  }
}

void main() {
  ProductDaoDemo demo = new ProductDaoDemo();
  demo.init();
  demo.insertTest();
  demo.findAllTest();
}
