import '../dao/Database.dart';
import '../muck_entity/BaseRow.dart';
import '../muck_entity/Category.dart';

class CategoryDaoDemo {
  Database database = Database.instance;
  /** create data 
   * return void
   */
  void init() {
    for (int i = 0; i < 10; i++) {
      Database.instance.categoryTable.add(new BaseRow(i, "cate $i"));
    }
  }

  /** test insert to database
   * return void
   */
  void insertTest() {
    BaseRow row = new BaseRow(20, 'cateDAOInsert');
    Database.instance.insertTable('category', row);
  }

  /**test update 
   * return void
  */
  void updateTest() {
    BaseRow row = new BaseRow(22, 'cateDAOUpdate');
    Database.instance.updateTable('category', row);
  }

  /** test delete
   * return void
   */
  void deleteTest() {
    BaseRow row = new BaseRow(25, 'cateDAODelete');
    Database.instance.deleteTable('category', row);
  }

  /**test findAll
   * return void
   */
  void findAllTest() {
    List<Category> list = Database.instance.selectTable('category');
    print(list);
  }

  /** test find by id
     * return void
     */
  void findByIdTest() {
    int id = 5;
    Database.instance.findById('category', id);
  }

  /** truncate database
   * return void
   */
  void truncateTable() {
    Database.instance.truncateTable('category');
  }
}

void main() {
  CategoryDaoDemo demo = new CategoryDaoDemo();
  demo.init();
  demo.insertTest();
  demo.findAllTest();
}
