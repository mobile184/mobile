import '../muck_entity/BaseRow.dart';

abstract class IDao {

  /** insert row to database
   * @param row 
   * return bool
   */
  bool insert(BaseRow row);
  /** delete row from database
   * @param row
   * return void
  */
  void delete(BaseRow row);
  /** update row to database
   * @param row
   * return void
   */
  void update(BaseRow row);
  /** find all data from database
   * @param name
   * return dynamic
   */
  dynamic findAll(String name);
  /** find row by id
   * @param id
   * return dynamic
   */
  dynamic findById(String name, int id);
}