abstract class IEntity {
  /**
   * get id 
   * return int
   */
  int get id;
  /**
   * set id
   * return void
   */
  void set id(int id);
  /**
   * get name
   * return String
   */
  String get name;
  /**
   * set name
   * return void
   */
  void set name(String name);
}
