import '../muck_entity/Accessory.dart';
import '../muck_entity/BaseRow.dart';
import '../muck_entity/Category.dart';
import '../muck_entity/Product.dart';

class Database {

  static const String NAME_PRODUCT = 'product';
  static const String NAME_CATEGORY = 'category';
  static const String NAME_ACCESSORY = 'accessory';

  //List of array used in database
  List<BaseRow> _productTable = [];
  List<BaseRow> _categoryTable = [];
  List<BaseRow> _accessoryTable = [];

  //class Database only has 1 object _instance
  static final Database _instance = Database._();
  Database._();
  static get instance => _instance;

  List<BaseRow> get productTable{ 
    return this._productTable;
  }
  List<BaseRow> get categoryTable {
    return this._categoryTable;
  }
  List<BaseRow> get accessoryTable {
    return this._accessoryTable;
  }
  /**
   * insert row to table
   * @param name
   * return void
   */
  void insertTable(String name, BaseRow row) {
    if (row is Product || row is Category || row is Accessory) {
      if (name == NAME_PRODUCT) {
        productTable.add(row);
      } else if (name == NAME_CATEGORY) {
        categoryTable.add(row);
      } else if (name == NAME_ACCESSORY) {
        accessoryTable.add(row);
      }
    }
  }

  /**
   * select table from database
   * @param name
   * return dynamic
   */
  dynamic selectTable(String name) {
    if (name == NAME_PRODUCT) {
      return productTable;
    } else if (name == NAME_CATEGORY) {
      return categoryTable;
    } else if (name == NAME_ACCESSORY) {
      return accessoryTable;
    }
  }

  /**
   * update row to table of database by name
   * @param name
   * @param row
   * return void
   */
  void updateTable(String name, BaseRow row) {
    int id = row.id;
    if (row is Product || row is Category || row is Accessory) {
      if (name == NAME_PRODUCT) {
        for (int i = 0; i < productTable.length; i++) {
          if (productTable[i].id == id) {
            productTable[i] = row;
            break;
          }
        }
      } else if (name == NAME_CATEGORY) {
        for (int i = 0; i < categoryTable.length; i++) {
          if (categoryTable[i].id == id) {
            categoryTable[i] = Category(row.id, row.name);
            break;
          }
        }
      } else if (name == NAME_ACCESSORY) {
        for (int i = 0; i < accessoryTable.length; i++) {
          if (accessoryTable[i].id == id) {
            accessoryTable[i] = Accessory(row.id, row.name);
            break;
          }
        }
      }
    }
  }

  /**
   * update row to table by id
   * @param id
   * @param row
   * return void
   */
  void updateTableById(int id, BaseRow row) {
    //check type of object to update
    if (row is Product) {
      for (int i = 0; i < productTable.length; i++) {
        if (productTable[i].id == id) {
          productTable[i] = row;
          break;
        }
      }
    } else if (row is Category) {
      for (int i = 0; i < categoryTable.length; i++) {
        if (categoryTable[i].id == id) {
          categoryTable[i] = row;
          break;
        }
      }
    } else if (row is Accessory) {
      for (int i = 0; i < accessoryTable.length; i++) {
        if (accessoryTable[i].id == id) {
          accessoryTable[i] = row;
          break;
        }
      }
    }
  }

  /** 
   * delete table by id of row
   * @param name
   * @param row
   * return void
   */
  void deleteTable(String name, BaseRow row) {
    if (row is Product || row is Category || row is Accessory) {
      int id = row.id;
      if (name == NAME_PRODUCT) {
            for (int i = 0; i < productTable.length; i++) {
              if (productTable[i].id == id) {
                productTable.removeAt(i);
                break;
              }
            }
          }
         else if (name == NAME_CATEGORY)
          {
            for (int i = 0; i < categoryTable.length; i++) {
              if (categoryTable[i].id == id) {
                categoryTable.remove(i + 1);
                break;
              }
            }
          }
          else if (name == NAME_ACCESSORY)
          {
            for (int i = 0; i < accessoryTable.length; i++) {
              if (accessoryTable[i].id == id) {
                accessoryTable.remove(i + 1);
                break;
              }
            }
          }
      }
    }

  /** 
   * truncate table by table name
   * @param name
   * return void
   */
  void truncateTable(String name) {
    if (name == NAME_PRODUCT) {
      productTable.clear();
    } else if  (name == NAME_CATEGORY) {
      categoryTable.clear();
    } else if (name == accessoryTable) {
      accessoryTable.clear();
    }
  }

}
