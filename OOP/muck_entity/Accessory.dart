import 'BaseRow.dart';

class Accessory extends BaseRow{
  /**
   * constructor
   * @param id
   * @param name
   */
  Accessory(int id, String name) : super(id, name);
}
