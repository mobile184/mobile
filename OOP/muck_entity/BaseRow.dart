import '../interface/IEntity.dart';

class BaseRow implements IEntity{
  late int _id;
  late String _name;
  late int _categoryId;
  BaseRow(int id, String name, [int? categoryId]) {
    this._id = id;
    this._name = name;
    if (categoryId != null) this._categoryId = categoryId;
  }
  int get id {
    return _id;
  }

  void set id(int id) {
    _id = id;
  }

  String get name {
    return _name;
  }

  void set name(String name) {
    _name = name;
  }

  String toString() {
    return "{id: $id, name: $name}";
  }
}
