import 'BaseRow.dart';

class Product extends BaseRow {
  late int _categoryId;

  /**
   * constructor
   * @param id
   * @param name
   * @param categoryId
   */
  Product(int id, String name, int categoryId) : super(id, name) {
    this._categoryId = categoryId;
  }

  int get category{
    return this._categoryId;
  }

  void set category(int categoryId) {
    this._categoryId = categoryId;
  }

  @override
  String toString() {
    return "{id: ${this.id}, name: ${this.name}, categoryId: ${this._categoryId}}";
  }
}
