import '../muck_entity/Product.dart';
import '../muck_entity/Category.dart';
import '../muck_entity/Accessory.dart';
import '../dao/Database.dart';
import '../interface/IDao.dart';

abstract class BaseDao implements IDao{

  /**
   * insert row
   * @param row
   * return bool
   */
  @override
  bool insert(row) {
    if (row is Product) {
      Database.instance.insertTable(Database.NAME_PRODUCT, row);
      return true;
    } else if (row is Category) {
      Database.instance.insertTable(Database.NAME_CATEGORY, row);
      return true;
    } else if (row is Accessory) {
      Database.instance.insertTable(Database.NAME_ACCESSORY, row);
      return true;
    }
    return false;
  }

  /**
   * update row
   * @param row
   * return void
   */
  @override
  void update(row) {
    if (row is Product) {
      Database.instance.updateTable(Database.NAME_PRODUCT, row);
    } else if (row is Category) {
      Database.instance.updateTable(Database.NAME_CATEGORY, row);
    } else if (row is Accessory) {
      Database.instance.updateTable(Database.NAME_ACCESSORY, row);
    }
  }

  /**
   * delete row
   * @param row
   * return void
   */
  @override
  void delete(row) {
    if (row is Product) {
      Database.instance.deleteTable(Database.NAME_PRODUCT, row);
    } else if (row is Category) {
      Database.instance.deleteTable(Database.NAME_CATEGORY, row);
    } else if (row is Accessory) {
      Database.instance.deleteTable(Database.NAME_ACCESSORY, row);
    }
  }

  /**
   * find table by name
   * @param name
   * return 
   */
  @override
  findAll(String name) {
    return Database.instance.selectTable(name);
  }
  @override
  findById(String name, int id) {
    return Database.instance.findById(name, id);
  }
}
