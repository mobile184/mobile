Các lớp trong DAO có các function tương tự nhau như 
update, insert, delete, findAll, findById.
Do các lớp DAO có bản chất như nhau nên
khi ta dùng 1 class abtract chứa các function này và 
cho các lớp trên kế thừa, ta sẽ có thể kiểm soát được
liệu các class con này đã được cài đủ các function cơ bản hay chưa,
tránh việc bị thiếu chức năng