import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x_1/helper/init_dep.dart';

import 'my_home_page.dart';
import "helper/init_controllers.dart" as di;

Future<void> main() async{
  //WidgetsFlutterBinding.ensureInitialized();
  //await di.init;
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: InitDep(),
      title: 'GetX',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

