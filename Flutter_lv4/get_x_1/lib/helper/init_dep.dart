import 'dart:ffi';

import 'package:get/get.dart';

import '../controllers/list_controllers.dart';
import '../controllers/tap_controllers.dart';

class InitDep implements Bindings {
    @override
    void dependencies() {
      Get.lazyPut(() => TapController());
    Get.lazyPut(() => ListController());
    }
}