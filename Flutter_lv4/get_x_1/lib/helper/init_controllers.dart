
import 'package:get/get.dart';
import 'package:get_x_1/controllers/list_controllers.dart';
import 'package:get_x_1/controllers/tap_controllers.dart';

Future<void> init() async {
  Get.put(() => TapController());
  Get.put(() => ListController());
}