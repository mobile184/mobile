import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:get_x_1/controllers/list_controllers.dart';
import 'package:get_x_1/controllers/tap_controllers.dart';

class ThirdPage extends StatelessWidget {
  const ThirdPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //TapController controller = Get.find();
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(() => Column(
              children: [
                Container(
                margin: EdgeInsets.all(20),
                width: double.infinity,
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color(0xFF89dad0),
                ),
                child: Center(child: Text("Y value: " + Get.find<TapController>().y.toString(),
                style: TextStyle(fontSize: 20,
                color: Colors.white),),),
          ),
          GestureDetector(
              onTap: () {
                //controller.increaseX();
                //Get.find<TapController>().totalXY();
              },
              child: Container(
                margin: EdgeInsets.all(20),
                width: double.infinity,
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color(0xFF89dad0),
                ),
                child: Center(child: Text("Total X+Y " + Get.find<TapController>().z.toString(),
                style: TextStyle(fontSize: 20,
                color: Colors.white),),),
              ),
            ),
              ],
            ),
            ),
          Container(
            margin: EdgeInsets.all(20),
            width: double.infinity,
            height: 80,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Color(0xFF89dad0),
            ),
            child: Center(child: Text("X value: " + Get.find<TapController>().x.toString(),
            style: TextStyle(fontSize: 20,
            color: Colors.white),),),
          ),
            GestureDetector(
              onTap: () {
                //controller.increaseX();
                Get.find<TapController>().increaseY();
              },
              child: Container(
                margin: EdgeInsets.all(20),
                width: double.infinity,
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color(0xFF89dad0),
                ),
                child: Center(child: Text("Increase Y",
                style: TextStyle(fontSize: 20,
                color: Colors.white),),),
              ),
            ),
            GestureDetector(
              onTap: () {
                //controller.increaseX();
                Get.find<TapController>().totalXY();
              },
              child: Container(
                margin: EdgeInsets.all(20),
                width: double.infinity,
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color(0xFF89dad0),
                ),
                child: Center(child: Text("Total X+Y " ,
                style: TextStyle(fontSize: 20,
                color: Colors.white),),),
              ),
            ),
            GestureDetector(
              onTap: () {
                //controller.increaseX();
                //Get.find<TapController>().totalXY();
                Get.find<ListController>().setValue(Get.find<TapController>().z.value);
              },
              child: Container(
                margin: EdgeInsets.all(20),
                width: double.infinity,
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Color(0xFF89dad0),
                ),
                child: Center(child: Text("Save total",
                style: TextStyle(fontSize: 20,
                color: Colors.white),),),
              ),
            ),
        ]),
      ),
    );
  }
}
