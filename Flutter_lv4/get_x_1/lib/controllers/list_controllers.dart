import 'package:get/get.dart';

class ListController {
  RxList _list = [].obs;
  RxList get list => _list;
  void setValue(int x) {
    _list.add(x);
  }
}