import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class PopularContentList extends StatefulWidget {
  final List info;
  const PopularContentList({Key? key, required this.info}) : super(key: key);
  @override
  State<PopularContentList> createState() => _PopularContentListState();
}

class _PopularContentListState extends State<PopularContentList> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 25, right: 25),
          child: Row(
            children: [
              Text(
                "Popular Contest",
                style: TextStyle(
                    color: Color(0xFF1f2326),
                    fontSize: 20,
                    decoration: TextDecoration.none),
              ),
              Expanded(child: Container()),
              Text(
                "Show all",
                style: TextStyle(
                    color: Colors.orange,
                    fontSize: 15,
                    decoration: TextDecoration.none),
              ),
              SizedBox(
                width: 5,
              ),
              Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xFFfdc33c)),
                child: GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          height: 220,
          child: PageView.builder(
              controller: PageController(viewportFraction: 0.88),
              itemCount: widget.info.length,
              itemBuilder: (_, i) {
                return GestureDetector(
                  onTap: () {
                    Get.toNamed("/detail", arguments: {
                      "title": widget.info[i]["title"].toString(),
                      "text": widget.info[i]["text"].toString(),
                      "name": widget.info[i]["name"].toString(),
                      "img": widget.info[i]["img"].toString(),
                      "time": widget.info[i]["time"].toString(),
                      "prize": widget.info[i]["prize"].toString(),
                    });
                  },
                  child: Container(
                    padding: const EdgeInsets.only(left: 20, top: 20),
                    height: 220,
                    width: MediaQuery.of(context).size.width - 20,
                    margin: const EdgeInsets.only(right: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color:
                            i.isEven ? Color(0xFF69c5df) : Color(0xFF9294cc)),
                    child: Column(
                      children: [
                        Container(
                            child: Row(
                          children: [
                            Text(
                              widget.info[i]["title"],
                              style: TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white),
                            ),
                            Expanded(child: Container())
                          ],
                        )),
                        SizedBox(height: 10),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            widget.info[i]["text"],
                            style: TextStyle(
                                fontSize: 18, color: Color(0xFFb8eefc)),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Divider(
                          thickness: 1.0,
                        ),
                        Row(children: [
                          for (int i = 0; i < 4; i++)
                            Container(
                              width: 50,
                              height: 50,
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    image: DecorationImage(
                                        image:
                                            AssetImage(widget.info[i]["img"]),
                                        fit: BoxFit.cover)),
                              ),
                            )
                        ]),
                      ],
                    ),
                  ),
                );
              }),
        ),
      ],
    );
  }
}
