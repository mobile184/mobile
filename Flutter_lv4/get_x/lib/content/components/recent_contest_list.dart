import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../../recent_contest/recent_contest.dart';

class RecentContestList extends StatefulWidget {
  final List list;
  const RecentContestList({Key? key, required this.list}) : super(key: key);
  @override
  State<RecentContestList> createState() => _RecentContestListState();
}

class _RecentContestListState extends State<RecentContestList> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemCount: widget.list.length,
          itemBuilder: (_, i) {
            return Container(
              width: MediaQuery.of(context).size.width,
              height: 80,
              margin: const EdgeInsets.only(left: 20, right: 20, bottom: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xFFebf8fd),
              ),
              child: Container(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: [
                    CircleAvatar(
                      radius: 30,
                      backgroundImage: AssetImage(widget.list[i]["img"]),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.list[i]["status"],
                          style: TextStyle(
                              color: Colors.orange,
                              fontSize: 14,
                              decoration: TextDecoration.none),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        SizedBox(
                          width: 150,
                          child: Text(
                            widget.list[i]["text"],
                            style: TextStyle(
                                color: Color(0xFF3b3f42),
                                fontSize: 15,
                                decoration: TextDecoration.none),
                          ),
                        )
                      ],
                    ),
                    Expanded(child: Container()),
                    Container(
                      width: 40,
                      height: 40,
                      child: Text(
                        "Time",
                        style: TextStyle(
                            fontSize: 10,
                            decoration: TextDecoration.none,
                            color: Color(0xFFb2b8bb)),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
