import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x/content/components/header.dart';
import 'package:get_x/content/components/recent_contest_list.dart';
import 'package:get_x/controllers/content_controller.dart';

import '../recent_contest/recent_contest.dart';
import 'components/popular_contest_list.dart';

class ContentPage extends StatefulWidget {
  const ContentPage({Key? key}) : super(key: key);

  @override
  _ContentPageState createState() => _ContentPageState();
}

class _ContentPageState extends State<ContentPage> {
  List list = [];
  List info = [];
  final ContentController controller = Get.find();

  /** get data from json file
   * @param null
   * return void
   */
  _readDataList() async {
    await controller.readDataList(context).then((value) {
      setState(() {
        list = value;
      });
    });
    await controller.readDataInfo(context).then((value) {
      setState(() {
        info = value;
      });
    });
  }

  @override
  void initState() {
    setState(() {
      _readDataList();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    int _currentIndex = 0;
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: 70),
        color: Color(0xFFc5e5f3),
        child: Column(
          children: [
            //content header
            ContentPageHeader(),
            SizedBox(
              height: 20,
            ),
            //popular contest
            PopularContentList(
              info: info,
            ),
            SizedBox(
              height: 30,
            ),
            //recent contests
            Container(
              padding: const EdgeInsets.only(left: 25, right: 25, bottom: 10),
              child: Row(
                children: [
                  Text(
                    "Recent Contests",
                    style: TextStyle(
                        color: Color(0xFF1f2326),
                        fontSize: 20,
                        decoration: TextDecoration.none),
                  ),
                  Expanded(child: Container()),
                  Text(
                    "Show all",
                    style: TextStyle(
                        color: Colors.orange,
                        fontSize: 15,
                        decoration: TextDecoration.none),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color(0xFFfdc33c)),
                    child: GestureDetector(
                      onTap: () {
                        Get.to(() => RecentContest());
                      },
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ),
            //Recent Contest List
            RecentContestList(list: list),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
