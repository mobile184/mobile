import 'package:get/get.dart';
import 'package:get_x/route_management/routes.dart';

import '../detail/detail_page.dart';
import '../home/my_home_page.dart';

class AllPages {

  /** get list of page for routing
   * @param null
   * return List<GetPage>
   */
  static List<GetPage> getPages() {
    return [
      GetPage(name: kMyHomePage, page: () => MyHomePage()),
      GetPage(name: kDetailPage, page: () => DetailPage()),
    ];
  }
}
