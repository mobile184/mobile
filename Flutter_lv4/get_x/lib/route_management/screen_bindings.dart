import 'package:get/get.dart';
import 'package:get_x/controllers/content_controller.dart';

import '../controllers/detail_controller.dart';

class ScreenBindings implements Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut(() => DetailController());
    Get.lazyPut(() => ContentController());
  }
}

