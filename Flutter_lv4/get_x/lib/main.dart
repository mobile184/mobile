import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x/home/my_home_page.dart';
import 'package:get_x/route_management/all_pages.dart';
import 'package:get_x/route_management/routes.dart';

import 'detail/detail_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter GetX',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: kMyHomePage,
      getPages: AllPages.getPages(),
    );
  }
}
