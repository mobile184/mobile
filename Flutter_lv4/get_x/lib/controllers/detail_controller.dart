import 'package:get/get.dart';

class DetailController extends GetxController {
  var fav = 0.obs;
  
  /** show a snackbar when you hit the button
   * @param null
   * return null
   */
  void favCounter() {
    if (fav.value == 1) {
      Get.snackbar("Loved it", "You already loved it!");
    } else {
      fav.value++;
      Get.snackbar("Loved it", "You just loved it!");
    }
  }
}