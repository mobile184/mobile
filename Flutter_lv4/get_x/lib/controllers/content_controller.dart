import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContentController extends GetxController {

  /** read data from file "recent.json"
   * @param context
   * return List
   */
  Future<List> readDataList(BuildContext context) async {
    return await DefaultAssetBundle.of(context)
        .loadString("assets/json/recent.json")
        .then((s) => json.decode(s));
  }

  /** read data from file "detail.json"
   * @param context
   * return List
   */
  Future<List> readDataInfo(BuildContext context) async {
    return await DefaultAssetBundle.of(context)
        .loadString("assets/json/detail.json")
        .then((s) => json.decode(s));
  }
}
