import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class TopParticipant extends StatelessWidget {
  const TopParticipant({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 120,
      left: 0,
      height: 100,
      width: MediaQuery.of(context).size.width,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 100,
        margin: const EdgeInsets.only(left: 25, right: 25),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Color(0xFFebf8fd),
        ),
        child: Container(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Row(
            children: [
              CircleAvatar(
                radius: 30,
                backgroundImage: AssetImage(Get.arguments["img"]),
              ),
              SizedBox(
                width: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    Get.arguments["name"],
                    style: TextStyle(
                        color: Color(0xFF3b3f42),
                        fontSize: 18,
                        decoration: TextDecoration.none),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Top Level",
                    style: TextStyle(
                        color: Colors.orange,
                        fontSize: 12,
                        decoration: TextDecoration.none),
                  ),
                ],
              ),
              Expanded(child: Container()),
              Container(
                width: 70,
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xFFf3fafc)),
                child: Center(
                  child: Icon(
                    Icons.notifications,
                    color: Color(0xFF69c5df),
                    size: 30,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
