import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class TotalParticipants extends StatelessWidget {
  final List imgs;
  const TotalParticipants({Key? key, required this.imgs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      for (int i = 0; i < imgs.length; i++)
        Positioned(
          top: 540,
          left: (20 + i * 35).toDouble(),
          width: 50,
          height: 50,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                image: DecorationImage(
                    image: AssetImage(imgs[i]["img"]), fit: BoxFit.cover)),
          ),
        )
    ]);
  }
}
