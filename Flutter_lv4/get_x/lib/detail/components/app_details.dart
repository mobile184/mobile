import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class AppDetail extends StatelessWidget {
  const AppDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 240,
      left: 0,
      width: MediaQuery.of(context).size.width,
      height: 230,
      child: Container(
        margin: const EdgeInsets.only(left: 25, right: 25),
        width: MediaQuery.of(context).size.width,
        height: 250,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Color(0xFFfcfffe),
            boxShadow: [
              BoxShadow(
                  blurRadius: 20,
                  spreadRadius: 1,
                  offset: Offset(0, 10),
                  color: Colors.grey.withOpacity(0.2))
            ]),
        child: Container(
          margin:
              const EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                  child: Row(
                children: [
                  Text(
                    Get.arguments["title"],
                    style: TextStyle(fontSize: 28, fontWeight: FontWeight.w500),
                  ),
                  Expanded(child: Container())
                ],
              )),
              SizedBox(height: 20),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Text(
                  Get.arguments["text"],
                  style: TextStyle(fontSize: 16, color: Color(0xFFb8b8b8)),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Divider(
                thickness: 1.0,
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(Icons.watch_later, color: Color(0xFF69c5df)),
                      SizedBox(
                        width: 5,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            Get.arguments["name"],
                            style: TextStyle(
                                fontSize: 14,
                                color: Color(0xFF303030),
                                fontWeight: FontWeight.w700),
                          ),
                          Text(
                            "Deadline",
                            style: TextStyle(
                                fontSize: 14, color: Color(0xFFacacac)),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(Icons.monetization_on, color: Color(0xFFfb8483)),
                      SizedBox(
                        width: 5,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            Get.arguments["prize"],
                            style: TextStyle(
                                fontSize: 14,
                                color: Color(0xFF303030),
                                fontWeight: FontWeight.w700),
                          ),
                          Text(
                            "Prize",
                            style: TextStyle(
                                fontSize: 14, color: Color(0xFFacacac)),
                          )
                        ],
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(Icons.star, color: Color(0xFFfbc33e)),
                      SizedBox(
                        width: 5,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Top Level",
                            style: TextStyle(
                                fontSize: 14,
                                color: Color(0xFF303030),
                                fontWeight: FontWeight.w700),
                          ),
                          Text(
                            "Entry",
                            style: TextStyle(
                                fontSize: 14, color: Color(0xFFacacac)),
                          )
                        ],
                      )
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
