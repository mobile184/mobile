import 'dart:convert';
import 'package:get/get.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x/controllers/detail_controller.dart';
import 'package:get_x/detail/components/app_details.dart';
import 'package:get_x/detail/components/top_participant.dart';
import 'package:get_x/detail/components/total_participants.dart';

import '../content/content_page.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  List imgs = [];

  /** read data from json file "img.json"
   * @param null
   * return void
   */
  _readData() async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/json/img.json")
        .then((s) {
      setState(() {
        imgs = json.decode(s);
      });
    });
  }

  @override
  void initState() {
    _readData();
    super.initState();
  }

  final DetailController fav = Get.find();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        color: Color(0xFFc5e5f3),
        child: Stack(
          children: [
            Positioned(
                top: 50,
                left: 10,
                child: IconButton(
                  onPressed: () => Get.to(() => ContentPage()),
                  icon: Icon(
                    Icons.home_outlined,
                    color: Colors.white,
                  ),
                )),
            //Top Participant
            TopParticipant(),
            Positioned(
              top: 320,
              left: 0,
              width: width,
              height: height,
              child: Container(
                width: 80,
                height: 80,
                color: Color(0xFFf9fbfc),
              ),
            ),
            //App Details
            AppDetail(),
            Positioned(
                top: 500,
                left: 25,
                height: 50,
                child: Container(
                  child: RichText(
                      text: TextSpan(
                          text: "Total Participants ",
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 18,
                              color: Colors.black),
                          children: [
                        TextSpan(
                            text: "(11)",
                            style: TextStyle(color: Color(0xFFfbc33e)))
                      ])),
                )),
            //Total participants
            TotalParticipants(imgs: imgs),
            //favourite
            Positioned(
                top: 610,
                left: 25,
                child: Row(
                  children: [
                    Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Color(0xFFfbc33e)),
                        child: IconButton(
                            onPressed: () => fav.favCounter(),
                            icon: Icon(Icons.favorite_border),
                            color: Colors.white)),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Add to favorite",
                      style: TextStyle(color: Color(0xFFfbc33e), fontSize: 16),
                    )
                  ],
                ))
            //))
          ],
        ),
      ),
    );
  }
}
