import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CreatePageController extends GetxController {
  TextEditingController titleController = TextEditingController();
  TextEditingController textController = TextEditingController();
  //this var is use to check condition for submit button
  bool canSubmmit = false;
  String? dropdownValue;
  List<File> imageFile = [];
  List<String> listCategory = ["Food & Drink", "Desserts"];
}