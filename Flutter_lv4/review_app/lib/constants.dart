import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const kDefaultPadding = 20.0;
const kPrimaryColor = Color.fromARGB(255, 78, 46, 241);
const kIconColor = Color.fromARGB(255, 150, 148, 148);
const kTextColor = Color.fromARGB(0, 116, 111, 111);
const kDefaultShadow = BoxShadow(
  color: Color.fromARGB(0, 168, 164, 164),
  blurRadius: 10,
  offset: Offset(5, 5),
);
