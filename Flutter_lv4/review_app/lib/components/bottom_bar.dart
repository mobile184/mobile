import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:review_app/home_page/home_page.dart';
import 'package:review_app/profile_page/profile_page.dart';
import 'package:review_app/route_management/screen_bindings.dart';

import '../constants.dart';
import '../create_page/create_page.dart';
import '../notification_page/notification_page.dart';
import '../search_page/search_page.dart';

class BottomBar extends StatelessWidget {
  final int current_page;
  const BottomBar({Key? key, required this.current_page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
        color: Colors.white,
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        GestureDetector(
          onTap: () {
            Get.to(() =>HomePage(), duration: Duration(seconds: 0));
          },
          child: Container(
            margin: EdgeInsets.all(kDefaultPadding / 2),
            height: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SvgPicture.asset(
                  "assets/icons/reviews.svg",
                  height: 25,
                  color: current_page==0? kPrimaryColor:Colors.grey,
                ),
                Text(
                  "Reviews",
                  style: TextStyle(
                      fontSize: 14,
                      color: current_page == 0 ? kPrimaryColor : Colors.grey),
                )
              ],
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            Get.to(() => SearchPage(), duration: Duration(seconds: 0));
          },
          child: Container(
            margin: EdgeInsets.all(kDefaultPadding / 2),
            height: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SvgPicture.asset(
                  "assets/icons/find.svg",
                  height: 25,
                  color: current_page == 1 ? kPrimaryColor : Colors.grey,
                ),
                Text(
                  "Search",
                  style: TextStyle(
                      color: current_page == 1 ? kPrimaryColor : Colors.grey,
                      fontSize: 14),
                )
              ],
            ),
          ),
        ),
        GestureDetector(
          onTap: (() {
            Get.to(() => CreatePage(), duration: Duration(seconds: 0), binding: ScreenBindings());
          }),
          child: Container(
            padding: EdgeInsets.all(10),
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: kPrimaryColor,
            ),
            child: SvgPicture.asset(
              "assets/icons/edit.svg",
              color: Colors.white,
              height: 15,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            Get.to(() => NotificationPage(), duration: Duration(seconds: 0));
          },
          child: Container(
            margin: EdgeInsets.all(kDefaultPadding / 2),
            height: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SvgPicture.asset(
                  "assets/icons/bell.svg",
                  height: 25,
                  color: current_page == 2 ? kPrimaryColor : Colors.grey,
                ),
                Text(
                  "Notifications",
                  style: TextStyle(
                      color: current_page == 2 ? kPrimaryColor : Colors.grey,
                      fontSize: 14),
                )
              ],
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            Get.to(() => ProfilePage(), duration: Duration(seconds: 0));
          },
          child: Container(
            margin: EdgeInsets.all(kDefaultPadding / 2),
            height: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SvgPicture.asset(
                  "assets/icons/user.svg",
                  height: 25,
                  color: current_page == 3 ? kPrimaryColor : Colors.grey,
                ),
                Text(
                  "Profile",
                  style: TextStyle(
                      color: current_page == 3 ? kPrimaryColor : Colors.grey,
                      fontSize: 14),
                )
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
