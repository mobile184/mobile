import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:review_app/components/bottom_bar.dart';

import 'components/body.dart';

class ProfilePage extends StatelessWidget {
  
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      bottomNavigationBar: BottomBar(current_page: 3,),
    );
  }
}