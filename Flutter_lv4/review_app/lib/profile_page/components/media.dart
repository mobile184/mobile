import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../constants.dart';

class Media extends StatelessWidget {
  final profile;
  const Media({Key? key, required this.profile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [Row(
                children: [
                  Text(
                    "Media",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  
                  Spacer(),
                  Text(
                    "See all",
                    style: TextStyle(color: kPrimaryColor),
                  )
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width - 20,
                padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    for (int i = 0; i < profile[0]["media"].length; i++) 
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(profile[0]["media"][i],
                        width: 90,
                        height: 80,
                        fit: BoxFit.cover,),
                      ),
                    ),
                  ]),
                ),
              ),
      ]
    );
  }
}