import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:readmore/readmore.dart';
import 'package:review_app/profile_page/components/my_following.dart';
import 'package:review_app/profile_page/components/my_review.dart';
import 'package:review_app/profile_page/components/profile_header.dart';

import '../../constants.dart';
import 'media.dart';
import 'my_followers.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List profile = [];
  /** read data from json file "home.json"
   * @param null
   * return void
   */
  _readData() async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/json/profile.json")
        .then((s) {
      setState(() {
        profile = json.decode(s);
      });
    });
  }

  @override
  void initState() {
    _readData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return profile.isEmpty
        ? Container()
        : SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(children: [
              ProfileHeader(),
              Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.9),
                ),
                child: Column(
                  children: [
                    MyReview(),
                    MyFollowers(profile: profile),
                    MyFollowing(
                      profile: profile,
                    ),
                    Media(profile: profile),
                  ],
                ),
              ),
            ]),
          );
  }
}
