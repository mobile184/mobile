import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:readmore/readmore.dart';

import '../../constants.dart';

class MyReview extends StatelessWidget {
  const MyReview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column
    (children: [Row(
                children: [
                  Text(
                    "My Reviews",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "(1)",
                    style: TextStyle(color: Colors.grey),
                  ),
                  Spacer(),
                  Text(
                    "See all",
                    style: TextStyle(color: kPrimaryColor),
                  )
                ],
              ),
             
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                ),
                child: Column(children: [
                  ListTile(
                    dense: true,
                    visualDensity: VisualDensity(vertical: -3),
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(
                          "https://thumbs.dreamstime.com/b/portrait-young-beautiful-girl-fashion-photo-29870052.jpg"),
                    ),
                    title: Text("Carolyne Agustine"),
                    subtitle: Text(
                      "1h ago",
                      style: TextStyle(color: Colors.grey),
                    ),
                    trailing: Icon(Icons.more_vert),
                  ),
                  Divider(
                    thickness: 1,
                  ),
                  Text(
                    "Secangkir Jawa",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  RatingBar.builder(
                    initialRating: double.parse("5"),
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: ReadMoreText(
                      "I'm very happy with the service. I think this is the best cafe in Yogyakarta ever",
                      colorClickableText: Colors.pink,
                      trimLength: 60,
                      trimMode: TrimMode.Length,
                      trimCollapsedText: 'See more',
                      trimExpandedText: 'See less',
                      textAlign: TextAlign.left,
                      lessStyle: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                      moreStyle: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                    ),
                  ),
                  Container(
                    height: 100,
                    child: Row(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.network(
                            "https://thumbs.dreamstime.com/b/gourmet-seafood-meal-grilled-salmon-steak-served-fresh-green-mangetout-peas-lemon-wedges-flavoring-41787753.jpg",
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width * 0.5,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Container(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Stack(
                              children: [
                                Image.network(
                                  "https://thumbs.dreamstime.com/b/close-up-healthy-balanced-nutrition-meal-bowl-beef-meat-rice-steamed-vegetables-broccoli-carrots-served-pl-111327218.jpg",
                                  fit: BoxFit.cover,
                                  height: 100,
                                  width:
                                      MediaQuery.of(context).size.width * 0.35,
                                ),
                                Container(
                                  height: 100,
                                  width:
                                      MediaQuery.of(context).size.width * 0.35,
                                  child: Center(
                                      child: Text(
                                    "2",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 14),
                                  )),
                                  decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.4)),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    margin: EdgeInsets.symmetric(vertical: 10),
                    height: 60,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey.withOpacity(0.11)),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Category",
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "Food & Drink",
                                style:
                                    TextStyle(fontSize: 14, color: Colors.grey),
                              )
                            ],
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Location",
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "Secang Yorkarta",
                                style:
                                    TextStyle(fontSize: 14, color: Colors.grey),
                              )
                            ],
                          ),
                        ]),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          SvgPicture.asset(
                            "assets/icons/like.svg",
                            color: kIconColor,
                            height: 30,
                          ),
                          Text(
                            "68",
                            style: TextStyle(color: kIconColor),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SvgPicture.asset(
                            "assets/icons/dislike.svg",
                            color: kIconColor,
                            height: 30,
                          ),
                          Text(
                            "0",
                            style: TextStyle(color: kIconColor),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          GestureDetector(
                              onTap: () {},
                              child: SvgPicture.asset(
                                "assets/icons/chat.svg",
                                color: kIconColor,
                                height: 30,
                              )),
                          Text(
                            "3",
                            style: TextStyle(color: kIconColor),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SvgPicture.asset(
                            "assets/icons/share.svg",
                            color: kIconColor,
                            height: 30,
                          ),
                          Text(
                            "2",
                            style: TextStyle(color: kIconColor),
                          ),
                        ],
                      ),
                    ],
                  ),
                ]),
              ),],
              );
  }
}