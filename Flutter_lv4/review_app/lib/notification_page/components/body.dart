import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:review_app/constants.dart';
import 'package:review_app/notification_page/components/notification_app_bar.dart';
import 'package:review_app/notification_page/components/notification_content.dart';

class Body extends StatelessWidget {
  final List notifications;
  const Body({Key? key, required this.notifications}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _notification;
    if (!notifications.isEmpty) {
      _notification = notifications[0];
    }
    return _notification == null
        ? Container()
        : SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                SizedBox(
                  height: 35,
                ),
                NotificationAppBar(),
                
                SizedBox(
                  height: 15,
                ),
                NotificationContent(notification: _notification,),
              ],
            ),
          );
  }
}
