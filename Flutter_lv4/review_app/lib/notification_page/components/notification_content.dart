import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../constants.dart';

class NotificationContent extends StatelessWidget {
  final notification;
  const NotificationContent({Key? key, required this.notification}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
                    constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height - 100),
                    padding: EdgeInsets.all(15),
                    width: double.maxFinite,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                        ),
                        color: Colors.white.withOpacity(0.9)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "JUST NOW",
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                        for (int i = 0;
                            i < notification["just now"].length;
                            i++)
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                width: 48,
                                height: 48,
                                child: Stack(
                                  children: [
                                    CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          notification["just now"][i]
                                              ["avatar"]),
                                      radius: 24,
                                    ),
                                    Positioned(
                                      right: 0,
                                      bottom: 0,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: notification["just now"][i]
                                                      ["type"] !=
                                                  "follow"
                                              ? Colors.white
                                              : Colors.transparent,
                                        ),
                                        width: 20,
                                        height: 20,
                                        child: LayoutBuilder(
                                          builder: (context, constraints) {
                                            if (notification["just now"][i]
                                                    ["type"] ==
                                                "like") {
                                              return Icon(
                                                Icons.thumb_up,
                                                size: 15,
                                                color: Colors.blue,
                                              );
                                            } else if (notification["just now"]
                                                    [i]["type"] ==
                                                "mention") {
                                              return Icon(
                                                Icons.chat_bubble,
                                                size: 15,
                                                color: kPrimaryColor,
                                              );
                                            } else if (notification["just now"]
                                                    [i]["type"] ==
                                                "dislike") {
                                              return Icon(
                                                Icons.thumb_down,
                                                size: 15,
                                                color: Colors.black,
                                              );
                                            } else {
                                              return Container(
                                                color: Colors.transparent,
                                              );
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Wrap(
                                    direction: Axis.horizontal,
                                    children: [
                                      Text(
                                        notification["just now"][i]["name"],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                          notification["just now"][i]["text"]),
                                    ],
                                  ),
                                  Text(notification["just now"][i]["time"]),
                                ],
                              ),
                            ],
                          ),
                        Divider(),
                        Text(
                          "Yesterday",
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                        for (int i = 0;
                            i < notification["yesterday"].length;
                            i++)
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                width: 48,
                                height: 48,
                                child: Stack(
                                  children: [
                                    CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          notification["yesterday"][i]
                                              ["avatar"]),
                                      radius: 24,
                                    ),
                                    Positioned(
                                      right: 0,
                                      bottom: 0,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: notification["yesterday"][i]
                                                      ["type"] !=
                                                  "follow"
                                              ? Colors.white
                                              : Colors.transparent,
                                        ),
                                        width: 20,
                                        height: 20,
                                        child: LayoutBuilder(
                                          builder: (context, constraints) {
                                            if (notification["yesterday"][i]
                                                    ["type"] ==
                                                "like") {
                                              return Icon(
                                                Icons.thumb_up,
                                                size: 15,
                                                color: Colors.blue,
                                              );
                                            } else if (notification["yesterday"]
                                                    [i]["type"] ==
                                                "mention") {
                                              return Icon(
                                                Icons.chat_bubble,
                                                size: 15,
                                                color: kPrimaryColor,
                                              );
                                            } else if (notification["yesterday"]
                                                    [i]["type"] ==
                                                "dislike") {
                                              return Icon(
                                                Icons.thumb_down,
                                                size: 15,
                                                color: Colors.black,
                                              );
                                            } else {
                                              return Container(
                                                color: Colors.transparent,
                                              );
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Wrap(
                                    direction: Axis.horizontal,
                                    children: [
                                      Text(
                                        notification["yesterday"][i]["name"],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(notification["yesterday"][i]
                                          ["text"]),
                                    ],
                                  ),
                                  Text(notification["yesterday"][i]["time"]),
                                ],
                              ),
                            ],
                          ),
                        Divider(),
                        Text(
                          "This week",
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                        for (int i = 0;
                            i < notification["this week"].length;
                            i++)
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                width: 48,
                                height: 48,
                                child: Stack(
                                  children: [
                                    CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          notification["this week"][i]
                                              ["avatar"]),
                                      radius: 24,
                                    ),
                                    Positioned(
                                      right: 0,
                                      bottom: 0,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: notification["this week"][i]
                                                      ["type"] !=
                                                  "follow"
                                              ? Colors.white
                                              : Colors.transparent,
                                        ),
                                        width: 20,
                                        height: 20,
                                        child: LayoutBuilder(
                                          builder: (context, constraints) {
                                            if (notification["this week"][i]
                                                    ["type"] ==
                                                "like") {
                                              return Icon(
                                                Icons.thumb_up,
                                                size: 15,
                                                color: Colors.blue,
                                              );
                                            } else if (notification["this week"]
                                                    [i]["type"] ==
                                                "mention") {
                                              return Icon(
                                                Icons.chat_bubble,
                                                size: 15,
                                                color: kPrimaryColor,
                                              );
                                            } else if (notification["this week"]
                                                    [i]["type"] ==
                                                "dislike") {
                                              return Icon(
                                                Icons.thumb_down,
                                                size: 15,
                                                color: Colors.black,
                                              );
                                            } else {
                                              return Container(
                                                color: Colors.transparent,
                                              );
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Wrap(
                                    direction: Axis.horizontal,
                                    children: [
                                      Text(
                                        notification["this week"][i]["name"],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(notification["this week"][i]
                                          ["text"]),
                                    ],
                                  ),
                                  Text(notification["this week"][i]["time"]),
                                ],
                              ),
                            ],
                          ),
                      ],
                    ),);
  }
}