import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:review_app/components/bottom_bar.dart';

import 'components/body.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  List notifications = [];

  /** read data from json file "notification.json"
   * @param null
   * return void
   */
  _readData() async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/json/notification.json")
        .then((s) {
      setState(() {
        notifications = json.decode(s);
      });
    });
  }

  @override
  void initState() {
    _readData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Body(notifications: notifications,),
      bottomNavigationBar: BottomBar(current_page: 2),
    );
  }
}