import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:readmore/readmore.dart';

class DetailComment extends StatelessWidget {
  final reviews;
  const DetailComment({Key? key, required this.reviews}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Container(
          padding: EdgeInsets.only(top: 10, left: 15, right: 15),
          color: Colors.white.withOpacity(0.90),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(children: [
              for (int i = 0; i < reviews["comment"].length; i++)
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CircleAvatar(
                      backgroundImage: NetworkImage(
                        reviews["comment"][i]["avatar"],
                      ),
                    ),
                    Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              reviews["comment"][i]["name"],
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: ReadMoreText(
                                reviews["comment"][i]["text"],
                                trimLength: 50,
                                trimExpandedText: "See less",
                                trimCollapsedText: "See more",
                                lessStyle: TextStyle(color: Colors.grey),
                                moreStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(reviews["comment"][i]["time"]),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Like",
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Reply",
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      "https://thumbs.dreamstime.com/b/closeup-portrait-muscular-man-workout-barbell-gym-brutal-bodybuilder-athletic-six-pack-perfect-abs-shoulders-55122231.jpg"),
                                  radius: 15,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Maruka",
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.4,
                                    child: ReadMoreText(
                                      "Excelent!",
                                      trimLength: 50,
                                      trimExpandedText: "See less",
                                      trimCollapsedText: "See more",
                                      lessStyle:
                                          TextStyle(color: Colors.grey),
                                      moreStyle:
                                          TextStyle(color: Colors.grey),
                                    )),
                              ],
                            )
                          ]),
                    ),
                  ],
                ),
            ]),
          ),
        ),
      );
  }
}