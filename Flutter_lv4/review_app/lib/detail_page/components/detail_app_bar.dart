import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class DetailAppBar extends StatelessWidget {
  final reviews;
  const DetailAppBar({Key? key, required this.reviews}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        dense: true,
        visualDensity: VisualDensity(vertical: -3),
        leading: Container(
          width: 90,
          child: Row(
            children: [
              IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                  size: 20,
                ),
              ),
              CircleAvatar(
                backgroundImage: NetworkImage(reviews["avatar"].toString()),
              ),
            ],
          ),
        ),
        title: Text(
          reviews["name"],
          style: TextStyle(color: Colors.white),
        ),
        subtitle: Text(
          reviews["time"],
          style: TextStyle(color: Colors.grey),
        ),
        trailing: Icon(
          Icons.more_horiz,
          color: Colors.grey,
        ),
      );
  }
}