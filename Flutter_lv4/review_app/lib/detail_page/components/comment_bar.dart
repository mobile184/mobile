import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';

import '../../constants.dart';

class CommentBar extends StatelessWidget {
  const CommentBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 90,
        padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 30),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Container(
          padding: EdgeInsets.only(left: 10, right: 2, top: 2, bottom: 2),
          decoration: BoxDecoration(
            color: Colors.grey.withOpacity(0.05),
            borderRadius: BorderRadius.circular(30),
            border: Border.all(width: 0.1),
          ),
          child: TextFormField(
            textAlignVertical: TextAlignVertical.center,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Write comment here...",
                suffixIcon: GestureDetector(
                  onTap: () {},
                  child: Container(
                      decoration: BoxDecoration(
                          color: kPrimaryColor,
                          borderRadius: BorderRadius.circular(30)),
                      width: 20,
                      height: 20,
                      padding: EdgeInsets.all(15),
                      child: SvgPicture.asset(
                        "assets/icons/send.svg",
                        color: Colors.white,
                      )),
                )),
          ),
        ),
      );
  }
}