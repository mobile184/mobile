import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:readmore/readmore.dart';

import '../../constants.dart';
import 'comment_bar.dart';
import 'detail_app_bar.dart';
import 'detail_comment.dart';
import 'detail_content.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var reviews = Get.arguments;
    return Column(
      children: [
        SizedBox(
          height: 30,
        ),
        DetailAppBar(reviews: reviews),
        DetailContent(reviews: reviews),
        DetailComment(reviews: reviews),
        CommentBar(),
      ],
    );
  }
}
