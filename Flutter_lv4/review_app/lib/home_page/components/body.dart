import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:readmore/readmore.dart';
import 'package:review_app/constants.dart';
import 'package:review_app/detail_page/detail_page.dart';

import 'review_content.dart';
import 'review_header.dart';

class Body extends StatefulWidget {
  final List reviews;
  const Body({Key? key, required this.reviews}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      child: SingleChildScrollView(
        child: Column(children: [
          for (int i = 0; i < widget.reviews.length; i++)
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
              ),
              child: Column(children: [
                ReviewHeader(review: widget.reviews[i],),
                Divider(
                  thickness: 1,
                ),
                ReviewContent(review: widget.reviews[i]),
              ]),
            ),
        ]),
      ),
    );
  }


  
}
