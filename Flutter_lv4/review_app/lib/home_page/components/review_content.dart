import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:readmore/readmore.dart';

import '../../constants.dart';
import '../../detail_page/detail_page.dart';

class ReviewContent extends StatelessWidget {
  final review;
  const ReviewContent({Key? key, this.review}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
                children: [
                  Text(
                    review["title"],
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  RatingBar.builder(
                    initialRating:
                        double.parse(review["rate"].toString()),
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: ReadMoreText(
                      review["text"],
                      colorClickableText: Colors.pink,
                      trimLength: 60,
                      trimMode: TrimMode.Length,
                      trimCollapsedText: 'See more',
                      trimExpandedText: 'See less',
                      textAlign: TextAlign.left,
                      lessStyle: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                      moreStyle: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                    ),
                  ),
                  Container(
                    height: 100,
                    child: Row(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.network(
                            review["images"][0],
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width * 0.5,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Container(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Stack(
                              children: [
                                Image.network(
                                  review["images"][1],
                                  fit: BoxFit.cover,
                                  height: 100,
                                  width: MediaQuery.of(context).size.width * 0.35,
                                ),
                                Container(
                                  height: 100,
                                  width: MediaQuery.of(context).size.width * 0.35,
                                  child: Center(
                                      child: Text(
                                    review["images"].length.toString(),
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 14),
                                  )),
                                  decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.4)),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    margin: EdgeInsets.symmetric(vertical: 10),
                    height: 60,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey.withOpacity(0.11)),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Category",
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                review["category"],
                                style:
                                    TextStyle(fontSize: 14, color: Colors.grey),
                              )
                            ],
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Location",
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                review["location"],
                                style:
                                    TextStyle(fontSize: 14, color: Colors.grey),
                              )
                            ],
                          ),
                        ]),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          SvgPicture.asset(
                            "assets/icons/like.svg",
                            color: kIconColor,
                            height: 30,
                          ),
                          Text(
                            review["like"].toString(),
                            style: TextStyle(color: kIconColor),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SvgPicture.asset(
                            "assets/icons/dislike.svg",
                            color: kIconColor,
                            height: 30,
                          ),
                          Text(
                            review["dislike"].toString(),
                            style: TextStyle(color: kIconColor),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          GestureDetector(
                              onTap: () {
                                Get.to(() => DetailPage(), arguments: {
                                  "avatar": review["avatar"],
                                  "name": review["name"],
                                  "time": review["time"],
                                  "title": review["title"],
                                  "rate": review["rate"],
                                  "text": review["text"],
                                  "images": review["images"],
                                  "category": review["category"],
                                  "location": review["location"],
                                  "like": review["like"],
                                  "dislike": review["dislike"],
                                  "comment": review["comment"],
                                  "share": review["share"]}
                                );
                              },
                              child: SvgPicture.asset(
                                "assets/icons/chat.svg",
                                color: kIconColor,
                                height: 30,
                              )),
                          Text(
                            review["comment"].length.toString(),
                            style: TextStyle(color: kIconColor),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SvgPicture.asset(
                            "assets/icons/share.svg",
                            color: kIconColor,
                            height: 30,
                          ),
                          Text(
                            review["share"].toString(),
                            style: TextStyle(color: kIconColor),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              );
  }
}