import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ReviewHeader extends StatelessWidget {
  final review;
  const ReviewHeader({Key? key, required this.review}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
                dense: true,
                visualDensity: VisualDensity(vertical: -3),
                leading: CircleAvatar(
                  backgroundImage:
                      NetworkImage(review["avatar"].toString()),
                ),
                title: Text(review["name"]),
                subtitle: Text(
                  review["time"],
                  style: TextStyle(color: Colors.grey),
                ),
                trailing: Icon(Icons.more_vert),
              );
  
  }
}