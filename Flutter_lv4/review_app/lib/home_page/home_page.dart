import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:review_app/constants.dart';

import '../components/bottom_bar.dart';
import 'components/body.dart';

class HomePage extends StatefulWidget {
    
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List reviews = [];

  /** read data from json file "home.json"
   * @param null
   * return void
   */
  _readData() async {
    await DefaultAssetBundle.of(context)
        .loadString("assets/json/home.json")
        .then((s) {
      setState(() {
        reviews = json.decode(s);
      });
    });
  }

  @override
  void initState() {
    _readData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildReviewHomeAppBar(),
        body: Body(reviews: reviews,),
        bottomNavigationBar: BottomBar(current_page: 0,));
  }  
}

  AppBar buildReviewHomeAppBar() {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: kPrimaryColor,
      elevation: 0.0,
      title: Padding(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Reviews',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              'Browse any reviews for your reference',
              style:
                  TextStyle(color: kIconColor, fontSize: 12),
            )
          ],
        ),
      ),
      actions: [
        SvgPicture.asset(
          'assets/icons/menu.svg',
          color: kIconColor.withOpacity(0.7),
          width: 30,
          height: 30,
        ),
        SizedBox(
          width: 10,
        )
      ],
    );
  }
