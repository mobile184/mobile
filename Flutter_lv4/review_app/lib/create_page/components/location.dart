import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:syncfusion_flutter_maps/maps.dart';

class Location extends StatefulWidget {
  const Location({Key? key}) : super(key: key);

  @override
  State<Location> createState() => _LocationState();
}

class _LocationState extends State<Location> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
      children: <Widget>[
      Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
        height: 150,
        width: MediaQuery.of(context).size.width - 40,
        child: GoogleMap(
          initialCameraPosition: CameraPosition(target: LatLng(21.035366513856292, 105.7673323),
          zoom: 14),
          onMapCreated: (GoogleMapController controller) {},
        ),
      ),
      ],
        ),
    );
  }
}

