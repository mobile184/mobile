import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:review_app/constants.dart';
import 'package:review_app/controllers/create_page_controller.dart';
import 'package:review_app/create_page/components/location.dart';
import 'package:review_app/home_page/home_page.dart';
import 'package:video_player/video_player.dart';

import 'create_app_bar.dart';
import 'create_content.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  CreatePageController controller = Get.find();
  
  @override
  void initState() {
    // TODO: implement initState
    controller.titleController.addListener(() {
      setState(() {});
    });
    controller.textController.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.titleController.dispose();
    controller.textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(children: [
        SizedBox(
          height: 35,
        ),
        CreateAppBar(),
        SizedBox(
          height: 15,
        ),
        CreateContent(),
      ]),
    );
  }

}
