import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../../home_page/home_page.dart';

class CreateAppBar extends StatelessWidget {
  const CreateAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox(
            width: 10,
          ),
          Text(
            "Rate & Shared",
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
          Expanded(
            child: Container(),
          ),
          GestureDetector(
            onTap: () {
              Get.to(() => HomePage());
            },
            child: Text(
              "Cancel",
              style: TextStyle(color: Colors.grey),
            ),
          ),
          SizedBox(
            width: 10,
          ),
        ],
      );
  }
}