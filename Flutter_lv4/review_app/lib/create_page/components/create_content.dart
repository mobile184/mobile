import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

import '../../constants.dart';
import '../../controllers/create_page_controller.dart';
import '../../home_page/home_page.dart';
import 'location.dart';

class CreateContent extends StatefulWidget {
  const CreateContent({Key? key}) : super(key: key);

  @override
  State<CreateContent> createState() => _CreateContentState();
}

class _CreateContentState extends State<CreateContent> {
    CreatePageController controller = Get.find();

  _getImageFromGallery() async {
    // ignore: deprecated_member_use
    PickedFile? pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 800,
      maxHeight: 800,
    );
    if (pickedFile != null) {
      setState(() {
        controller.imageFile.add(File(pickedFile.path));
      });
    }
  }

  File? _video;
  List<File> videoFile = [];
  VideoPlayerController? videoController;
  _getVideoFromGallery() async {
    // ignore: deprecated_member_use
    final video = await ImagePicker().getVideo(source: ImageSource.gallery);

    _video = File(video!.path);

    videoController = VideoPlayerController.file(_video!)
      ..initialize().then((_) {
        setState(() {});
        videoController!.play();
      });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(15),
        width: double.maxFinite,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
            ),
            color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Review Title",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            TextFormField(
              onChanged: ((value) {
                setState(() {
                  if (!controller.titleController.text.isEmpty &&
                      !controller.textController.text.isEmpty)
                    controller.canSubmmit = true;
                  else
                    controller.canSubmmit = false;
                });
              }),
              controller: controller.titleController,
              decoration: InputDecoration(
                hintText: "Enter the Title",
                hintStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey.withOpacity(0.2),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "What's your rate?",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            RatingBar.builder(
              initialRating: 1,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: false,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
            Text(
              "Category",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: double.maxFinite,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(width: 0.1)),
              child: DropdownButton<String>(
                isExpanded: true,
                underline: SizedBox(),
                hint: Text("Select Category"),
                value: controller.dropdownValue,
                items: controller.listCategory
                    .map<DropdownMenuItem<String>>(
                      (e) => DropdownMenuItem(
                        value: e,
                        child: Text(e),
                      ),
                    )
                    .toList(),
                onChanged: (String? newValue) {
                  setState(() {
                    controller.dropdownValue = newValue!;
                  });
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Location",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Location(),
            Text(
              "Your Comment",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              padding: EdgeInsets.symmetric(horizontal: 10),
              height: 100,
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.05),
                borderRadius: BorderRadius.circular(10),
                border: Border.all(width: 0.1),
              ),
              child: TextFormField(
                onChanged: ((value) {
                  setState(() {
                    if (!controller.titleController.text.isEmpty &&
                        !controller.textController.text.isEmpty)
                      controller.canSubmmit = true;
                    else
                      controller.canSubmmit = false;
                  });
                }),
                controller: controller.textController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Write your comment here...",
                  hintStyle: TextStyle(fontSize: 14, color: Colors.grey),
                ),
              ),
            ),
            Text(
              "Upload photos",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            controller.imageFile.length == 0
                ? DottedBorder(
                    radius: Radius.circular(10),
                    borderType: BorderType.RRect,
                    color: Colors.grey,
                    dashPattern: [8, 8, 8, 8],
                    child: GestureDetector(
                      onTap: () {
                        _getImageFromGallery();
                      },
                      child: Container(
                        width: double.maxFinite,
                        height: 100,
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.05),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset("assets/icons/add-image.svg"),
                              Text("Click to add photo"),
                            ]),
                      ),
                    ))
                : SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Container(
                      child: Row(
                        children: [
                          DottedBorder(
                            radius: Radius.circular(10),
                            borderType: BorderType.RRect,
                            color: Colors.grey,
                            dashPattern: [8, 8, 8, 8],
                            child: GestureDetector(
                              onTap: () {
                                _getImageFromGallery();
                              },
                              child: Container(
                                constraints: BoxConstraints(minWidth: 110),
                                height: 120,
                                width: MediaQuery.of(context).size.width /
                                        (controller.imageFile.length + 1) -
                                    20,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.05),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SvgPicture.asset(
                                          "assets/icons/add-image.svg"),
                                    ]),
                              ),
                            ),
                          ),
                          for (int i = 0;
                              i < controller.imageFile.length;
                              i++)
                            ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Container(
                                constraints: BoxConstraints(minWidth: 110),
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                height: 120,
                                width: MediaQuery.of(context).size.width /
                                        (controller.imageFile.length + 1) -
                                    10 -
                                    controller.imageFile.length * 10,
                                child: Image.file(controller.imageFile[i]),
                              ),
                            )
                        ],
                      ),
                    ),
                  ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Upload Videos",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            _video == null
                ? DottedBorder(
                    radius: Radius.circular(10),
                    borderType: BorderType.RRect,
                    color: Colors.grey,
                    dashPattern: [8, 8, 8, 8],
                    child: GestureDetector(
                      onTap: () {
                        _getVideoFromGallery();
                      },
                      child: Container(
                        width: double.maxFinite,
                        height: 100,
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.05),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset("assets/icons/camera.svg"),
                              Text("Click to add video"),
                            ]),
                      ),
                    ))
                : Container(
                    width: double.maxFinite,
                    height: 400,
                    child: (videoController!.value.isInitialized
                        ? AspectRatio(
                            aspectRatio: 3 / 2,
                            child: GestureDetector(
                                onTap: () {
                                  videoController!.play();
                                },
                                child: VideoPlayer(videoController!)),
                          )
                        : Container()),
                  ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              height: 40,
              width: double.maxFinite,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: controller.canSubmmit == true
                    ? kPrimaryColor
                    : Colors.black.withOpacity(0.1),
              ),
              child: GestureDetector(
                onTap: () {
                  controller.canSubmmit == true
                      ? Get.to(() => HomePage())
                      : Get.snackbar("Missing components",
                          "Please check your title and comment");
                },
                child: Text(
                  "Submit Your Review",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ),
      );
  }
}