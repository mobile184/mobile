import 'package:get/get.dart';
import 'package:review_app/controllers/create_page_controller.dart';

class ScreenBindings implements Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut(() => CreatePageController());
  }
}