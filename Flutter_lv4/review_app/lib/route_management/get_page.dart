import 'package:get/get.dart';
import 'package:review_app/route_management/route.dart';
import 'package:review_app/route_management/screen_bindings.dart';

import '../create_page/create_page.dart';
import '../detail_page/detail_page.dart';
import '../home_page/home_page.dart';
import '../notification_page/notification_page.dart';
import '../welcom_page/welcom_page.dart';

class GetAllPage {
  /** get list of page for routing
   * @param null
   * return List<GetPage>
   */
  static List<GetPage> getAllPages() {
    return [
      GetPage(name: kWelcomPage, page: () => WelcomPage()),
      GetPage(name: kReviewHomePage, page: () => HomePage()),
      GetPage(name: kDetailPage, page: () => DetailPage()),
      GetPage(name: kNotificationPage, page: () => NotificationPage()),
      GetPage(name: kCreatePage, page: () => CreatePage(), bindings: [ScreenBindings()])
    ];
  }
}
