import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:review_app/constants.dart';
import 'package:review_app/route_management/get_page.dart';
import 'package:review_app/route_management/route.dart';
import 'package:review_app/route_management/screen_bindings.dart';
import 'package:review_app/welcom_page/welcom_page.dart';
import 'create_page/create_page.dart';
import 'detail_page/detail_page.dart';
import 'home_page/home_page.dart';
import 'notification_page/notification_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Review App',
      theme: ThemeData(
        brightness: Brightness.light,
        scaffoldBackgroundColor: kPrimaryColor,
        textTheme:
            GoogleFonts.poppinsTextTheme().apply(displayColor: kTextColor),
      ),
      initialRoute: kWelcomPage,
      getPages: GetAllPage.getAllPages(),
    );
  }
}
